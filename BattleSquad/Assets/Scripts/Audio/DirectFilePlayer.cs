﻿using FMOD;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DirectFilePlayer : LowLevelAudioPlayer
{
    [SerializeField]
    private string soundFileLocation = null;
    [SerializeField]
    protected bool isLooping = default(bool);
    public bool IsLooping
    {
        get { return isLooping; }
        set
        {
            isLooping = value;
            SetLooping();
        }
    }

    protected override void Start()
    {
        base.Start();

        LowLevelSystemAdjuster.Instance.ResizeDspBuffer();

        lowLevelFmodSystem.createStream(System.IO.Path.Combine(Application.streamingAssetsPath, soundFileLocation), MODE.DEFAULT, out sound);

        if (is3D)
        {
            sound.setMode(MODE._3D | MODE._3D_LINEARSQUAREROLLOFF);
        }

        if (playOnStart)
        {
            Play();
        }
    }

    protected override void Update()
    {
        base.Update();
        if (changeVolumeGradually)
        {
            amplitude = Mathf.MoveTowards(amplitude, desiredAmplitude, secondsToGradualVolumeChangeFromZeroToOne * Time.deltaTime);
        }
        channel.setVolume(amplitude);
        CheckGradualityConditions();
    }

    protected override void OnValidate()
    {
        base.OnValidate();
    }

    private void SetLooping()
    {
        if (isLooping)
        {
            channel.setLoopCount(-1);
            channel.setMode(MODE.LOOP_NORMAL);
        }
        else
        {
            channel.setLoopCount(0);
            channel.setMode(MODE.LOOP_OFF);
        }
    }

    public override void Play()
    {
        if (playState == PlayState.Stopping)
        {
            playState = PlayState.Plaing;
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            return;
        }
        else if (playState != PlayState.Stopped)
        {
            return;
        }
        playState = PlayState.Plaing;
        lowLevelFmodSystem.playSound(sound, channelGroup, true, out channel);
        SetLooping();
        channel.setPosition(0, TIMEUNIT.MS);
        if (is3D)
        {
            channel.set3DMinMaxDistance(minDistance, maxDinstance);
            Update();
        }
        if (changeVolumeGradually)
        {
            amplitude = 0.0F;
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
        }
        else
        {
            amplitude = Decibels.DecibelsToLinearWithZeroing(volume);
        }
        channel.setPaused(false);
    }

    public override void Resume()
    {
        if (playState == PlayState.Pausing)
        {
            playState = PlayState.Plaing;
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            return;
        }
        else if (playState != PlayState.Paused)
        {
            return;
        }
        playState = PlayState.Plaing;
        if (changeVolumeGradually)
        {
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            channel.setPaused(false);
        }
        else
        {
            amplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            channel.setPaused(false);
        }
    }

    public override void Pause()
    {
        if (playState != PlayState.Plaing)
        {
            return;
        }
        playState = PlayState.Pausing;
        if (changeVolumeGradually)
        {
            desiredAmplitude = 0.0F;
        }
        else
        {
            channel.setPaused(true);
            playState = PlayState.Paused;
        }
    }

    public override void Stop()
    {
        if (playState == PlayState.Stopped)
        {
            return;
        }
        playState = PlayState.Stopping;
        if (changeVolumeGradually)
        {
            desiredAmplitude = 0.0F;
        }
        else
        {
            channel.stop();
            playState = PlayState.Stopped;
        }
    }

    public override void CheckGradualityConditions()
    {
        if (changeVolumeGradually)
        {
            if (playState == PlayState.Pausing)
            {
                if (Mathf.Approximately(amplitude, 0.0F))
                {
                    channel.setPaused(true);
                    playState = PlayState.Paused;
                }
            }
            else if (playState == PlayState.Stopping)
            {
                if (Mathf.Approximately(amplitude, 0.0F))
                {
                    channel.stop();
                    playState = PlayState.Stopped;
                }
            }
        }
        if (playState == PlayState.Plaing)
        {
            bool isChannelPlaing;
            channel.isPlaying(out isChannelPlaing);
            if (isChannelPlaing == false)
            {
                playState = PlayState.Stopped;
            }
        }
    }
}