﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DopplerEffectEmitter : MonoBehaviour
{
    public const float SoundSpeed = 340.0F;

    [SerializeField]
    private float basePitch = 1.0F;
    public float BasePitch
    {
        get { return basePitch; }
        set { basePitch = value; }
    }

    [SerializeField]
    private float scalePercentage = 100.0F;
    public float ScalePercentage
    {
        get { return scalePercentage; }
        set
        {
            scalePercentage = value;
            scale = value * 0.01F;
        }
    }
    private float scale = 1.0F;

    private StudioEventEmitter fmodStudioEventEmitter;
    private Rigidbody rigidbodyComponent;

    private Vector3 previousPosition;

    private void Awake()
    {
        fmodStudioEventEmitter = GetComponent<StudioEventEmitter>();
        if (fmodStudioEventEmitter == null)
        {
            Debug.LogWarning("No StudioEventEmitter component found in DopplerEffectEmitter's gameobject!");
            enabled = false;
        }
        rigidbodyComponent = GetComponentInParent<Rigidbody>();
    }

    private void Start()
    {
        previousPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (LowLevelListener.Instance == null)
        {
            return;
        }
        Vector3 directionOfListener = (transform.position - LowLevelListener.Instance.transform.position).normalized;

        Vector3 emitterVelocity;
        if (rigidbodyComponent != null)
        {
            emitterVelocity = rigidbodyComponent.velocity;
        }
        else
        {
            emitterVelocity = (transform.position - previousPosition) / Time.deltaTime;
        }
        Vector3 listenerVelocity;
        if (LowLevelListener.Instance.HasRigidbodyComponent)
        {
            listenerVelocity = LowLevelListener.Instance.RigidbodyComponent.velocity;
        }
        else
        {
            listenerVelocity = (LowLevelListener.Instance.transform.position - LowLevelListener.Instance.PreviousPosition) / Time.deltaTime;
        }

        Vector3 sumOfVelocities = emitterVelocity - listenerVelocity;
        float speedRelativeToListener = sumOfVelocities.magnitude * Vector3.Dot(sumOfVelocities.normalized, directionOfListener);
        fmodStudioEventEmitter.EventInstance.setPitch(basePitch + (((SoundSpeed - (speedRelativeToListener * scale)) / SoundSpeed) - 1.0F));
        previousPosition = transform.position;
    }

    private void OnValidate()
    {
        scale = scalePercentage * 0.01F;
    }
}