﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EchoFilter : FilterBase
{
    [SerializeField]
    private float echoLengthInSeconds = default(float);
    public float EchoLengthInSeconds
    {
        get { return echoLengthInSeconds; }
        set { echoLengthInSeconds = value; }
    }

    [SerializeField]
    private float echoFactor = default(float);
    public float EchoFactor
    {
        get { return echoFactor; }
        set { echoFactor = value; }
    }

    private int currentTotalChannels;
    private int currentSampleRate;
    private float currentEchoLengthInSeconds;

    private List<Queue<float>> samples;

    private void Awake()
    {
        samples = new List<Queue<float>>();
    }

    public override void Init(int totalChannels, int sampleRate)
    {
        if ((currentTotalChannels == totalChannels) && (currentSampleRate == sampleRate) && Mathf.Approximately(currentEchoLengthInSeconds, echoLengthInSeconds))
        {
            return;
        }
        currentTotalChannels = totalChannels;
        currentSampleRate = sampleRate;
        currentEchoLengthInSeconds = echoLengthInSeconds;
        if (samples.Count > totalChannels)
        {
            int removeCount = samples.Count - totalChannels;
            for (int i = 0; i < removeCount; ++i)
            {
                samples.RemoveAt(samples.Count - 1);
            }
        }
        else if (samples.Count < totalChannels)
        {
            int addCount = totalChannels - samples.Count;
            for (int i = 0; i < addCount; ++i)
            {
                samples.Add(new Queue<float>());
            }
        }
        for (int i = 0; i < samples.Count; ++i)
        {
            int echoLengthDifference = Mathf.RoundToInt(currentEchoLengthInSeconds * currentSampleRate) - samples[i].Count;
            if (echoLengthDifference > 0)
            {
                for (int j = 0; j < echoLengthDifference; ++j)
                {
                    samples[i].Enqueue(0.0F);
                }
            }
            else if (echoLengthDifference < 0)
            {
                echoLengthDifference = -echoLengthDifference;
                for (int j = 0; j < echoLengthDifference; ++j)
                {
                    samples[i].Dequeue();
                }
            }
        }
    }

    public override float ProcessSample(float sample, int sampleIndex, int channelIndex, int sampleRate, int totalChannels)
    {
        float result = Mathf.Clamp(sample + echoFactor * samples[channelIndex].Dequeue(), -1.0F, 1.0F);
        samples[channelIndex].Enqueue(result);
        return result;
    }

    public override void Reset()
    {
        for (int i = 0; i < samples.Count; ++i)
        {
            samples[i].Clear();
            for (int j = 0; j < Mathf.RoundToInt(currentEchoLengthInSeconds * currentSampleRate); j++)
            {
                samples[i].Enqueue(0.0F);
            }
        }
    }
}