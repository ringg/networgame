﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using System;
using FMODUnity;
using System.Runtime.InteropServices;

public class FilterApplier : MonoBehaviour
{
    private FMOD.System lowLevelFmodSystem;

    private ChannelGroup channelGroup;

    private DSP DspFilter;
    private DSP_DESCRIPTION DspDescription;

    private float[] buffer;

    private int sampleRate;

#pragma warning disable 0414
    private SPEAKERMODE speakerMode;
    private int numberOfRawSpeakers;
#pragma warning restore 0414

    private FilterBase filter;

    private bool doResetFilter = false;

    private void Awake()
    {
        buffer = new float[0];
    }

    private void Start()
    {
        lowLevelFmodSystem = RuntimeManager.LowlevelSystem;

        lowLevelFmodSystem.getMasterChannelGroup(out channelGroup);

        DspDescription = new DSP_DESCRIPTION();

        DspDescription.read = OnDspReadCallback;

        lowLevelFmodSystem.createDSP(ref DspDescription, out DspFilter);
        DspFilter.setBypass(false);

        channelGroup.addDSP(CHANNELCONTROL_DSP_INDEX.HEAD, DspFilter);
        RuntimeManager.LowlevelSystem.getSoftwareFormat(out sampleRate, out speakerMode, out numberOfRawSpeakers);

        int inChannels;
        DspFilter.getNumInputs(out inChannels);
        filter = GetComponent<FilterBase>();
        filter.Init(inChannels, sampleRate);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("LocalSoldier"))
        {
            doResetFilter = true;
            filter.IsFilterActive = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("LocalSoldier"))
        {
            filter.IsFilterActive = false;
        }
    }

    private void OnDestroy()
    {
        channelGroup.removeDSP(DspFilter);
    }

    private RESULT OnDspReadCallback(ref DSP_STATE dspState, IntPtr inBuffer, IntPtr outBuffer, uint rawLength, int inChannels, ref int outChannels)
    {

        float currentSample;
        int i = 0;
        try
        {
            int length = (int)rawLength * inChannels;

            if (length > buffer.Length)
            {
                buffer = new float[length];
            }

            if (doResetFilter)
            {
                filter.Reset();
                doResetFilter = false;
            }

            filter.Init(inChannels, sampleRate);

            for (i = 0; i < length; ++i)
            {
                IntPtr currentPtr = new IntPtr(inBuffer.ToInt64() + (i * sizeof(float)));
                currentSample = (float)Marshal.PtrToStructure(currentPtr, typeof(float));

                float sampleOutput = currentSample;
                if (filter.IsFilterActive)
                {
                    sampleOutput = filter.ProcessSample(sampleOutput, (i / inChannels), (i % inChannels), sampleRate, inChannels);
                }
                buffer[i] = sampleOutput;
            }

            Marshal.Copy(buffer, 0, outBuffer, length);
            outChannels = inChannels;
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log("Samples broke at sample: " + i + "! Error:" + Environment.NewLine + e);
            return RESULT.ERR_OUTPUT_CREATEBUFFER;
        }
        return RESULT.OK;
    }
}