﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FilterBase : MonoBehaviour
{
    [SerializeField]
    protected bool isFilterActive;
    public bool IsFilterActive
    {
        get { return isFilterActive; }
        set { isFilterActive = value; }
    }

    public abstract void Init(int totalChannels, int sampleRate);

    public abstract float ProcessSample(float sample, int sampleIndex, int channelIndex, int sampleRate, int totalChannels);

    public abstract void Reset();
}