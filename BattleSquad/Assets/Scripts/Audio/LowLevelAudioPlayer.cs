﻿using FMOD;
using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public abstract class LowLevelAudioPlayer : MonoBehaviour
{
    [SerializeField, Range(-80.0F, 0.0F)]
    protected float volume = -20.0F;
    public float Volume
    {
        get { return volume; }
        set
        {
            volume = value;
            OnValidate();
        }
    }

    protected float desiredAmplitude;

    protected float amplitude;
    public float Amplitude { get { return amplitude; } }

    [SerializeField]
    protected float minDistance = default(float);
    [SerializeField]
    protected float maxDinstance = default(float);
    [SerializeField]
    protected bool playOnStart = default(bool);
    [SerializeField]
    protected bool is3D = default(bool);
    [SerializeField]
    protected bool changeVolumeGradually = default(bool);
    [SerializeField]
    protected float secondsToGradualVolumeChangeFromZeroToOne = default(float);

    protected Rigidbody rigidbodyComponent;

    protected FMOD.System lowLevelFmodSystem;

    protected ChannelGroup channelGroup;

    protected Channel channel;

    protected Sound sound;

    protected PlayState playState;
    public PlayState PlayState { get { return playState; } }

    protected virtual void Awake()
    {
        OnValidate();
        rigidbodyComponent = GetComponentInParent<Rigidbody>();
        playState = PlayState.Stopped;
    }

    protected virtual void Start()
    {
        lowLevelFmodSystem = RuntimeManager.LowlevelSystem;

        lowLevelFmodSystem.getMasterChannelGroup(out channelGroup);
    }

    protected virtual void Update()
    {
        if (is3D == false)
        {
            return;
        }
        VECTOR position = transform.position.ToFMODVector();
        VECTOR velocity = (rigidbodyComponent != null ? rigidbodyComponent.velocity : Vector3.zero).ToFMODVector();
        VECTOR altPanPos = Vector3.zero.ToFMODVector();
        channel.set3DAttributes(ref position, ref velocity, ref altPanPos);
    }

    protected virtual void OnDestroy()
    {
        channel.stop();
    }

    protected virtual void OnValidate()
    {
        if (changeVolumeGradually)
        {
            if (playState == PlayState.Plaing)
            {
                desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            }
        }
        else
        {
            amplitude = Decibels.DecibelsToLinearWithZeroing(volume);
        }
    }

    public abstract void CheckGradualityConditions();

    public abstract void Play();

    public abstract void Resume();

    public abstract void Pause();

    public abstract void Stop();
}

public enum PlayState
{
    Plaing,
    Pausing,
    Paused,
    Stopping,
    Stopped
}