﻿using FMOD;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowLevelListener : MonoBehaviour
{
    [SerializeField]
    private bool overrideInstance = default(bool);

    private static LowLevelListener instance;
    public static LowLevelListener Instance { get { return instance; } }

    private Rigidbody rigidbodyComponent;
    public Rigidbody RigidbodyComponent { get { return rigidbodyComponent; } }

    private Vector3 previousPosition;
    public Vector3 PreviousPosition { get { return previousPosition; } }

    private bool hasRigidbodyComponent;
    public bool HasRigidbodyComponent { get { return hasRigidbodyComponent; } }

    private FMOD.System lowLevelFmodSystem;

    private void Awake()
    {
        rigidbodyComponent = GetComponentInParent<Rigidbody>();
        if (rigidbodyComponent != null)
        {
            hasRigidbodyComponent = true;
        }
        else
        {
            hasRigidbodyComponent = false;
        }
    }

    private void Start()
    {
        lowLevelFmodSystem = RuntimeManager.LowlevelSystem;
        previousPosition = transform.position;
    }

    private void Update()
    {
        VECTOR position = transform.position.ToFMODVector();
        VECTOR velocity = hasRigidbodyComponent ? rigidbodyComponent.velocity.ToFMODVector() : Vector3.zero.ToFMODVector();
        VECTOR forwardVector = transform.forward.ToFMODVector();
        VECTOR upVector = transform.up.ToFMODVector();
        lowLevelFmodSystem.set3DListenerAttributes(0, ref position, ref velocity, ref forwardVector, ref upVector);
    }

    private void LateUpdate()
    {
        lowLevelFmodSystem.update();
    }

    private void FixedUpdate()
    {
        previousPosition = transform.position;
    }

    private void OnEnable()
    {
        if (overrideInstance)
        {
            instance = this;
        }
    }

    private void OnDisable()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

    public void EnableInstanceOverride()
    {
        overrideInstance = true;
        OnEnable();
    }
}