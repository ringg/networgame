﻿using FMOD;
using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowLevelSystemAdjuster : MonoBehaviour
{
    private static LowLevelSystemAdjuster instance;
    public static LowLevelSystemAdjuster Instance { get { return instance; } }

    private bool wasStreamBufferResized = false;
    private bool wasDspBufferResized = false;

    private void Awake()
    {
        if (instance != null)
        {
            UnityEngine.Debug.LogError("LowLevelSystemAdjuster instance created more than once!");
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    public void ResizeStreamBuffer()
    {
        if (wasStreamBufferResized)
        {
            return;
        }
        RuntimeManager.LowlevelSystem.setStreamBufferSize(65536, TIMEUNIT.RAWBYTES);
        wasStreamBufferResized = true;
    }

    public void ResizeDspBuffer()
    {
        if (wasDspBufferResized)
        {
            return;
        }
        RuntimeManager.LowlevelSystem.setDSPBufferSize(1024, 10);
        wasDspBufferResized = true;
    }
}