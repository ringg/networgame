﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedNoiseGenerator : SampleGenerator
{
    private double lastOutput;

    public override float GetSample(int sample, SoundGenerator soundGenerator)
    {
        double white = MathHelper.RandomFromMinusOneToOneThreadIndependend();
        double red = (lastOutput + (0.02 * white)) / 1.02;
        lastOutput = red;
        return Mathf.Clamp((float)(soundGenerator.Amplitude * red * 3.5), -soundGenerator.Amplitude, soundGenerator.Amplitude);
    }
}