﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SampleGenerator
{
    public abstract float GetSample(int sample, SoundGenerator soundGenerator);
}

public enum SampleGeneratorType
{
    Sine,
    Square,
    Triangle,
    SawTooth,
    WhiteNoise,
    PinkNoise,
    RedNoise
}