﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawToothGenerator : SampleGenerator
{
    public override float GetSample(int sample, SoundGenerator soundGenerator)
    {
        return soundGenerator.Amplitude * (((sample * (2 * soundGenerator.Frequency / soundGenerator.SampleRate)) % 2) - 1);
    }
}