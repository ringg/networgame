﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveGenerator : SampleGenerator
{
    public override float GetSample(int sample, SoundGenerator soundGenerator)
    {
        return soundGenerator.Amplitude * Mathf.Sin((MathHelper.TwoPI * sample * soundGenerator.Frequency) / soundGenerator.SampleRate);
    }
}