﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareWaveGenerator : SampleGenerator
{
    public override float GetSample(int sample, SoundGenerator soundGenerator)
    {
        float sampleSaw = ((sample * (2 * soundGenerator.Frequency / soundGenerator.SampleRate)) % 2) - 1;
        return sampleSaw > 0 ? soundGenerator.Amplitude : -soundGenerator.Amplitude;
    }
}