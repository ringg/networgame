﻿using System;

public class WhiteNoiseGenerator : SampleGenerator
{
    public override float GetSample(int sample, SoundGenerator soundGenerator)
    {
        return soundGenerator.Amplitude * MathHelper.RandomFromMinusOneToOneThreadIndependend();
    }
}