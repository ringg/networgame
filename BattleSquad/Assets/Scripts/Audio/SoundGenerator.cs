﻿using FMOD;
using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class SoundGenerator : LowLevelAudioPlayer
{
    private const int numberOfChannels = 1;

    [SerializeField]
    private SampleGeneratorType sampleGeneratorType = default(SampleGeneratorType);

    [SerializeField, Range(20, 20000)]
    private int frequency = 440;
    public float Frequency { get { return frequency; } }

    private int sampleRate = 44100;
    public int SampleRate { get { return sampleRate; } }

    private SampleGenerator sampleGenerator;

    CREATESOUNDEXINFO soundInfo;

    private int sample = 0;

    private short[] buffer;

    protected override void Awake()
    {
        base.Awake();
        buffer = new short[0];
    }

    protected override void Start()
    {
        base.Start();

        switch (sampleGeneratorType)
        {
            case SampleGeneratorType.Sine:
                sampleGenerator = new SineWaveGenerator();
                break;
            case SampleGeneratorType.Square:
                sampleGenerator = new SquareWaveGenerator();
                break;
            case SampleGeneratorType.Triangle:
                sampleGenerator = new TriangleWaveGenerator();
                break;
            case SampleGeneratorType.SawTooth:
                sampleGenerator = new SawToothGenerator();
                break;
            case SampleGeneratorType.WhiteNoise:
                sampleGenerator = new WhiteNoiseGenerator();
                break;
            case SampleGeneratorType.PinkNoise:
                sampleGenerator = new PinkNoiseGenerator();
                break;
            case SampleGeneratorType.RedNoise:
                sampleGenerator = new RedNoiseGenerator();
                break;
        }
        // Initialize Sample Generation

        soundInfo = new CREATESOUNDEXINFO();

        soundInfo.cbsize = Marshal.SizeOf(soundInfo);
        soundInfo.decodebuffersize = (uint)sampleRate / 10; //100ms

        soundInfo.length = (uint)(sampleRate * numberOfChannels * sizeof(short));
        soundInfo.numchannels = numberOfChannels;
        soundInfo.defaultfrequency = sampleRate;
        soundInfo.format = SOUND_FORMAT.PCM16;

        soundInfo.pcmreadcallback = OnPcmReadCallback;
        soundInfo.pcmsetposcallback = OnPcmSetPositionCallback;

        LowLevelSystemAdjuster.Instance.ResizeStreamBuffer();
        lowLevelFmodSystem.createStream("GeneratedSound", MODE.OPENUSER, ref soundInfo, out sound);

        if (is3D)
        {
            sound.setMode(MODE.OPENUSER | MODE._3D | MODE._3D_LINEARSQUAREROLLOFF);
        }

        if (playOnStart)
        {
            Play();
        }
    }

    private RESULT OnPcmReadCallback(IntPtr soundraw, IntPtr data, uint rawLength)
    {
        int length = ((int)rawLength) / sizeof(short);
        if (length > buffer.Length)
        {
            buffer = new short[length];
        }

        int i = 0;

        for (i = 0; i < length; ++i)
        {

            if (changeVolumeGradually)
            {
                amplitude = Mathf.MoveTowards(amplitude, desiredAmplitude, 1.0F / (sampleRate * secondsToGradualVolumeChangeFromZeroToOne));
                if (playState == PlayState.Stopping)
                {
                    CheckGradualityConditions();
                }
            }

            short sampleValue = (short)Mathf.RoundToInt(sampleGenerator.GetSample(sample, this) * 32767.0F);

            buffer[i] = sampleValue;

            ++sample;
            if (sample >= sampleRate)
            {
                sample = 0;
            }
        }

        Marshal.Copy(buffer, 0, data, length);

        return RESULT.OK;
    }

    private RESULT OnPcmSetPositionCallback(IntPtr soundraw, int subsound, uint position, TIMEUNIT postype)
    {
        return RESULT.OK;
    }

    public override void Play()
    {
        if (playState == PlayState.Stopping)
        {
            playState = PlayState.Plaing;
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            return;
        }
        else if (playState != PlayState.Stopped)
        {
            return;
        }
        playState = PlayState.Plaing;
        lowLevelFmodSystem.playSound(sound, channelGroup, true, out channel);
        channel.setLoopCount(-1);
        channel.setMode(MODE.LOOP_NORMAL);
        channel.setPosition(0, TIMEUNIT.MS);
        if (is3D)
        {
            channel.set3DMinMaxDistance(minDistance, maxDinstance);
            Update();
        }
        if (changeVolumeGradually)
        {
            amplitude = 0.0F;
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
        }
        else
        {
            amplitude = Decibels.DecibelsToLinearWithZeroing(volume);
        }
        channel.setPaused(false);
    }

    public override void Resume()
    {
        if (playState != PlayState.Paused)
        {
            return;
        }
        playState = PlayState.Plaing;
        if (changeVolumeGradually)
        {
            desiredAmplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            channel.setPaused(false);
        }
        else
        {
            amplitude = Decibels.DecibelsToLinearWithZeroing(volume);
            channel.setPaused(false);
        }
    }

    public override void Pause()
    {
        if (playState != PlayState.Plaing)
        {
            return;
        }
        playState = PlayState.Paused;
        if (changeVolumeGradually)
        {
            desiredAmplitude = 0.0F;
        }
        else
        {
            channel.setPaused(true);
        }
    }

    public override void Stop()
    {
        if (playState == PlayState.Stopped)
        {
            return;
        }
        playState = PlayState.Stopping;
        if (changeVolumeGradually)
        {
            desiredAmplitude = 0.0F;
        }
        else
        {
            channel.stop();
            playState = PlayState.Stopped;
        }
    }

    public override void CheckGradualityConditions()
    {
        bool isChannelPlaing;
        channel.isPlaying(out isChannelPlaing);
        if (isChannelPlaing && Mathf.Approximately(amplitude, 0.0F))
        {
            channel.stop();
            playState = PlayState.Stopped;
        }
    }
}