﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Birds : MonoBehaviour
{
    [SerializeField]
    private float minSecondsToNextBirdEffect = default(float);
    [SerializeField]
    private float maxSecondsToNextBirdEffect = default(float);
    [SerializeField, EventRef]
    private string birdsSoundEffect = null;

    private List<Transform> birdLocations = new List<Transform>();

    private float effectTime;

    private void Awake()
    {
        foreach (Transform child in transform)
        {
            birdLocations.Add(child);
        }
    }

    private void Start()
    {
        SetNewRandomEffectTime();
    }

    private void Update()
    {
        effectTime = Mathf.MoveTowards(effectTime, 0.0F, Time.deltaTime);
        if (Mathf.Approximately(effectTime, 0.0F))
        {
            RuntimeManager.PlayOneShot(birdsSoundEffect, birdLocations[Random.Range(0, birdLocations.Count)].position);
            SetNewRandomEffectTime();
        }
    }

    private void SetNewRandomEffectTime()
    {
        effectTime = Random.Range(minSecondsToNextBirdEffect, maxSecondsToNextBirdEffect);
    }
}