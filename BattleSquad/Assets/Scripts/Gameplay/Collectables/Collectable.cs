﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : SyncBehaviour
{
    [SerializeField]
    protected UInt16 collectableIndex = default(UInt16);

    protected SyncRPCRunOnServerActionWithParameter<UInt16> onCollectedRPCRunOnServer;

    protected NetObj netObj;

    public override void Init(NetObj netObj)
    {
        this.netObj = netObj;
        onCollectedRPCRunOnServer = new SyncRPCRunOnServerActionWithParameter<UInt16>(OnCollectedRPCRunOnServerMethod, netObj);
    }

    public void Collect(UInt16 soldierDoingDamageNetID)
    {
        onCollectedRPCRunOnServer.Invoke(soldierDoingDamageNetID);
    }

    protected void OnCollectedRPCRunOnServerMethod(UInt16 soldierDoingDamageNetID)
    {
        Soldier soldierDoingDamage = Soldier.GetSoldier(soldierDoingDamageNetID);
        if (soldierDoingDamage.IsDead)
        {
            return;
        }
        soldierDoingDamage.SoldierHealthComponent.ShowCollectInfoForKiller.Invoke(collectableIndex);
        soldierDoingDamage.SoldierHealthComponent.ShowCollectInfoForEveryoneElse.Invoke(collectableIndex);
        ++NetObjMgr.instance.players[soldierDoingDamage.SoldierNetObj.owningPlayerNetID].score.Value;
        NetObjMgr.instance.DeSpawn(netObj.netID);
        Game.Instance.CheckPlayersScoreForWin();
    }
}