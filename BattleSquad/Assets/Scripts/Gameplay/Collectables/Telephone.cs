﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Telephone : Collectable
{
    [SerializeField]
    private SoundGenerator soundGenerator = null;

    private Coroutine noSignalSoundCoroutine;

    private void Start()
    {
        noSignalSoundCoroutine = StartCoroutine(NoSignalSoundRoutine());
    }

    private void OnDestroy()
    {
        if (noSignalSoundCoroutine != null)
        {
            StopCoroutine(noSignalSoundCoroutine);
        }
    }

    private IEnumerator NoSignalSoundRoutine()
    {
        yield return null;
        while (true)
        {
            soundGenerator.Resume();
            yield return new WaitForSeconds(0.5F);
            soundGenerator.Pause();
            yield return new WaitForSeconds(0.5F);
        }
    }
}