﻿using UnityEngine;
using System.Collections;

public class FreeCamera : MonoBehaviour
{
    private bool freeLookEnabled = false;

    public float lookSpeed = 5f;
    public float moveSpeed = 5f;
    public float sprintSpeed = 50f;

    float m_yaw;
    float m_pitch;

    private void Awake()
    {
        EventManager.StartListening(EventType.GameStarted, OnGameStarted);
        EventManager.StartListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StartListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StartListening(EventType.LocalSoldierDied, OnLocalSoldierDiedOrDeSpawned);
        EventManager.StartListening(EventType.LocalSoldierDeSpawned, OnLocalSoldierDiedOrDeSpawned);
    }

    private void Update()
    {
        if (freeLookEnabled == false || Game.Instance == null || Game.Instance.hasEnded == null || Game.Instance.hasEnded.Value)
        {
            return;
        }

        m_yaw = (m_yaw + lookSpeed * Input.GetAxis("Mouse X")) % 360f;
        m_pitch = (m_pitch - lookSpeed * Input.GetAxis("Mouse Y")) % 360f;
        transform.rotation = Quaternion.AngleAxis(m_yaw, Vector3.up) * Quaternion.AngleAxis(m_pitch, Vector3.right);

        var speed = Time.deltaTime * (Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : moveSpeed);
        var forward = speed * Input.GetAxis("Vertical");
        var right = speed * Input.GetAxis("Horizontal");
        var up = speed * ((Input.GetKey(KeyCode.E) ? 1f : 0f) - (Input.GetKey(KeyCode.Q) ? 1f : 0f));
        transform.position += transform.forward * forward + transform.right * right + transform.up * up;
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.GameStarted, OnGameStarted);
        EventManager.StopListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StopListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StopListening(EventType.LocalSoldierDied, OnLocalSoldierDiedOrDeSpawned);
        EventManager.StopListening(EventType.LocalSoldierDeSpawned, OnLocalSoldierDiedOrDeSpawned);
    }

    private void OnGameStarted()
    {
        freeLookEnabled = true;
    }

    private void OnLocalSoldierSpawnedOrResurrected()
    {
        gameObject.SetActive(false);
    }

    private void OnLocalSoldierDiedOrDeSpawned(object sender)
    {
        Soldier soldier = (Soldier)sender;
        transform.position = soldier.FPPCamera.transform.position;
        transform.rotation = soldier.FPPCamera.transform.rotation;
        gameObject.SetActive(true);
    }
}