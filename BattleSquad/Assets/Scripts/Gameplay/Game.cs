﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Game : SyncBehaviour
{
    [SerializeField]
    private int scoreToWin = default(int);
    [SerializeField]
    private float secondsToGameEnd = default(float);
    [SerializeField]
    private float initialTime = default(float);

    public static Game Instance;

    public SyncVar<bool> isStarted;
    public SyncVar<bool> hasEnded;

    private SyncVar<float> synchronizedTimer;

    private TimerUI timerUI;

    private float timer;
    public float Timer { get { return timer; } }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Game instance created more than once!");
            return;
        }
        Instance = this;
        timerUI = FindObjectOfType<TimerUI>();
    }

    private void Update()
    {
        if (isStarted == null || isStarted.Value == false)
        {
            return;
        }

        if (NetObjMgr.instance.IsServer)
        {
            synchronizedTimer.Value = Mathf.MoveTowards(synchronizedTimer.Value, 0.0F, Time.deltaTime);
            timer = synchronizedTimer.Value;
            if (Mathf.Approximately(timer, 0.0F))
            {
                EndGame();
            }
        }
        else
        {
            timer = Mathf.MoveTowards(timer, 0.0f, Time.deltaTime);
        }

        timerUI.UpdateTimer(timer);
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    public override void Init(NetObj netObj)
    {
        isStarted = new SyncVar<bool>(false, OnIsStartedValueChanged, netObj);
        hasEnded = new SyncVar<bool>(false, OnHasEndedValueChanged, netObj);
        synchronizedTimer = new SyncVar<float>(initialTime, OnSynchronizedTimerChange, netObj);
        timer = initialTime;
    }

    private void OnSynchronizedTimerChange(float newValue)
    {
        if (NetObjMgr.instance.IsServer == false)
        {
            if (Mathf.Abs(timer - newValue) > 1.0F)
            {
                timer = newValue;
            }
            else
            {
                timer = Mathf.MoveTowards(timer, newValue, Time.deltaTime);
            }
        }
    }

    private void OnIsStartedValueChanged(bool newValue)
    {
        if (newValue)
        {
            EventManager.TriggerEvent(EventType.GameStarted, this);
        }
    }

    private void OnHasEndedValueChanged(bool newValue)
    {
        if (newValue)
        {
            EventManager.TriggerEvent(EventType.GameEnded, this);
        }
    }

    public void OnStartGameClicked()
    {
        isStarted.Value = true;
    }

    public void CheckPlayersScoreForWin()
    {
        if (NetObjMgr.instance.players.Values.Any(player => player.score.Value >= scoreToWin))
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        if (hasEnded.Value)
        {
            return;
        }
        hasEnded.Value = true;
        StartCoroutine(EndGameRoutine());
    }

    private IEnumerator EndGameRoutine()
    {
        yield return new WaitForSeconds(secondsToGameEnd);
        NetObjMgr.instance.Quit(QuitMode.ExitGame);
    }
}