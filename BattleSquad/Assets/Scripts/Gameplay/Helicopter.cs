﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : SyncBehaviour
{
    [SerializeField]
    private Rigidbody rigidbodyComponent = null;
    [SerializeField]
    private Transform mainRotorTransform = null;
    [SerializeField]
    private Transform tailRotorTransform = null;
    [SerializeField]
    private float spawnCircleRadius = default(float);
    [SerializeField]
    private float reSpawnDistanceFromSpawnCircle = default(float);
    [SerializeField]
    private float flyingHeight = default(float);
    [SerializeField]
    private float flygingSpeedInMetersPerSecond = default(float);
    [SerializeField]
    private float mainRotorRevolutionsPerSecond = default(float);
    [SerializeField]
    private float tailRotorRevolutionsPerSecond = default(float);
    [SerializeField]
    private float percentOfDistanceFromCenterToCircleToSpawnAtBegin = default(float);

    private float distanceFromZeroPointToRespawn;

    private bool wasInit = false;
    private bool canBeRespawned = false;

    private void Awake()
    {
        distanceFromZeroPointToRespawn = spawnCircleRadius + reSpawnDistanceFromSpawnCircle;
        distanceFromZeroPointToRespawn *= distanceFromZeroPointToRespawn;
        EventManager.StartListening(EventType.GameStarted, OnGameStarted);
    }

    private void Update()
    {
        if (wasInit == false)
        {
            return;
        }
        mainRotorTransform.Rotate(0.0F, -mainRotorRevolutionsPerSecond * 360.0F * Time.deltaTime, 0.0F);
        tailRotorTransform.Rotate(-tailRotorRevolutionsPerSecond * 360.0F * Time.deltaTime, 0.0F, 0.0F);
        if (NetObjMgr.instance.IsServer && canBeRespawned)
        {
            if (new Vector3(transform.position.x, 0.0F, transform.position.z).sqrMagnitude > distanceFromZeroPointToRespawn)
            {
                Spawn(false);
            }
        }
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.GameStarted, OnGameStarted);
    }

    public override void Init(NetObj netObj)
    {
        wasInit = true;
    }

    private void OnGameStarted()
    {
        if (NetObjMgr.instance.IsServer)
        {
            Spawn(true);
        }
    }

    private void Spawn(bool firstSpawn)
    {
        Vector2 spawnPoint = Random.insideUnitCircle.normalized * spawnCircleRadius;
        if (firstSpawn)
        {
            spawnPoint = spawnPoint * (percentOfDistanceFromCenterToCircleToSpawnAtBegin * 0.01F);
            StartCoroutine(EnableRespawnRoutine());
        }
        transform.position = new Vector3(spawnPoint.x, flyingHeight, spawnPoint.y);
        transform.LookAt(Vector3.zero, Vector3.up);
        Vector3 velocityDirection = -transform.position;
        velocityDirection.y = 0.0F;
        rigidbodyComponent.velocity = velocityDirection.normalized * flygingSpeedInMetersPerSecond;
    }

    private IEnumerator EnableRespawnRoutine()
    {
        do
        {
            yield return null;
        } while (new Vector3(transform.position.x, 0.0F, transform.position.z).sqrMagnitude >= distanceFromZeroPointToRespawn);
        canBeRespawned = true;
    }
}