﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class Overlook : MonoBehaviour
{
    private static Overlook instance;
    public static Overlook Instance { get { return instance; } }

    private PlayerData playerLoginData;
    public PlayerData PlayerLoginData
    {
        get
        {
            PlayerData tempPlayerLoginData = playerLoginData;
            playerLoginData = null;
            return tempPlayerLoginData;
        }
    }

    public string Username { get; set; }

    public bool IsHosting { get; set; }

    public bool IsHostAlsoCllient { get; set; }

    public IPAddress IPAdress { get; set; }

    public string ErrorMessageDisplay { get; set; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetPlayerData(string login, string hashword)
    {
        playerLoginData = new PlayerData();
        playerLoginData.Login = login;
        playerLoginData.Hashword = hashword;
    }
}