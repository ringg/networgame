﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : SyncBehaviour
{
    [SerializeField]
    private Transform FPP = null;
    [SerializeField]
    private Transform TPP = null;
    [SerializeField]
    private float resurrectionTime = default(float);

    [SerializeField]
    private SoldierRigidbodyController soldierRigidbodyControllerComponent = null;
    public SoldierRigidbodyController SoldierRigidbodyControllerComponent { get { return soldierRigidbodyControllerComponent; } }

    [SerializeField]
    private NetObj soldierNetObj = null;
    public NetObj SoldierNetObj { get { return soldierNetObj; } }

    [SerializeField]
    private Camera fppCamera = null;
    public Camera FPPCamera { get { return fppCamera; } }

    [SerializeField]
    private SoldierShooting soldierShootingComponent = null;
    public SoldierShooting SoldierShootingComponent { get { return soldierShootingComponent; } }

    [SerializeField]
    private SoldierHealth soldierHealthComponent = null;
    public SoldierHealth SoldierHealthComponent { get { return soldierHealthComponent; } }

    [SerializeField]
    private LowLevelListener lowLevelListenerComponent = null;
    public LowLevelListener LowLevelListenerComponent { get { return lowLevelListenerComponent; } }

    private bool isControlledByLocalPlayer;
    public bool IsControlledByLocalPlayer { get { return isControlledByLocalPlayer; } }

    private static Dictionary<UInt16, Soldier> soldiersWithNetID = new Dictionary<UInt16, Soldier>();

    private UInt16 soldierNetID;

    private bool isDead = false;
    public bool IsDead { get { return isDead; } }

    private SyncRPCMulticastActionWithParameter<Vector3> setResurrectionPosition;
    private SyncRPCMulticastActionWithParameter<Quaternion> setResurrectionRotation;
    private SyncRPCMulticastActionWithParameter<Vector3> setResurrectionVelocity;


    public override void Init(NetObj netObj)
    {
        if (netObj.isOwnedByLocalPlayer)
        {
            TPP.gameObject.SetActive(false);
            isControlledByLocalPlayer = true;
            EventManager.TriggerEvent(EventType.LocalSoldierSpawned, this);
            lowLevelListenerComponent.EnableInstanceOverride();
        }
        else
        {
            FPP.gameObject.SetActive(false);
            isControlledByLocalPlayer = false;
        }
        soldierNetID = netObj.netID;
        soldiersWithNetID.Add(soldierNetID, this);
        setResurrectionPosition = new SyncRPCMulticastActionWithParameter<Vector3>(SetResurrectionPositionRPC, netObj);
        setResurrectionRotation = new SyncRPCMulticastActionWithParameter<Quaternion>(SetResurrectionRotationRPC, netObj);
        setResurrectionVelocity = new SyncRPCMulticastActionWithParameter<Vector3>(SetResurrectionVelocityRPC, netObj);
    }

    private void OnDestroy()
    {
        if (isControlledByLocalPlayer)
        {
            EventManager.TriggerEvent(EventType.LocalSoldierDeSpawned, this);
        }
        soldiersWithNetID.Remove(soldierNetID);
    }

    public static Soldier GetSoldier(UInt16 soldierNetID)
    {
        return soldiersWithNetID[soldierNetID];
    }

    public void ResurrectStart()
    {
        Transform selectedSpawnPoint = SpawnPointsManager.Instance.GetRandomSpawnPoint();
        transform.position = selectedSpawnPoint.position;
        transform.rotation = selectedSpawnPoint.rotation;
        soldierRigidbodyControllerComponent.RigidbodyComponent.velocity = Vector3.zero;
        NetObjMgr.instance.players[soldierNetObj.owningPlayerNetID].StartCoroutine(ResurrectRoutine());
        setResurrectionPosition.Invoke(selectedSpawnPoint.position);
        setResurrectionRotation.Invoke(selectedSpawnPoint.rotation);
        setResurrectionVelocity.Invoke(Vector3.zero);
        soldierRigidbodyControllerComponent.OnDeadOnServer(selectedSpawnPoint.position, selectedSpawnPoint.rotation, Vector3.zero);
    }

    private IEnumerator ResurrectRoutine()
    {
        yield return new WaitForSeconds(resurrectionTime);
        soldierHealthComponent.Resurrect();
    }

    private void SetResurrectionPositionRPC(Vector3 resurrectionPosition)
    {
        transform.position = resurrectionPosition;
    }

    private void SetResurrectionRotationRPC(Quaternion resurrectionRotation)
    {
        transform.rotation = resurrectionRotation;
        soldierRigidbodyControllerComponent.mouseLook.ResetCharacterRotation(resurrectionRotation);
    }

    private void SetResurrectionVelocityRPC(Vector3 resurrectionVelocity)
    {
        soldierRigidbodyControllerComponent.RigidbodyComponent.velocity = resurrectionVelocity;
    }

    public void OnResurrected()
    {
        isDead = false;
        if (soldierNetObj.isOwnedByLocalPlayer)
        {
            EventManager.TriggerEvent(EventType.LocalSoldierResurrected, this);
        }
        gameObject.SetActive(true);
    }

    public void OnDied()
    {
        isDead = true;
        if (soldierNetObj.isOwnedByLocalPlayer)
        {
            EventManager.TriggerEvent(EventType.LocalSoldierDied, this);
            soldierShootingComponent.OnLocalSoldierDied();
            soldierRigidbodyControllerComponent.OnLocalSoldierDied();
        }
        gameObject.SetActive(false);
    }
}