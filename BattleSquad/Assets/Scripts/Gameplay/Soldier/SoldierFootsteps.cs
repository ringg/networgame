using UnityEngine;
using FMODUnity;

public class SoldierFootsteps : MonoBehaviour
{
    [SerializeField, EventRef]
    public string footstepsSoundEffect = null;
    [SerializeField, EventRef]
    public string footsteps3DSoundEffect = null;

    [InspectorReadOnly]
    public float m_Wood;
    [InspectorReadOnly]
    public float m_Water;
    [InspectorReadOnly]
    public float m_Dirt;
    [InspectorReadOnly]
    public float m_Sand;

    [SerializeField]
    private float m_StepDistance = default(float);

    [SerializeField]
    private bool m_Debug = default(bool);

    private float m_StepRand;
    private Vector3 m_PrevPos;
    private float m_DistanceTravelled;

    private Vector3 m_LinePos;
    private Vector3 m_TrianglePoint0;
    private Vector3 m_TrianglePoint1;
    private Vector3 m_TrianglePoint2;

    private Soldier soldierComponent;
    private SoldierRigidbodyController soldierRigidbodyControllerComponent;

    private void Awake()
    {
        soldierComponent = GetComponent<Soldier>();
        soldierRigidbodyControllerComponent = soldierComponent.SoldierRigidbodyControllerComponent;
    }

    private void Start()
    {
        Random.InitState(System.DateTime.Now.Second);

        m_StepRand = Random.Range(0.0f, 0.5f);
        m_PrevPos = transform.position;
        m_LinePos = transform.position;
    }

    private void Update()
    {
        m_DistanceTravelled += (transform.position - m_PrevPos).magnitude;
        if(m_DistanceTravelled >= m_StepDistance + m_StepRand && soldierRigidbodyControllerComponent.Grounded)
        {
            PlayFootstepSound();
            m_StepRand = Random.Range(0.0f, 0.5f);
            m_DistanceTravelled = 0.0f;
        }

        m_PrevPos = transform.position;

        if(m_Debug)
        {
            Debug.DrawLine(m_LinePos, m_LinePos + Vector3.down * 1000.0f);
            Debug.DrawLine(m_TrianglePoint0, m_TrianglePoint1);
            Debug.DrawLine(m_TrianglePoint1, m_TrianglePoint2);
            Debug.DrawLine(m_TrianglePoint2, m_TrianglePoint0);
        }
    }

    private void PlayFootstepSound()
    {
        m_Water = 0.0f;
        m_Dirt = 1.0f;
        m_Sand = 0.0f;
        m_Wood = 0.0f;

        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 1000.0f))
        {
            if(m_Debug)
                m_LinePos = transform.position;

            if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))//The Viking Village terrain mesh (terrain_near_01) is set to the Ground layer.
            {
                int materialIndex = GetMaterialIndex(hit);
                if(materialIndex != -1)
                {
                    Material material = hit.collider.gameObject.GetComponent<Renderer>().materials[materialIndex];
                    if(material.name == "mat_terrain_near_01 (Instance)")//This texture name is specific to the terrain mesh in the Viking Village scene.
                    {
                        if(m_Debug)
                        {//Calculate the points for the triangle in the mesh that we have hit with our raycast.
                            MeshFilter mesh = hit.collider.gameObject.GetComponent<MeshFilter>();
                            if(mesh)
                            {
                                Mesh m = hit.collider.gameObject.GetComponent<MeshFilter>().mesh;
                                m_TrianglePoint0 = hit.collider.transform.TransformPoint(m.vertices[m.triangles[hit.triangleIndex * 3 + 0]]);
                                m_TrianglePoint1 = hit.collider.transform.TransformPoint(m.vertices[m.triangles[hit.triangleIndex * 3 + 1]]);
                                m_TrianglePoint2 = hit.collider.transform.TransformPoint(m.vertices[m.triangles[hit.triangleIndex * 3 + 2]]);
                            }
                        }

                        //The mask texture determines how the material's main two textures are blended.
                        //Colour values from each texture are blended based on the mask texture's alpha channel value.
                            //0.0f is full dirt texture, 1.0f is full sand texture, 0.5f is half of each. 
                        Texture2D maskTexture = material.GetTexture("_Mask") as Texture2D;
                        Color maskPixel = maskTexture.GetPixelBilinear(hit.textureCoord.x, hit.textureCoord.y);

                        //The specular texture maps shininess / gloss / reflection to the terrain mesh.
                        //We are using it to determine how much water is shown at the cast ray's point of intersection.
                        Texture2D specTexture2 = material.GetTexture("_SpecGlossMap2") as Texture2D;
                        //We apply tiling assuming it is not already applied to hit.textureCoord2
                        float tiling = 40.0f;//This is a public variable set on the material, we could reference the actual variable but I ran out of time.
                        float u = hit.textureCoord.x % (1.0f / tiling);
                        float v = hit.textureCoord.y % (1.0f / tiling);
                        Color spec2Pixel = specTexture2.GetPixelBilinear(u, v);

                        float specMultiplier = 6.0f;//We use a multiplier to better represent the amount of water.
                        m_Water = maskPixel.a * Mathf.Min(spec2Pixel.a * specMultiplier, 0.9f);//Only the sand texture has water, so we multiply by the mask pixel alpha value.
                        m_Dirt = (1.0f - maskPixel.a);
                        m_Sand = maskPixel.a - m_Water * 0.1f;//Ducking the sand a little for the water
                        m_Wood = 0.0f;
                    }
                }
            }
            else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("WoodenPlanks"))
            {
                m_Water = 0.0f;
                m_Dirt = 0.0f;
                m_Sand = 0.0f;
                m_Wood = 1.0f;
            }
        }

        if(m_Debug)
            Debug.Log("Wood: " + m_Wood + " Dirt: " + m_Dirt + " Sand: " + m_Sand + " Water: " + m_Water);

        string footstepsSoundEffectPath;
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            footstepsSoundEffectPath = footstepsSoundEffect;
        }
        else
        {
            footstepsSoundEffectPath = footsteps3DSoundEffect;
        }

        FMOD.Studio.EventInstance e = RuntimeManager.CreateInstance(footstepsSoundEffectPath);
        e.set3DAttributes(RuntimeUtils.To3DAttributes(transform.position));

        SetParameter(e, "Wood", m_Wood);
        SetParameter(e, "Dirt", m_Dirt);
        SetParameter(e, "Sand", m_Sand);
        SetParameter(e, "Water", m_Water);

        e.start();
        e.release();
    }

    private void SetParameter(FMOD.Studio.EventInstance e, string name, float value)
    {
        FMOD.Studio.ParameterInstance parameter;
        e.getParameter(name, out parameter);
        parameter.setValue(value);
    }

    private int GetMaterialIndex(RaycastHit hit)
    {
        Mesh m = hit.collider.gameObject.GetComponent<MeshFilter>().mesh;
        int[] triangle = new int[]
        {
            m.triangles[hit.triangleIndex * 3 + 0],
            m.triangles[hit.triangleIndex * 3 + 1],
            m.triangles[hit.triangleIndex * 3 + 2]
        };
        for(int i = 0; i < m.subMeshCount; ++i)
        {
            int[] triangles = m.GetTriangles(i);
            for(int j = 0; j < triangles.Length; j += 3)
            {
                if(triangles[j + 0] == triangle[0] &&
                    triangles[j + 1] == triangle[1] &&
                    triangles[j + 2] == triangle[2])
                    return i;
            }
        }
        return -1;
    }
}
