﻿using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierHealth : SyncBehaviour
{
    [SerializeField]
    private int maxHP = default(int);
    [SerializeField]
    private int headDamage = default(int);
    [SerializeField]
    private int torsoDamage = default(int);
    [SerializeField]
    private int armDamage = default(int);
    [SerializeField]
    private int handDamage = default(int);
    [SerializeField]
    private int legDamage = default(int);
    [SerializeField]
    private int footDamage = default(int);
    [SerializeField, EventRef]
    private string gruntSoundEffect = null;
    [SerializeField]
    private StudioEventEmitter playerWoundedSoundEffectEmitter = null;
    [SerializeField]
    private float secondsToHealthRegenerationStart = default(float);

    private SyncVar<int> hp;
    public int HP { get { return hp.Value; } }

    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToHead;
    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToTorso;
    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToArm;
    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToHand;
    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToLeg;
    private SyncRPCRunOnServerActionWithParameter<UInt16> DoDamageToFoot;

    private SyncRPCRunOnOwningClientActionWithParameter<UInt16> ShowKillInfoForVictim;
    private SyncRPCRunOnOwningClientActionWithParameter<UInt16> ShowKillInfoForKiller;
    private SyncRPCMulticastActionWithParameter<UInt16> ShowKillInfoForEveryoneElse;

    public SyncRPCRunOnOwningClientActionWithParameter<UInt16> ShowCollectInfoForKiller;
    public SyncRPCMulticastActionWithParameter<UInt16> ShowCollectInfoForEveryoneElse;

    private Dictionary<string, SyncRPCRunOnServerActionWithParameter<UInt16>> colliderNameToDamageRPCDictionary;

    private Soldier soldierComponent;
    private DamageUI damageUI;
    private KillInfoUI killInfoUI;

    private Coroutine healthRegenerationCoroutine;

    private void Awake()
    {
        soldierComponent = GetComponent<Soldier>();
        damageUI = FindObjectOfType<DamageUI>();
        killInfoUI = FindObjectOfType<KillInfoUI>();
        healthRegenerationCoroutine = StartCoroutine(Helper.EmptyRoutine());
    }

    public override void Init(NetObj netObj)
    {
        hp = new SyncVar<int>(maxHP, OnHPChange, netObj);
        DoDamageToHead = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToHeadRPC, netObj);
        DoDamageToTorso = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToTorsoRPC, netObj);
        DoDamageToArm = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToArmRPC, netObj);
        DoDamageToHand = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToHandRPC, netObj);
        DoDamageToLeg = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToLegRPC, netObj);
        DoDamageToFoot = new SyncRPCRunOnServerActionWithParameter<UInt16>(DoDamageToFootRPC, netObj);
        ShowKillInfoForVictim = new SyncRPCRunOnOwningClientActionWithParameter<UInt16>(ShowKillInfoForVictimRPC, netObj);
        ShowKillInfoForKiller = new SyncRPCRunOnOwningClientActionWithParameter<UInt16>(ShowKillInfoForKillerRPC, netObj);
        ShowKillInfoForEveryoneElse = new SyncRPCMulticastActionWithParameter<UInt16>(ShowKillInfoForEveryoneElseRPC, netObj);
        ShowCollectInfoForKiller = new SyncRPCRunOnOwningClientActionWithParameter<UInt16>(ShowCollectInfoForKillerRPC, netObj);
        ShowCollectInfoForEveryoneElse = new SyncRPCMulticastActionWithParameter<UInt16>(ShowCollectInfoForEveryoneElseRPC, netObj);
        colliderNameToDamageRPCDictionary = new Dictionary<string, SyncRPCRunOnServerActionWithParameter<UInt16>>()
        {
            { "Head", DoDamageToHead },
            { "Torso", DoDamageToTorso },
            { "RightArm", DoDamageToArm },
            { "LeftArm", DoDamageToArm },
            { "RightHand", DoDamageToHand },
            { "LeftHand", DoDamageToHand },
            { "RightLeg", DoDamageToLeg },
            { "LeftLeg", DoDamageToLeg },
            { "RightFoot", DoDamageToFoot },
            { "LeftFoot", DoDamageToFoot }
        };
    }

    private void OnHPChange(int newValue)
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            if (newValue > 0)
            {
                damageUI.UpdatePostprocessStrenght(((float)newValue) / maxHP);
                if (newValue < hp.Value)
                {
                    RuntimeManager.PlayOneShot(gruntSoundEffect);
                }
                if (newValue < (maxHP / 4))
                {
                    if (playerWoundedSoundEffectEmitter.IsPlaying() == false)
                    {
                        playerWoundedSoundEffectEmitter.Play();
                    }
                }
                else
                {
                    playerWoundedSoundEffectEmitter.Stop();
                }
            }
            else
            {
                damageUI.UpdatePostprocessStrenght(1.0F);
            }
        }
        if (NetObjMgr.instance.IsServer)
        {
            if (newValue == 0 || newValue == maxHP)
            {
                StopCoroutine(healthRegenerationCoroutine);
            }
            if (newValue < hp.Value)
            {
                StopCoroutine(healthRegenerationCoroutine);
                healthRegenerationCoroutine = StartCoroutine(HealthRegenerationRoutine());
            }
        }
        if (soldierComponent.IsDead)
        {
            if (newValue == maxHP)
            {
                soldierComponent.OnResurrected();
            }
        }
        else
        {
            if (newValue == 0)
            {
                soldierComponent.OnDied();
                if (soldierComponent.IsControlledByLocalPlayer)
                {
                    playerWoundedSoundEffectEmitter.Stop();
                }
            }
        }
    }

    private IEnumerator HealthRegenerationRoutine()
    {
        yield return new WaitForSeconds(secondsToHealthRegenerationStart);
        hp.Value = Mathf.Min(hp.Value + 20, maxHP);
        while (hp.Value < maxHP)
        {
            yield return new WaitForSeconds(1.0F);
            hp.Value = Mathf.Min(hp.Value + 20, maxHP);
        }
    }

    public void DoDamage(string colliderName, UInt16 soldierDoingDamageNetID)
    {
        colliderNameToDamageRPCDictionary[colliderName].Invoke(soldierDoingDamageNetID);
    }

    private void DoDamageToHeadRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(headDamage, soldierDoingDamageNetID);
    }

    private void DoDamageToTorsoRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(torsoDamage, soldierDoingDamageNetID);
    }

    private void DoDamageToArmRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(armDamage, soldierDoingDamageNetID);
    }

    private void DoDamageToHandRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(handDamage, soldierDoingDamageNetID);
    }

    private void DoDamageToLegRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(legDamage, soldierDoingDamageNetID);
    }

    private void DoDamageToFootRPC(UInt16 soldierDoingDamageNetID)
    {
        CalculateDamage(footDamage, soldierDoingDamageNetID);
    }

    private void CalculateDamage(int damage, UInt16 soldierDoingDamageNetID)
    {
        Soldier soldierDoingDamage = Soldier.GetSoldier(soldierDoingDamageNetID);
        if (soldierComponent.IsDead || soldierDoingDamage.IsDead)
        {
            return;
        }
        int calculatedValue = Mathf.Max(hp.Value - damage, 0);
        hp.Value = calculatedValue;
        if (calculatedValue == 0)
        {
            soldierComponent.ResurrectStart();
            ShowKillInfoForVictim.Invoke(soldierDoingDamage.SoldierNetObj.owningPlayerNetID);
            soldierDoingDamage.SoldierHealthComponent.ShowKillInfoForKiller.Invoke(soldierComponent.SoldierNetObj.owningPlayerNetID);
            ShowKillInfoForEveryoneElse.Invoke(soldierDoingDamage.SoldierNetObj.owningPlayerNetID);
            ++NetObjMgr.instance.players[soldierDoingDamage.SoldierNetObj.owningPlayerNetID].score.Value;
            Game.Instance.CheckPlayersScoreForWin();
        }
    }

    public void Resurrect()
    {
        hp.Value = maxHP;
    }

    private void ShowKillInfoForVictimRPC(UInt16 killerPlayerNetID)
    {
        killInfoUI.ShowKillInfoForVictim(NetObjMgr.instance.players[killerPlayerNetID].username.Value);
    }

    private void ShowKillInfoForKillerRPC(UInt16 victimPlayerNetID)
    {
        killInfoUI.ShowKillInfoForKiller(NetObjMgr.instance.players[victimPlayerNetID].username.Value);
    }

    private void ShowKillInfoForEveryoneElseRPC(UInt16 killerPlayerNetID)
    {
        if (soldierComponent.IsControlledByLocalPlayer || (SyncPlayer.localPlayer != null && SyncPlayer.localPlayer.NetObj.netID == killerPlayerNetID))
        {
            return;
        }
        killInfoUI.ShowKillInfoForEveryoneElse(NetObjMgr.instance.players[killerPlayerNetID].username.Value, NetObjMgr.instance.players[soldierComponent.SoldierNetObj.owningPlayerNetID].username.Value);
    }

    private void ShowCollectInfoForKillerRPC(UInt16 collectableIndex)
    {
        switch (collectableIndex)
        {
            case 0:
                killInfoUI.ShowCollectInfoForKiller("Telephone");
                break;
            case 1:
                killInfoUI.ShowCollectInfoForKiller("TV");
                break;
        }
    }

    private void ShowCollectInfoForEveryoneElseRPC(UInt16 collectableIndex)
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        switch (collectableIndex)
        {
            case 0:
                killInfoUI.ShowCollectInfoForEveryoneElse(NetObjMgr.instance.players[soldierComponent.SoldierNetObj.owningPlayerNetID].username.Value, "Telephone");
                break;
            case 1:
                killInfoUI.ShowCollectInfoForEveryoneElse(NetObjMgr.instance.players[soldierComponent.SoldierNetObj.owningPlayerNetID].username.Value, "TV");
                break;
        }
    }
}