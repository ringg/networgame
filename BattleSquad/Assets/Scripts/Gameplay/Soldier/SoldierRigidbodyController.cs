using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class SoldierRigidbodyController : SyncBehaviour
{
    [Serializable]
    public class MovementSettings
    {
        public float ForwardSpeed = 8.0f;   // Speed when walking forward
        public float BackwardSpeed = 4.0f;  // Speed when walking backwards
        public float StrafeSpeed = 4.0f;    // Speed when walking sideways
        public float RunMultiplier = 2.0f;   // Speed when sprinting
        public LayerMask sphereCastLayerMask;
        public KeyCode RunKey = KeyCode.LeftShift;
        public float JumpForce = 30f;
        public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));

        [InspectorReadOnly]
        public float CurrentTargetSpeed = 8f;

        private bool m_Running;

        public void UpdateDesiredTargetSpeed(Vector2 input)
        {
            if (input == Vector2.zero) return;
            if (input.x > 0 || input.x < 0)
            {
                //strafe
                CurrentTargetSpeed = StrafeSpeed;
            }
            if (input.y < 0)
            {
                //backwards
                CurrentTargetSpeed = BackwardSpeed;
            }
            if (input.y > 0)
            {
                //forwards
                //handled last as if strafing and moving forward at the same time forwards speed should take precedence
                CurrentTargetSpeed = ForwardSpeed;
            }
            if (Input.GetKey(RunKey))
            {
                CurrentTargetSpeed *= RunMultiplier;
                m_Running = true;
            }
            else
            {
                m_Running = false;
            }
        }

        public bool Running
        {
            get { return m_Running; }
        }
    }


    [Serializable]
    public class AdvancedSettings
    {
        public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
        public float stickToGroundHelperDistance = 0.5f; // stops the character
        public float maxDifferenceBetweenPositionAndSynchronizedPosition = 2.5F;
    }


    public Camera cam;
    public MovementSettings movementSettings = new MovementSettings();
    public MouseLook mouseLook = new MouseLook();
    public AdvancedSettings advancedSettings = new AdvancedSettings();

    private Rigidbody m_RigidBody;
    public Rigidbody RigidbodyComponent { get { return m_RigidBody; } }
    private CapsuleCollider m_Capsule;
    private Vector3 m_GroundContactNormal;
    private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded;

    private bool handleInput;

    private Soldier soldierComponent;
    private NetObj soldierNetObj;

    private SyncRPCRunOnServerActionWithParameter<Vector3> SetPositionRPCRunOnServer;
    private SyncRPCRunOnServerActionWithParameter<Quaternion> SetRotationRPCRunOnServer;
    private SyncRPCRunOnServerActionWithParameter<Vector3> SetVelocityRPCRunOnServer;

    private Vector3 receivedPosition;
    private Quaternion receivedRotation;
    private Vector3 receivedVelocity;

    private Vector3 previousPosition;
    private Quaternion previousRotation;
    private Vector3 previousVelocity;

    private bool CanControll
    {
        get { return BattleMenuUI.Instance.IsBattleMenuActive == false && Game.Instance.hasEnded.Value == false; }
    }

    public Vector3 Velocity
    {
        get { return m_RigidBody.velocity; }
    }

    public bool Grounded
    {
        get { return m_IsGrounded; }
    }

    public bool Jumping
    {
        get { return m_Jumping; }
    }

    public bool Running
    {
        get
        {
            return movementSettings.Running;
        }
    }

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();
        soldierComponent = GetComponent<Soldier>();
        soldierNetObj = GetComponent<NetObj>();
        mouseLook.Init(transform, cam.transform);
    }

    private void Update()
    {
        if ((handleInput == false) || (CanControll == false))
        {
            return;
        }

        RotateView();

        if (Input.GetButtonDown("Jump") && !m_Jump)
        {
            m_Jump = true;
        }
    }

    private void LateUpdate()
    {
        if (handleInput)
        {
            if ((transform.position - soldierNetObj.SynchronizedPosition).magnitude > advancedSettings.maxDifferenceBetweenPositionAndSynchronizedPosition)
            {
                transform.position = Vector3.Lerp(transform.position, soldierNetObj.SynchronizedPosition, soldierNetObj.DefaultRubberbandPosition);
                m_RigidBody.velocity = Vector3.zero;
            }
            if (transform.position != previousPosition)
            {
                SetPositionRPCRunOnServer.Invoke(transform.position);
                previousPosition = transform.position;
            }
            if (transform.rotation != previousRotation)
            {
                SetRotationRPCRunOnServer.Invoke(transform.rotation);
                previousRotation = transform.rotation;
            }
            if (m_RigidBody.velocity != previousVelocity)
            {
                SetVelocityRPCRunOnServer.Invoke(m_RigidBody.velocity);
                previousVelocity = m_RigidBody.velocity;
            }
        }
        else
        {
            if (soldierNetObj.isServer)
            {
                if (soldierComponent.IsDead == false)
                {
                    transform.position =   Vector3.Lerp    (transform.position,   receivedPosition, soldierNetObj.DefaultRubberbandPosition);
                    transform.rotation =   Quaternion.Slerp(transform.rotation,   receivedRotation, soldierNetObj.DefaultRubberbandRotation);
                    m_RigidBody.velocity = Vector3.Lerp    (m_RigidBody.velocity, receivedVelocity, soldierNetObj.DefaultRubberbandVelocity);
                }
            }
        }
    }


    private void FixedUpdate()
    {
        GroundCheck();

        if (handleInput == false)
        {
            return;
        }

        Vector2 input;

        if (CanControll)
        {
            input = GetInput();
        }
        else
        {
            input = Vector2.zero;
        }

        if ((Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon))
        {
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = cam.transform.forward * input.y + cam.transform.right * input.x;
            desiredMove = Vector3.ProjectOnPlane(desiredMove, m_GroundContactNormal).normalized;

            desiredMove.x = desiredMove.x * movementSettings.CurrentTargetSpeed;
            desiredMove.z = desiredMove.z * movementSettings.CurrentTargetSpeed;
            desiredMove.y = desiredMove.y * movementSettings.CurrentTargetSpeed;
            if (m_RigidBody.velocity.sqrMagnitude <
                (movementSettings.CurrentTargetSpeed * movementSettings.CurrentTargetSpeed))
            {
                m_RigidBody.AddForce(desiredMove * SlopeMultiplier() * (m_IsGrounded ? 1.0F : 0.1F), ForceMode.Impulse);
            }
        }

        if (m_IsGrounded)
        {
            m_RigidBody.drag = 5f;

            if (m_Jump)
            {
                m_RigidBody.drag = 0f;
                m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, 0f, m_RigidBody.velocity.z);
                m_RigidBody.AddForce(new Vector3(0f, movementSettings.JumpForce, 0f), ForceMode.Impulse);
                m_Jumping = true;
            }

            if (!m_Jumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && m_RigidBody.velocity.magnitude < 1f)
            {
                m_RigidBody.Sleep();
            }
        }
        else
        {
            m_RigidBody.drag = 1.0F;
            if (m_PreviouslyGrounded && !m_Jumping)
            {
                StickToGroundHelper();
            }
        }
        m_Jump = false;
    }


    private float SlopeMultiplier()
    {
        float angle = Vector3.Angle(m_GroundContactNormal, Vector3.up);
        return movementSettings.SlopeCurveModifier.Evaluate(angle);
    }


    private void StickToGroundHelper()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position + m_Capsule.center, m_Capsule.radius, Vector3.down, out hitInfo,
                               ((m_Capsule.height / 2f) - m_Capsule.radius) +
                               advancedSettings.stickToGroundHelperDistance))
        {
            if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
            {
                m_RigidBody.velocity = Vector3.ProjectOnPlane(m_RigidBody.velocity, hitInfo.normal);
            }
        }
    }


    private Vector2 GetInput()
    {

        Vector2 input = new Vector2
        {
            x = Input.GetAxis("Horizontal"),
            y = Input.GetAxis("Vertical")
        };
        movementSettings.UpdateDesiredTargetSpeed(input);
        return input;
    }


    private void RotateView()
    {
        //avoids the mouse looking if the game is effectively paused
        if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

        // get the rotation before it's changed
        float oldYRotation = transform.eulerAngles.y;

        mouseLook.LookRotation(transform, cam.transform);

        if (m_IsGrounded)
        {
            // Rotate the rigidbody velocity to match the new direction that the character is looking
            Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
            m_RigidBody.velocity = velRotation * m_RigidBody.velocity;
        }
    }


    /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
    private void GroundCheck()
    {
        m_PreviouslyGrounded = m_IsGrounded;
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position + m_Capsule.center, m_Capsule.radius, Vector3.down, out hitInfo,
                               ((m_Capsule.height / 2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, movementSettings.sphereCastLayerMask))
        {
            m_IsGrounded = true;
            m_GroundContactNormal = hitInfo.normal;
        }
        else
        {
            m_IsGrounded = false;
            m_GroundContactNormal = Vector3.up;
        }
        if (!m_PreviouslyGrounded && m_IsGrounded && m_Jumping)
        {
            m_Jumping = false;
        }
    }

    public override void Init(NetObj netObj)
    {
        if (netObj.isOwnedByLocalPlayer)
        {
            handleInput = true;
            netObj.RubberbandPosition = 0.0F;
            netObj.RubberbandRotation = 0.0F;
            netObj.RubberbandVelocity = 0.0F;
        }
        else
        {
            handleInput = false;
        }
        SetPositionRPCRunOnServer = new SyncRPCRunOnServerActionWithParameter<Vector3>(SetPositionOnServer, netObj);
        SetRotationRPCRunOnServer = new SyncRPCRunOnServerActionWithParameter<Quaternion>(SetRotationOnServer, netObj);
        SetVelocityRPCRunOnServer = new SyncRPCRunOnServerActionWithParameter<Vector3>(SetVelocityOnServer, netObj);
        if (netObj.isServer)
        {
            receivedPosition = netObj.SynchronizedPosition;
            receivedRotation = netObj.SynchronizedRotation;
            receivedVelocity = netObj.SynchronizedVelocity;
        }
    }

    private void SetPositionOnServer(Vector3 position)
    {
        if (soldierComponent.IsDead == false)
        {
            if ((transform.position - position).magnitude > advancedSettings.maxDifferenceBetweenPositionAndSynchronizedPosition)
            {
                return;
            }
            receivedPosition = position;
        }
    }

    private void SetRotationOnServer(Quaternion rotation)
    {
        if (soldierComponent.IsDead == false)
        {
            receivedRotation = rotation;
        }
    }

    private void SetVelocityOnServer(Vector3 velocity)
    {
        if (soldierComponent.IsDead == false)
        {
            receivedVelocity = velocity;
        }
    }

    public void OnDeadOnServer(Vector3 newPosition, Quaternion newRotation, Vector3 newVelocity)
    {
        receivedPosition = newPosition;
        receivedRotation = newRotation;
        receivedVelocity = newVelocity;
    }

    public void OnLocalSoldierDied()
    {
        cam.transform.localRotation = Quaternion.identity;
        mouseLook.OnLocalSoldierDied();
    }
}

[Serializable]
public class MouseLook
{
    public float XSensitivity = 2f;
    public float YSensitivity = 2f;
    public bool clampVerticalRotation = true;
    public float MinimumX = -90F;
    public float MaximumX = 90F;
    public bool smooth;
    public float smoothTime = 5f;


    private Quaternion m_CharacterTargetRot;
    private Quaternion m_CameraTargetRot;


    public void Init(Transform character, Transform camera)
    {
        m_CharacterTargetRot = character.localRotation;
        m_CameraTargetRot = camera.localRotation;
    }


    public void LookRotation(Transform character, Transform camera)
    {
        float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        float xRot = Input.GetAxis("Mouse Y") * YSensitivity;

        m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
        m_CameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

        if (clampVerticalRotation)
            m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);

        if (smooth)
        {
            character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot,
                smoothTime * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot,
                smoothTime * Time.deltaTime);
        }
        else
        {
            character.localRotation = m_CharacterTargetRot;
            camera.localRotation = m_CameraTargetRot;
        }
    }

    public void OnLocalSoldierDied()
    {
        m_CameraTargetRot = Quaternion.identity;
    }

    public void ResetCharacterRotation(Quaternion newRotation)
    {
        m_CharacterTargetRot = newRotation;
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}