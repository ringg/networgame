﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierShooting : SyncBehaviour
{
    [SerializeField]
    private int maxAmmo = 30;
    [SerializeField]
    private float shotsPerSeconds = 11.1F;
    [SerializeField]
    private LayerMask shootingLayerMask = default(LayerMask);
    [SerializeField]
    private float maxRecoil = 0.05F;
    [SerializeField]
    private float recoilReductionPerSecond = 1.0F;
    [SerializeField]
    private float recoilIncreasePerShoot = 0.2F;
    [SerializeField]
    private float reloadTimeInSeconds = 2.0F;
    [SerializeField]
    private ParticleSystem fireEffectPrefab = null;
    [SerializeField]
    private ParticleSystem bloodEffectPrefab = null;
    [SerializeField]
    private ParticleSystem impactEffetcPrefab = null;
    [SerializeField]
    private Transform muzzleFlashFPPLocation = null;
    [SerializeField]
    private Transform muzzleFlashTPPLocation = null;
    [SerializeField, EventRef]
    private string shotSoundEffect = null;
    [SerializeField, EventRef]
    private string shotSoundEffect3D = null;
    [SerializeField, EventRef]
    private string hitmarkerSoundEffect = null;
    [SerializeField]
    private StudioEventEmitter reloadSoundEffectEmitter = null;
    [SerializeField]
    private StudioEventEmitter reloadSoundEffectEmitter3D = null;
    [SerializeField, EventRef]
    private string emptyAmmoSoundEffect = null;
    [SerializeField, EventRef]
    private string bulletImpactSoundEffect = null;
    [SerializeField, EventRef]
    private string bloodHitSoundEffect = null;

    private Soldier soldierComponent;
    private SoldierRigidbodyController soldierRigidbodyControllerComponent;
    private float shootInterval;
    private Transform muzzleFlashLocation;
    private CrosshairUI crosshairUI;
    private AmmoUI ammoUI;
    private int currentAmmo;

    private float currentRecoil = 0.0F;
    private float shootingTimer = 0.0F;
    private bool isShooting = false;
    private bool isReloading = false;

    private SyncRPCMulticastAction showFireEffectRPCMulticastAction;
    private SyncRPCMulticastActionWithParameter<Vector3> showBloodEffectRPCMulticastAction;
    private SyncRPCMulticastActionWithParameter<Vector3> showImpactEffectRPCMulticastAction;
    private SyncRPCMulticastActionWithParameter<bool> playOrStopReloadSoundEffectRPCMulticastAction;

    private Coroutine reloadCoroutine;

    private void Awake()
    {
        soldierComponent = GetComponent<Soldier>();
        soldierRigidbodyControllerComponent = soldierComponent.SoldierRigidbodyControllerComponent;
        crosshairUI = FindObjectOfType<CrosshairUI>();
        ammoUI = FindObjectOfType<AmmoUI>();
        shootInterval = 1.0F / shotsPerSeconds;
        currentAmmo = maxAmmo;
        reloadCoroutine = StartCoroutine(Helper.EmptyRoutine());
    }

    private void Update()
    {
        if (soldierComponent.SoldierNetObj.isOwnedByLocalPlayer == false)
        {
            return;
        }

        if (BattleMenuUI.Instance.IsBattleMenuActive || LeaderboardUI.Instance.IsLeaderboardActive || Game.Instance.hasEnded.Value)
        {
            return;
        }

        shootingTimer = Mathf.MoveTowards(shootingTimer, 0.0F, Time.deltaTime);
        bool didShot = false;

        if (Input.GetButtonDown("Fire"))
        {
            if (currentAmmo > 0)
            {
                isShooting = true;
            }
            else
            {
                if (isReloading == false)
                {
                    RuntimeManager.PlayOneShot(emptyAmmoSoundEffect);
                }
            }
        }
        else if (Input.GetButtonUp("Fire"))
        {
            isShooting = false;
        }
        if (isShooting)
        {
            if (Mathf.Approximately(shootingTimer, 0.0F))
            {
                if ((currentAmmo > 0) && (isReloading == false))
                {
                    Shoot();
                    didShot = true;
                }
            }
        }
        if (didShot)
        {
            currentRecoil = Mathf.MoveTowards(currentRecoil, maxRecoil, recoilIncreasePerShoot * maxRecoil);
        }
        else
        {
            currentRecoil = Mathf.MoveTowards(currentRecoil, 0.0F, recoilReductionPerSecond * Time.deltaTime * maxRecoil);
        }
        
        if (Input.GetButtonDown("Reload"))
        {
            if ((isReloading == false) && (currentAmmo < maxAmmo) && (soldierRigidbodyControllerComponent.Running == false))
            {
                Reload();
            }
        }
        if (isReloading == false)
        {
            if ((isShooting == false) && (currentAmmo == 0) && (soldierRigidbodyControllerComponent.Running == false))
            {
                Reload();
            }
        }
        crosshairUI.SetRecoil(MathHelper.Remap(currentRecoil, 0.0F, maxRecoil, 0.0F, 1.0F));
    }

    public override void Init(NetObj netObj)
    {
        showFireEffectRPCMulticastAction = new SyncRPCMulticastAction(ShowFireEffectRPCMulticast, netObj);
        showBloodEffectRPCMulticastAction = new SyncRPCMulticastActionWithParameter<Vector3>(ShowBloodEffectRPCMulticast, netObj);
        showImpactEffectRPCMulticastAction = new SyncRPCMulticastActionWithParameter<Vector3>(ShowImpactEffectRPCMulticast, netObj);
        playOrStopReloadSoundEffectRPCMulticastAction = new SyncRPCMulticastActionWithParameter<bool>(PlayOrStopReloadSoundRPCMulticast, netObj);
        if (netObj.isOwnedByLocalPlayer)
        {
            gameObject.layer = LayerMask.NameToLayer("LocalSoldier");
            gameObject.tag = "LocalPlayer";
            muzzleFlashLocation = muzzleFlashFPPLocation;
            ammoUI.ShowAmmo(currentAmmo, maxAmmo);
        }
        else
        {
            muzzleFlashLocation = muzzleFlashTPPLocation;
        }
    }

    private void Shoot()
    {
        --currentAmmo;
        ammoUI.ShowAmmo(currentAmmo, maxAmmo);
        shootingTimer = shootInterval;

        Ray ray = soldierComponent.FPPCamera.ViewportPointToRay(new Vector3(0.5F + Random.Range(-currentRecoil, currentRecoil), 0.5F + (Random.Range(-currentRecoil, currentRecoil) * soldierComponent.FPPCamera.aspect), 0.0F));
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, shootingLayerMask))
        {
            if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "SoldierThirdPerson")
            {
                hitInfo.collider.GetComponentInParent<SoldierHealth>().DoDamage(hitInfo.collider.name, soldierComponent.SoldierNetObj.netID);
                ShowBloodEffect(hitInfo.point);
                showBloodEffectRPCMulticastAction.Invoke(hitInfo.point);

                crosshairUI.ShowHitmarker();
                RuntimeManager.PlayOneShot(hitmarkerSoundEffect);
            }
            else if (LayerMask.LayerToName(hitInfo.collider.gameObject.layer) == "Collectable")
            {
                hitInfo.collider.GetComponent<Collectable>().Collect(soldierComponent.SoldierNetObj.netID);

                ShowImpactEffect(hitInfo.point);
                showImpactEffectRPCMulticastAction.Invoke(hitInfo.point);

                crosshairUI.ShowHitmarker();
                RuntimeManager.PlayOneShot(hitmarkerSoundEffect);
            }
            else
            {
                ShowImpactEffect(hitInfo.point);
                showImpactEffectRPCMulticastAction.Invoke(hitInfo.point);
            }
        }

        RuntimeManager.PlayOneShot(shotSoundEffect);

        ShowFireEffect();
        showFireEffectRPCMulticastAction.Invoke();
    }

    private void Reload()
    {
        isReloading = true;
        reloadCoroutine = StartCoroutine(ReloadRoutine());

        reloadSoundEffectEmitter.Play();
        playOrStopReloadSoundEffectRPCMulticastAction.Invoke(true);
    }

    private IEnumerator ReloadRoutine()
    {
        float currentReloadTime = reloadTimeInSeconds;
        yield return null;
        while (Mathf.Approximately(currentReloadTime, 0.0F) == false)
        {
            if (soldierRigidbodyControllerComponent.Running)
            {
                isReloading = false;
                reloadSoundEffectEmitter.Stop();
                playOrStopReloadSoundEffectRPCMulticastAction.Invoke(false);
                yield break;
            }
            currentReloadTime = Mathf.MoveTowards(currentReloadTime, 0.0F, Time.deltaTime);
            yield return null;
        }
        currentAmmo = maxAmmo;
        ammoUI.ShowAmmo(currentAmmo, maxAmmo);
        isReloading = false;
    }

    private void ShowFireEffectRPCMulticast()
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        ShowFireEffect();
        RuntimeManager.PlayOneShot(shotSoundEffect3D, muzzleFlashLocation.position);
    }

    private void ShowFireEffect()
    {
        ParticleSystem fireEffect = Instantiate(fireEffectPrefab, muzzleFlashLocation, false);
        Destroy(fireEffect.gameObject, fireEffect.main.duration);
    }

    private void ShowBloodEffectRPCMulticast(Vector3 position)
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        ShowBloodEffect(position);
    }

    private void ShowBloodEffect(Vector3 position)
    {
        ParticleSystem bloodEffect = Instantiate(bloodEffectPrefab, position, Quaternion.identity);
        Destroy(bloodEffect.gameObject, bloodEffect.main.duration);
        RuntimeManager.PlayOneShot(bloodHitSoundEffect, position);
    }

    private void ShowImpactEffectRPCMulticast(Vector3 position)
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        ShowImpactEffect(position);
    }

    private void ShowImpactEffect(Vector3 position)
    {
        ParticleSystem impactEffect = Instantiate(impactEffetcPrefab, position, Quaternion.identity);
        Destroy(impactEffect.gameObject, impactEffect.main.duration);
        RuntimeManager.PlayOneShot(bulletImpactSoundEffect, position);
    }

    private void PlayOrStopReloadSoundRPCMulticast(bool play)
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        if (play)
        {
            reloadSoundEffectEmitter3D.Play();
        }
        else
        {
            reloadSoundEffectEmitter3D.Stop();
        }
    }

    private void StopReloadSoundRPCMulticast()
    {
        if (soldierComponent.IsControlledByLocalPlayer)
        {
            return;
        }
        reloadSoundEffectEmitter3D.Stop();
    }

    public void OnLocalSoldierDied()
    {
        StopCoroutine(reloadCoroutine);
        isReloading = false;
        isShooting = false;
        currentAmmo = maxAmmo;
        ammoUI.ShowAmmo(currentAmmo, maxAmmo);
    }
}