﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointsManager : MonoBehaviour
{
    public static SpawnPointsManager Instance;

    private List<Transform> spawnPoints;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("SpawnPointsManager instance created more than once!");
            Destroy(gameObject);
            return;
        }
        Instance = this;
        spawnPoints = new List<Transform>();
        foreach (Transform child in transform)
        {
            spawnPoints.Add(child);
        }
    }

    public Transform GetRandomSpawnPoint()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Count)];
    }
}