﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speaker : MonoBehaviour
{
    [SerializeField]
    private DirectFilePlayer speakerAudioPlayer = null;

    private void Awake()
    {
        EventManager.StartListening(EventType.GameStarted, OnGameStarted);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.GameStarted, OnGameStarted);
    }

    private void OnGameStarted()
    {
        speakerAudioPlayer.Play();
    }
}