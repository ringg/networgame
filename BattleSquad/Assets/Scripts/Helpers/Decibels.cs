using System;
using UnityEngine;

public class Decibels
{
    // 20 / ln( 10 )
    private const float LOG_2_DB = 8.6858896380650365530225783783321F;

    // ln( 10 ) / 20
    private const float DB_2_LOG = 0.11512925464970228420089957273422F;

    public static float LinearToDecibels(float lin)
    {
        return Mathf.Log(lin) * LOG_2_DB;
    }

    public static float DecibelsToLinear(float dB)
    {
        return Mathf.Exp(dB * DB_2_LOG);
    }

    public static float DecibelsToLinearWithZeroing(float dB)
    {
        float value = DecibelsToLinear(dB);
        if (value <= 0.0001F)
        {
            value = 0.0F;
        }
        return value;
    }

}