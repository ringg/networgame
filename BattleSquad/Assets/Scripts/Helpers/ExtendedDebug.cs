﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtendedDebug : MonoBehaviour
{
    [SerializeField]
    private Text debugText = null;

    private static ExtendedDebug instance;

    private Coroutine DisableTextCoroutine;

    private void Awake()
    {
        if (instance != null)
        {
            throw new InvalidOperationException("Only one Extended Debug allowed!");
        }
        instance = this;
        DisableTextCoroutine = StartCoroutine(DisableTextRoutine(0.0F));
    }

    public static void Log(object message, float duration = 5.0F)
    {
        Log(message.ToString(), duration);
    }

    public static void Log(string message, float duration = 5.0F)
    {
        Debug.Log(message);
        if (instance == null)
        {
            return;
        }
        instance.debugText.color = Color.white;
        instance.debugText.text = message;
        instance.debugText.enabled = true;
        instance.StopCoroutine(instance.DisableTextCoroutine);
        instance.DisableTextCoroutine = instance.StartCoroutine(instance.DisableTextRoutine(duration));
    }

    public static void LogWarning(object message, float duration = 5.0F)
    {
        LogWarning(message.ToString(), duration);
    }

    public static void LogWarning(string message, float duration = 5.0F)
    {
        Debug.LogWarning(message);
        if (instance == null)
        {
            return;
        }
        instance.debugText.color = new Color(1.0F, 0.5F, 0.0F);
        instance.debugText.text = message;
        instance.debugText.enabled = true;
        instance.StopCoroutine(instance.DisableTextCoroutine);
        instance.DisableTextCoroutine = instance.StartCoroutine(instance.DisableTextRoutine(duration));
    }

    public static void LogError(object message, float duration = 5.0F)
    {
        LogError(message.ToString(), duration);
    }

    public static void LogError(string message, float duration = 5.0F)
    {
        Debug.LogError(message);
        if (instance == null)
        {
            return;
        }
        instance.debugText.text = message;
        instance.debugText.color = Color.red;
        instance.debugText.enabled = true;
        instance.StopCoroutine(instance.DisableTextCoroutine);
        instance.DisableTextCoroutine = instance.StartCoroutine(instance.DisableTextRoutine(duration));
    }

    private IEnumerator DisableTextRoutine(float duration)
    {
        yield return new WaitForSeconds(duration);
        debugText.enabled = false;
    }
}