﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ErrorMessage : MonoBehaviour
{
    [SerializeField]
    private Text messageText = null;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void Start()
    {
        messageText.text = "An error occured: " + Overlook.Instance.ErrorMessageDisplay;
    }

    public void OnReturnToMainMenuButtonClick()
    {
        SceneManager.LoadScene("MainMenu");
    }
}