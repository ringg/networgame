﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitConfirmationMenu : MonoBehaviour
{
    [SerializeField]
    private MainMenu mainMenu = null;

    public void OnOkButtonClick()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void OnCancelButtonClick()
    {
        mainMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}