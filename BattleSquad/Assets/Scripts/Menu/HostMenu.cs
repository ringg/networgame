﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HostMenu : MonoBehaviour
{
    [SerializeField]
    private PlayMenu playMenu = null;
    [SerializeField]
    private Toggle joinAlsoAsClientToggle = null;
    [SerializeField]
    private InputField usernameInputField = null;
    [SerializeField]
    private InputField loginInputFIeld = null;
    [SerializeField]
    private InputField passwordInputFIeld = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable nextSelectable;
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(joinAlsoAsClientToggle.gameObject);
                return;
            }
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                nextSelectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
            }
            else
            {
                nextSelectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
            }
            if (nextSelectable != null)
            {
                EventSystem.current.SetSelectedGameObject(nextSelectable.gameObject);
            }
        }
    }

    private void OnEnable()
    {
        OnJoinAlsoAsClientToggleValueChanged(joinAlsoAsClientToggle.isOn);
    }

    public void OnJoinAlsoAsClientToggleValueChanged(bool newValue)
    {
        usernameInputField.gameObject.SetActive(newValue);
        loginInputFIeld.gameObject.SetActive(newValue);
        passwordInputFIeld.gameObject.SetActive(newValue);
    }

    public void OnStartHostingButtonClick()
    {
        Overlook.Instance.IsHosting = true;
        Overlook.Instance.IsHostAlsoCllient = joinAlsoAsClientToggle.isOn;
        Overlook.Instance.Username = string.Empty;
        if (joinAlsoAsClientToggle.isOn)
        {
            Overlook.Instance.Username = usernameInputField.text;
            Overlook.Instance.SetPlayerData(
                loginInputFIeld.text,
                Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes(passwordInputFIeld.text)))
                );
        }
        SceneManager.LoadScene("Battle");
    }

    public void OnBackButtonClick()
    {
        playMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}