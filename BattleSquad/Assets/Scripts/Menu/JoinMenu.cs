﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JoinMenu : MonoBehaviour
{
    [SerializeField]
    private PlayMenu playMenu = null;
    [SerializeField]
    private InputField IPAdressInputFIeld = null;
    [SerializeField]
    private InputField usernameInputField = null;
    [SerializeField]
    private InputField loginInputFIeld = null;
    [SerializeField]
    private InputField passwordInputFIeld = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable nextSelectable;
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(IPAdressInputFIeld.gameObject);
                return;
            }
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                nextSelectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
            }
            else
            {
                nextSelectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
            }
            if (nextSelectable != null)
            {
                EventSystem.current.SetSelectedGameObject(nextSelectable.gameObject);
            }
        }
    }

    public void OnJoinButtonClick()
    {
        Overlook.Instance.IsHosting = false;
        Overlook.Instance.Username = usernameInputField.text;
        Overlook.Instance.SetPlayerData(
            loginInputFIeld.text,
            Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes(passwordInputFIeld.text)))
            );
        Overlook.Instance.IPAdress = IPAddress.Parse(string.IsNullOrEmpty(IPAdressInputFIeld.text) ? "127.0.0.1" : IPAdressInputFIeld.text);
        SceneManager.LoadScene("Battle");
    }

    public void OnBackButtonClick()
    {
        playMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}