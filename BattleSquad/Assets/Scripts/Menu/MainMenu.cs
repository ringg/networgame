﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private PlayMenu playMenu = null;
    [SerializeField]
    private ExitConfirmationMenu exitConfirmationMenu = null;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void OnPlayButtonClick()
    {
        playMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnExitButtonClick()
    {
        exitConfirmationMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}