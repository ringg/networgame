﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMenu : MonoBehaviour
{
    [SerializeField]
    private HostMenu hostMenu = null;
    [SerializeField]
    private JoinMenu joinMenu = null;
    [SerializeField]
    private MainMenu mainMenu = null;

    public void OnHostButtonClick()
    {
        hostMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnJoinButtonClick()
    {
        joinMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OnBackButtonClick()
    {
        mainMenu.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}