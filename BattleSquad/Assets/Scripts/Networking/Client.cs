﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with Client class defined.</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

/// <summary>
/// Class Client. It is responsible for connecting client to host, and for client messages management.
/// </summary>
public class Client : MonoBehaviour
{
    /// <summary>
    /// The message handlers. Allows to subscribe a delegate that will be invoked when message of specific type will be processed.
    /// </summary>
    public List<Action<Message, IPEndPoint>> msgHandlers = new List<Action<Message, IPEndPoint>>();

    /// <summary>
    /// List of unconfirmed messages with source.
    /// </summary>
    private List<MsgSendInfo> unconfirmedMsgs = new List<MsgSendInfo>();

    /// <summary>
    /// List of received messages with source.
    /// </summary>
    private List<MsgSendInfo> receivedMsgs = new List<MsgSendInfo>();

    /// <summary>
    /// List of messages to be send to host.
    /// </summary>
    private List<Message> msgsToSend = new List<Message>();

    /// <summary>
    /// An array of recently received messages. It stores up to 200 last messages.
    /// </summary>
    private MsgSendInfo[] receivedHistory = new MsgSendInfo[200];

    /// <summary>
    /// Iterator for receivedHistory array.
    /// </summary>
    private int receivedHistoryIter;

    /// <summary>
    /// Network settings assigned to tha same gameObject as this component.
    /// </summary>
    private NetSettings settings;

    /// <summary>
    /// Socket we are connecting with.
    /// </summary>
    private SocketWrapper socket;

    /// <summary>
    /// Host end point info. Filled upon connecting to host.
    /// </summary>
    private IPEndPoint hostEP;

    /// <summary>
    /// Current timer of messages sending.
    /// </summary>
    private float msgDispatchTimer;

    /// <summary>
    /// Unity build-in Awake function. Settings component is being taken and message handlers are being prepared.
    /// </summary>
    private void Awake()
    {
        settings = GetComponent<NetSettings>();

        for (int i = 0; i < 128; ++i)
        {
            msgHandlers.Add(null);
            msgHandlers[i] += ConfirmMsg;
        }

        msgHandlers[3] += HandleConfirm;
    }

    /// <summary>
    /// Unity build-in Start function. The check for hosting is being performed. If user is host, this component is not neccesary and thus being destroyed.
    /// Otherwise, if provided player data is present, connection to host is being made.
    /// </summary>
    private void Start()
    {
        Host host = GetComponent<Host>();
        if (host != null && host.IsHosting)
        {
            Destroy(this);
            return;
        }

        PlayerData providedPlayerData = Overlook.Instance.PlayerLoginData;
        if (providedPlayerData != null)
        {
            Connect(providedPlayerData.Login, providedPlayerData.Hashword, Overlook.Instance.IPAdress);
        }
    }

    /// <summary>
    /// Unity build-in Update function.
    /// It handles most of client messages logic. The order:
    /// -Handle all newly received messages.
    /// -Calculate dispatch timer. If dispatch time is less than send interval - return.
    /// -For each message in messages to send, add message to unconfirmed messages and send it through socket.
    /// -Check connection for performance and if connection is bad - drop it.
    /// -Resend unconfirmed messages which exceeded resend time.
    /// </summary>
    private void Update()
    {
        // handle received messages
        for (int i = 0; i < receivedMsgs.Count; ++i)
            if (receivedMsgs[i] != null)
                ProcessMsg(receivedMsgs[i]);
        receivedMsgs.Clear();

        // dispatch queued messages
        msgDispatchTimer += Time.deltaTime;
        if (msgDispatchTimer < settings.SendInterval)
            return;
        msgDispatchTimer %= settings.SendInterval;

        // send queued messages
        Message msg;
        for (int i = 0; i < msgsToSend.Count; ++i)
        {
            msg = msgsToSend[i];
            if (msg == null)
                continue;

            msg.Encode();
            if (msg.buffer[0] != 3 && msg.buffer[0] != 5)
                unconfirmedMsgs.Add(new MsgSendInfo(msg, hostEP));
            socket.Send(msg.buffer, hostEP);
        }
        msgsToSend.Clear();

        // drop bad connection
        if (unconfirmedMsgs.Count > settings.NumberOfUnconfirmedToKick)
        {
            MsgKick msgKick = new MsgKick();
            msgKick.harmless = false;
            msgKick.reason = "Poor communication to host.".ToCharArray();
            msgKick.Encode();
            ProcessLocalMsg(new MsgSendInfo(msgKick, hostEP));
        }
        else if (unconfirmedMsgs.Count > 0)
        {
            if (Time.time - unconfirmedMsgs[0].firstSendTime > settings.SecondsOfUnconfirmedToKick)
            {
                MsgKick msgKick = new MsgKick();
                msgKick.harmless = false;
                if (SyncPlayer.localPlayer == null)
                {
                    msgKick.reason = "Could not connect to host.".ToCharArray();
                }
                else
                {
                    msgKick.kickedPlayerNetID = SyncPlayer.localPlayer.NetObj.netID;
                    msgKick.reason = "Connection to host lost.".ToCharArray();
                }
                msgKick.Encode();
                ProcessLocalMsg(new MsgSendInfo(msgKick, hostEP));
            }
        }

        // resend unconfirmed msgs
        for (int i = 0; i < unconfirmedMsgs.Count; ++i)
        {
            if (Time.time - unconfirmedMsgs[i].lastSendTime < settings.ResendTime)
                continue;

            unconfirmedMsgs[i].lastSendTime = Time.time;
            msg = unconfirmedMsgs[i].msg;
            if (msg == null)
            {
                unconfirmedMsgs.RemoveAt(i);
                --i;
                continue;
            }
            socket.Send(msg.buffer, unconfirmedMsgs[i].ep);
        }
    }

    /// <summary>
    /// Unity build-in OnDestroy function. If socket is not null, clean it up.
    /// </summary>
    private void OnDestroy()
    {
        if (socket != null)
        {
            socket.CleamUp();
        }
    }

    /// <summary>
    /// Function that is called whenever a new message has been received by socket.
    /// </summary>
    /// <param name="msg">Received message.</param>
    /// <param name="ep">Source of message.</param>
    private void MsgReceived(Message msg, IPEndPoint ep)
    {
        receivedMsgs.Add(new MsgSendInfo(msg, ep));
    }

    /// <summary>
    /// Connects to host with provided data.
    /// </summary>
    /// <param name="login">User Login.</param>
    /// <param name="hashword">Hashword (SHA256 hash calculated from ASCI password bytes).</param>
    /// <param name="ip">Host IP.</param>
    public void Connect(string login, string hashword, IPAddress ip)
    {
        socket = new SocketWrapper(0, false);
        socket.OnReceive += MsgReceived;

        hostEP = new IPEndPoint(ip, 11000);
        
        MsgLogin msgLogin = new MsgLogin();
        msgLogin.login = login.ToCharArray();
        msgLogin.hashword = hashword.ToCharArray();

        SendToHost(msgLogin);
    }

    /// <summary>
    /// Disconnects client from host, clears messages lists and cleans up socket.
    /// </summary>
    public void Disconnect()
    {
        unconfirmedMsgs.Clear();
        receivedMsgs.Clear();
        msgsToSend.Clear();

        hostEP = null;

        socket.CleamUp();
        socket = null;
    }

    /// <summary>
    /// Sends message to host.
    /// </summary>
    /// <param name="msg">Message to send. Does not need to be encoded.</param>
    public void SendToHost(Message msg)
    {
        msgsToSend.Add(msg);
    }

    /// <summary>
    /// Processes local message (f. ex. created by-hand in custom script).
    /// </summary>
    /// <param name="info">Message encapsulated in MsgSendInfo class. Message MUST be encoded. IPEndPoint will be jst passed to handlers.</param>
    public void ProcessLocalMsg(MsgSendInfo info)
    {
        Delegate[] invocationList = msgHandlers[info.msg.buffer[0]].GetInvocationList();
        for (int i = 1; i < invocationList.Length; ++i) // 1 because we don't need to send confirmation to ourselves
        {
            invocationList[i].DynamicInvoke(info.msg, info.ep);
        }
    }

    /// <summary>
    /// Sends confirmation of message to host.
    /// </summary>
    /// <param name="msg">Message to confirm.</param>
    /// <param name="ep">Host IPEndPoint.</param>
    private void ConfirmMsg(Message msg, IPEndPoint ep)
    {
        if (msg.buffer[0] == 3 || msg.buffer[0] == 5) return;
        MsgConfirm msgConfirm = new MsgConfirm();
        msgConfirm.confirmStamp = msg.timestamp;
        SendToHost(msgConfirm);
    }

    #region message handlers

    /// <summary>
    /// Processes message. If message is in history, it is ignored. Otherwise mesage is added to history and passed to handlers.
    /// </summary>
    /// <param name="info">Message with source.</param>
    private void ProcessMsg(MsgSendInfo info)
    {
        if (IsInHistory(info))
        {
            return;
        }
        else
        {
            receivedHistory[receivedHistoryIter] = info;
            receivedHistoryIter = (receivedHistoryIter + 1) % receivedHistory.Length;
            msgHandlers[info.msg.buffer[0]](info.msg, info.ep);
        }
    }

    /// <summary>
    /// Handles the confirm message.
    /// </summary>
    /// <param name="msg">Received confirm message.</param>
    /// <param name="ep">Source of confirm message.</param>
    private void HandleConfirm(Message msg, IPEndPoint ep)
    {
        MsgConfirm msgConfirm = (MsgConfirm)msg;

        MsgSendInfo info = unconfirmedMsgs
            .FirstOrDefault(x => x.msg.timestamp == msgConfirm.confirmStamp);

        if (info == null)
        {
            return;
        }
        unconfirmedMsgs.Remove(info);
    }

    /// <summary>
    /// Determines whether specific message from specific source is in received messages history.
    /// </summary>
    /// <param name="msgInfo">Message with source.</param>
    /// <returns><c>true</c> if provided message from provided soruce was found in history; otherwise, <c>false</c>.</returns>
    private bool IsInHistory(MsgSendInfo msgInfo)
    {
        foreach (MsgSendInfo info in receivedHistory)
            if (info != null && msgInfo.msg.timestamp == info.msg.timestamp && info.ep.Equals(msgInfo.ep))
                return true;
        return false;
    }

    #endregion
}
