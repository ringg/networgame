﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with Host class defined.</summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

/// <summary>
/// Class Host.  It is responsible for managing clients connections, and for host messages management.
/// </summary>
public class Host : MonoBehaviour
{
    /// <summary>
    /// The message handlers. Allows to subscribe a delegate that will be invoked when message of specific type will be processed.
    /// </summary>
    public List<Action<Message, IPEndPoint>> msgHandlers = new List<Action<Message, IPEndPoint>>();

    /// <summary>
    /// Dictionary of registered users. An user with credentials that does not exists here will not be able to login.
    /// </summary>
    private Dictionary<string, string> registeredUsers = new Dictionary<string, string>()
    {
        { "Patryk", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("214980")))},
        { "Kuba", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("214960")))},
        { "q", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("q")))},
        { "w", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("w")))},
        { "e", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("e")))},
        { "r", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("r")))},
        { "t", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("t")))},
        { "y", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("y")))},
        { "u", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("u")))},
        { "i", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("i")))},
        { "o", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("o")))},
        { "p", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("p")))},
        { "a", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("a")))},
        { "s", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("s")))},
        { "d", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("d")))},
        { "f", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("f")))},
        { "g", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("g")))},
        { "h", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("h")))},
        { "j", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("j")))},
        { "k", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("k")))},
        { "l", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("l")))},
        { "z", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("z")))},
        { "x", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("x")))},
        { "c", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("c")))},
        { "v", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("v")))},
        { "b", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("b")))},
        { "n", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("n")))},
        { "m", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("m")))},
        { "1", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("1")))},
        { "2", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("2")))},
        { "3", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("3")))},
        { "4", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("4")))},
        { "5", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("5")))},
        { "6", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("6")))},
        { "7", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("7")))},
        { "8", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("8")))},
        { "9", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("9")))},
        { "0", Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.ASCII.GetBytes("0")))}
    };

    /// <summary>
    /// List of unconfirmed messages with source.
    /// </summary>
    private Dictionary<string, List<MsgSendInfo>> unconfirmedMsgs = new Dictionary<string, List<MsgSendInfo>>();

    /// <summary>
    /// List of received messages with source.
    /// </summary>
    private List<MsgSendInfo> receivedMsgs = new List<MsgSendInfo>();

    /// <summary>
    /// List of messages to be send to host.
    /// </summary>
    private List<MsgSendInfo> msgsToSend = new List<MsgSendInfo>();

    /// <summary>
    /// An array of recently received messages. It stores up to 200 last messages.
    /// </summary>
    private MsgSendInfo[] receivedHistory = new MsgSendInfo[200];

    /// <summary>
    /// Iterator for receivedHistory array.
    /// </summary>
    private int receivedHistoryIter;

    /// <summary>
    /// Network settings assigned to tha same gameObject as this component.
    /// </summary>
    private NetSettings settings;

    /// <summary>
    /// Socket we are connecting with.
    /// </summary>
    private SocketWrapper socket;

    /// <summary>
    /// List of all currently connected clients.
    /// </summary>
    private List<Connection> clients = new List<Connection>();

    /// <summary>
    /// Current timer of messages sending.
    /// </summary>
    private float msgDispatchTimer;

    /// <summary>
    /// The credentials provided by host, if host is client too.
    /// </summary>
    private PlayerData hostCredentials;

    /// <summary>
    /// Debug Inspector-set field indicating whether we are hosting or not.
    /// </summary>
    [SerializeField]
    private bool isHosting = default(bool);

    /// <summary>
    /// Gets a value indicating whether we are going to host or not.
    /// </summary>
    /// <value><c>true</c> if we are going to be host; otherwise, <c>false</c>.</value>
    public bool IsHosting { get { return ((Debug.isDebugBuild && isHosting) || Overlook.Instance.IsHosting); } }

    /// <summary>
    /// Debug Inspector-set field indicating whether we should also be client upon hosting or not.
    /// </summary>
    [SerializeField]
    private bool isAlsoClient = default(bool);

    /// <summary>
    /// Gets a value indicating whether we are going to be also client or not.
    /// </summary>
    /// <value><c>true</c> if this we are going to be also client; otherwise, <c>false</c>.</value>
    public bool IsAlsoClient { get { return ((Debug.isDebugBuild && isAlsoClient) || Overlook.Instance.IsHostAlsoCllient); } }

    /// <summary>
    /// Unity build-in Awake function. Settings component is being taken and message handlers are being prepared.
    /// </summary>
    private void Awake()
    {
        settings = GetComponent<NetSettings>();

        for (int i = 0; i < 128; ++i)
        {
            msgHandlers.Add(null);
            msgHandlers[i] += ConfirmMsg;
        }

        msgHandlers[0] += HandleLogin;
        msgHandlers[3] += HandleConfirm;
    }

    /// <summary>
    /// Unity build-in Start function. The check for hosting is being performed. If user is not host, this component is not neccesary and thus being destroyed.
    /// Otherwise we are preparing socket for incomming connections.
    /// </summary>
    private void Start()
    {
        if (Overlook.Instance.IsHosting == false)
        {
            if (IsHosting == false)
            {
                Destroy(this);
                return;
            }
        }

        Connect();
    }

    /// <summary>
    /// Unity build-in Update function.
    /// It handles most of host messages logic. The order:
    /// -Handle all newly received messages.
    /// -Calculate dispatch timer. If dispatch time is less than send interval - return.
    /// -For each message in messages to send, add message to unconfirmed messages and send it through socket.
    /// -Check for connection performance for each client and if it is bad - kick him.
    /// -Resend unconfirmed messages which exceeded resend time.
    /// </summary>
    private void Update()
    {
        // handle received messages
        for (int i = 0; i < receivedMsgs.Count; ++i)
            if (receivedMsgs[i] != null)
                ProcessMsg(receivedMsgs[i]);
        receivedMsgs.Clear();

        // dispatch queued messages
        msgDispatchTimer += Time.deltaTime;
        if (msgDispatchTimer < settings.SendInterval)
            return;
        msgDispatchTimer %= settings.SendInterval;

        // send queued messages
        Message msg;
        for (int i = 0; i < msgsToSend.Count; ++i)
        {
            msg = msgsToSend[i].msg;
            if (msg == null)
                continue;

            msg.Encode();
            if (msg.buffer[0] != 3 && msg.buffer[0] != 5)
                unconfirmedMsgs[msgsToSend[i].ep.ToString()].Add(new MsgSendInfo(msg, msgsToSend[i].ep));
            socket.Send(msg.buffer, msgsToSend[i].ep);
        }
        msgsToSend.Clear();

        // kick bad connections
        foreach (List<MsgSendInfo> msgList in unconfirmedMsgs.Values.ToArray())
        {
            if (msgList.Count > settings.NumberOfUnconfirmedToKick)
            {
                Kick(msgList[0].ep, false, "Kicked due to poor communication.");
            }
            else if (msgList.Count > 0)
            {
                if (Time.time - msgList[0].firstSendTime > settings.SecondsOfUnconfirmedToKick)
                {
                    Kick(msgList[0].ep, false, "Kicked due to connection lost.");
                }
            }
        }
          
        // resend unconfirmed msgs
        foreach (List<MsgSendInfo> msgList in unconfirmedMsgs.Values)
        {
            for (int i = 0; i < msgList.Count; ++i)
            {
                if (Time.time - msgList[i].lastSendTime < settings.ResendTime)
                    continue;

                msgList[i].lastSendTime = Time.time;
                msg = msgList[i].msg;

                if (msg == null)
                    continue;

                socket.Send(msg.buffer, msgList[i].ep);
            }
        }
    }

    /// <summary>
    /// Unity build-in OnDestroy function. If socket is not null, clean it up.
    /// </summary>
    private void OnDestroy()
    {
        if (socket != null)
        {
            socket.CleamUp();
        }
    }

    /// <summary>
    /// Prepares socket for data receiving.
    /// </summary>
    public void Connect()
    {
        socket = new SocketWrapper(11000, true);
        socket.OnReceive += MsgReceived;
    }

    /// <summary>
    /// Disconnects host, clears messages lists and cleans up socket.
    /// </summary>
    public void Disconnect()
    {
        unconfirmedMsgs.Clear();
        receivedMsgs.Clear();
        msgsToSend.Clear();

        clients.Clear();

        socket.CleamUp();
        socket = null;
    }

    /// <summary>
    /// Routine that waits until all clients are being disconnected.
    /// </summary>
    /// <returns>IEnumerator - Coroutine IEnumerator.</returns>
    public IEnumerator HostClientsDisconnectingRoutine()
    {
        while (clients.Count > 0)
        {
            yield return null;
        }
        yield return new WaitForSecondsRealtime(settings.SendInterval * 3.0F);
    }

    /// <summary>
    /// Function that is called whenever a new message has been received by socket.
    /// </summary>
    /// <param name="msg">Received message.</param>
    /// <param name="ep">Source of message.</param>
    private void MsgReceived(Message msg, IPEndPoint ep)
    {
        if (msg.buffer[0] != 0 && FindConnection(ep) == -1)
        {
            return;
        }

        receivedMsgs.Add(new MsgSendInfo(msg, ep));
    }

    /// <summary>
    /// Sends message to all connected clients.
    /// </summary>
    /// <param name="msg">Message to send. Does not need to be encoded.</param>
    public void SendToAllClients(Message msg)
    {
        foreach (Connection player in clients)  
            SendToClient(msg, player.ep);
    }

    /// <summary>
    /// Sends message to specific client.
    /// </summary>
    /// <param name="msg">Message to send. Does not need to be encoded.</param>
    /// <param name="clientEP">Client IPEndPoint to send message to.</param>
    public void SendToClient(Message msg, IPEndPoint clientEP)
    {
        msgsToSend.Add(new MsgSendInfo(msg, clientEP));
    }

    /// <summary>
    /// Sends message to all clients except provided one.
    /// </summary>
    /// <param name="msg">Message to send. Does not need to be encoded.</param>
    /// <param name="clientEPNotToSendMessageTo">Client IPEndPoint not to send message to.</param>
    public void SendToAllClientsExcept(Message msg, IPEndPoint clientEPNotToSendMessageTo)
    {
        List<Connection> destinationClients = clients.FindAll(clientConnection => !clientConnection.ep.Equals(clientEPNotToSendMessageTo));
        foreach (Connection player in destinationClients)
            SendToClient(msg, player.ep);
    }

    /// <summary>
    /// Processes local message (f. ex. created by-hand in custom script).
    /// </summary>
    /// <param name="info">Message encapsulated in MsgSendInfo class. Message MUST be encoded. IPEndPoint will be jst passed to handlers.</param>
    public void ProcessLocalMsg(MsgSendInfo info)
    {
        Delegate[] invocationList = msgHandlers[info.msg.buffer[0]].GetInvocationList();
        for (int i = 1; i < invocationList.Length; ++i) // 1 because we don't need to send confirmation to ourselves
        {
            invocationList[i].DynamicInvoke(info.msg, info.ep);
        }
    }

    /// <summary>
    /// Sends confirmation of message to specific client.
    /// </summary>
    /// <param name="msg">Message to confirm.</param>
    /// <param name="ep">Client IPEndPoint to send confirmation to.</param>
    private void ConfirmMsg(Message msg, IPEndPoint clientEP)
    {
        if (msg.buffer[0] == 3) return;
        MsgConfirm msgConfirm = new MsgConfirm();
        msgConfirm.confirmStamp = msg.timestamp;
        SendToClient(msgConfirm, clientEP);
    }

    #region message handlers

    /// <summary>
    /// Processes message. If message is in history, it is ignored. Otherwise mesage is added to history and passed to handlers.
    /// </summary>
    /// <param name="info">Message with source.</param>
    private void ProcessMsg(MsgSendInfo info)
    {
        if (IsInHistory(info))
        {
            ConfirmMsg(info.msg, info.ep);
            return;
        }
        else
        {
            receivedHistory[receivedHistoryIter] = info;
            receivedHistoryIter = (receivedHistoryIter + 1) % receivedHistory.Length;
            msgHandlers[info.msg.buffer[0]](info.msg, info.ep);
        }
    }

    /// <summary>
    /// Handles new user login. It performs check for:
    /// -User login existence.
    /// -Login-Password match.
    /// -Unique connection (IP adress and port).
    /// -Unique login (if player with such login is not already connected).
    /// </summary>
    /// <param name="msg">Received Login message.</param>
    /// <param name="ep">Source of message.</param>
    private void HandleLogin(Message msg, IPEndPoint ep)
    {
        MsgLogin logMsg = (MsgLogin)msg;

        string msgLogin = new string(logMsg.login);
        string msgHashword = new string(logMsg.hashword);

        if (registeredUsers.ContainsKey(msgLogin) == false)
        {
            DenyLogin("User with provided login not found.", ep);
            return;
        }

        if (registeredUsers[msgLogin] != msgHashword)
        {
            DenyLogin("Invalid password.", ep);
            return;
        }

        if (FindConnection(ep) != -1)
        {
            DenyLogin("Identical connection already exists.", ep);
            return;
        }

        if ((FindConnection(msgLogin) != -1) || ((hostCredentials != null) && (hostCredentials.Login == msgLogin)))
        {
            DenyLogin("Player with identical login already connected.", ep);
            return;
        }

        clients.Add(MakeConnection(logMsg, ep));
        NetObjMgr.instance.HandleLogin(logMsg, ep);
    }

    /// <summary>
    /// Checks the provided host credentials if host will be client too. It performs check for:
    /// -User login existence.
    /// -Login-Password match.
    /// </summary>
    /// <param name="providedPlayerData">The provided player data for host.</param>
    /// <param name="message">Returned message of check.</param>
    /// <returns><c>true</c> if credentials meet conditions (known login and matched password), <c>false</c> otherwise.</returns>
    public bool CheckHostCredentials(PlayerData providedPlayerData, out string message)
    {
        if (registeredUsers.ContainsKey(providedPlayerData.Login) == false)
        {
            message = "User with provided login not found.";
            return false;
        }

        if (registeredUsers[providedPlayerData.Login] != providedPlayerData.Hashword)
        {
            message = "Invalid password.";
            return false;
        }

        hostCredentials = providedPlayerData;
        message = "OK";
        return true;
    }

    /// <summary>
    /// Denies the login for client that tries to connect.
    /// </summary>
    /// <param name="reason">Denial reason.</param>
    /// <param name="ep">Client IPEndPoint.</param>
    private void DenyLogin(string reason, IPEndPoint ep)
    {
        MsgKick msgKick = new MsgKick();
        msgKick.harmless = false;
        msgKick.reason = reason.ToCharArray();
        SendToClient(msgKick, ep);
        return;
    }

    /// <summary>
    /// Handles the confirm message.
    /// </summary>
    /// <param name="msg">Received confirm message.</param>
    /// <param name="ep">Source of confirm message.</param>
    private void HandleConfirm(Message msg, IPEndPoint ep)
    {
        int id = FindConnection(ep);
        if (id == -1)
        {
            return;
        }

        MsgConfirm msgConfirm = (MsgConfirm)msg;
        
        MsgSendInfo info = unconfirmedMsgs[ep.ToString()]
            .FirstOrDefault(x => x.msg.timestamp == msgConfirm.confirmStamp);
        
        if (info == null)
        {
            return;
        }
        unconfirmedMsgs[ep.ToString()].Remove(info);
    }

    /// <summary>
    /// Determines whether specific message from specific source is in received messages history.
    /// </summary>
    /// <param name="msgInfo">Message with source.</param>
    /// <returns><c>true</c> if provided message from provided soruce was found in history; otherwise, <c>false</c>.</returns>
    private bool IsInHistory(MsgSendInfo msgInfo)
    {
        foreach(MsgSendInfo info in receivedHistory)
            if (info != null && msgInfo.msg.timestamp == info.msg.timestamp && info.ep.Equals(msgInfo.ep))
                return true;
        return false;
    }

    #endregion

    #region managing connections

    /// <summary>
    /// Checks if user with provided login is already connected.
    /// </summary>
    /// <param name="login">Client login.</param>
    /// <returns>System.Int32. - Connection index, or -1 if no such client is connected.</returns>
    private int FindConnection(string login)
    {
        Connection client = clients.FirstOrDefault(x => x.login == login);

        if (client == null) return -1;
        else return clients.IndexOf(client);
    }

    /// <summary>
    /// Checks if provided IPEndPoint connection is no already established.
    /// </summary>
    /// <param name="ep">IPEndPoint to check.</param>
    /// <returns>System.Int32. - Connection index, or -1 if no such connectio exists.</returns>
    private int FindConnection(IPEndPoint ep)
    {
        return clients.FindIndex(x => x.ep.Equals(ep));
    }

    /// <summary>
    /// Makes connection to client.
    /// </summary>
    /// <param name="logMsg">Login message.</param>
    /// <param name="ep">Source IPEndPoint.</param>
    /// <returns>Created connection.</returns>
    private Connection MakeConnection(MsgLogin logMsg, IPEndPoint ep)
    {
        unconfirmedMsgs.Add(ep.ToString(), new List<MsgSendInfo>());

        Connection c = new Connection();
        c.login = new string(logMsg.login);
        c.hashword = new string(logMsg.hashword);
        c.ep = ep;
        return c;
    }

    /// <summary>
    /// Kicks the specified client by his IPEndPoint.
    /// </summary>
    /// <param name="ep">Client IPEndPoint.</param>
    /// <param name="harmless">Indicates whether kick is harmless <c>true</c> or not <c>false</c>.</param>
    /// <param name="reason">Kick reason.</param>
    public void Kick(IPEndPoint ep, bool harmless, string reason)
    {
        Kick(clients[FindConnection(ep)], harmless, reason);
    }

    /// <summary>
    /// Kicks the specified client by his connection.
    /// </summary>
    /// <param name="c">Client connection.</param>
    /// <param name="harmless">Indicates whether kick is harmless <c>true</c> or not <c>false</c>.</param>
    /// <param name="reason">Kick reason.</param>
    public void Kick(Connection c, bool harmless, string reason)
    {
        MsgKick kick = new MsgKick();
        kick.kickedPlayerNetID = NetObjMgr.instance.playersNetIDWithIPEndPoints.FirstOrDefault(playerWithIPEndPoint => playerWithIPEndPoint.Value.Equals(c.ep)).Key;
        kick.harmless = harmless;
        kick.reason = reason.ToCharArray();

        msgsToSend.RemoveAll(x => x.ep.Equals(c.ep));
        receivedMsgs.RemoveAll(x => x.ep.Equals(c.ep));
        SendToClient(kick, c.ep);
        unconfirmedMsgs.Remove(c.ep.ToString());
        clients.Remove(c);

        MsgKick localKick = new MsgKick();
        localKick.kickedPlayerNetID = kick.kickedPlayerNetID;
        localKick.harmless = harmless;
        localKick.reason = reason.ToCharArray();
        localKick.Encode();
        ProcessLocalMsg(new MsgSendInfo(localKick, c.ep));
    }
    #endregion
}