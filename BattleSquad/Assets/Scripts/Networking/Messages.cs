﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with all available Message classes defined.</summary>
// ***********************************************************************
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

/// <summary>
/// Base message class.
/// </summary>
public abstract class Message
{
    /// <summary>
    /// Message id. Incremented for each new created message.
    /// </summary>
    private static Int64 idCounter;

    /// <summary>
    /// Gets the type of message.
    /// </summary>
    /// <value>The type of message.</value>
    public abstract byte msgType { get; }

    /// <summary>
    /// Message timestamp.
    /// </summary>
    public Int64 timestamp;

    /// <summary>
    /// Message buffer - contains encoded message fields if message has been encoded.
    /// </summary>
    public byte[] buffer;

    /// <summary>
    /// Encodes this message i.e. uses subclass fields to fill byte buffer.
    /// </summary>
    public abstract void Encode();

    /// <summary>
    /// Decodes this instance i.e. uses byte buffer to fill subclass fields.
    /// </summary>
    public abstract void Decode();

    /// <summary>
    /// Initializes a new instance of the <see cref="Message"/> class. Timestamp is being assigned with curreent idCounter.
    /// </summary>
    public Message()
    {
        timestamp = idCounter++;
    }

    /// <summary>
    /// Creates message of appriopriate type based on buffer received value.
    /// </summary>
    /// <param name="buffer">Received data buffer.</param>
    /// <returns>Message of apprippriate type.</returns>
    /// <exception cref="System.Exception">Invalid msgType if wrong message type byte in buffer.</exception>
    public static Message MakeFromBuffer(byte[] buffer)
    {
        Message result;
        switch(buffer[0])
        {
            case 0: result = new MsgLogin(); break;
            case 1: result = new MsgTransform(); break;
            case 2: result = new MsgSpawn(); break;
            case 3: result = new MsgConfirm(); break;
            case 4: result = new MsgSpawnPlayer(); break;
            case 5: result = new MsgKick(); break;
            case 6:
                {
                    int syncVarTypeAsInt = StructTools.RawDeserialize<int>(buffer, 1);
                    if (syncVarTypeAsInt == SyncType.SyncTypeTypeToInt(typeof(string)))
                    {
                        result = new MsgSyncVarString();
                    }
                    else
                    {
                        Type syncVarType = SyncType.SyncTypeIntToType(syncVarTypeAsInt);
                        Type genericSyncVarMessageType = typeof(MsgSyncVar<>);
                        Type constructedClass = genericSyncVarMessageType.MakeGenericType(syncVarType);
                        result = (MsgSyncVar)Activator.CreateInstance(constructedClass);
                    }
                    break;
                }
            case 7:
                {
                    int rpcParameterTypeAsInt = StructTools.RawDeserialize<int>(buffer, 1);
                    if (rpcParameterTypeAsInt == 0)
                    {
                        result = new MsgRpcCallAction();
                    }
                    else
                    {
                        Type rpcParameterType = SyncType.SyncTypeIntToType(rpcParameterTypeAsInt);
                        Type genericRpcMessageType = typeof(MsgRpcCallActionWithParameter<>);
                        Type constructedClass = genericRpcMessageType.MakeGenericType(rpcParameterType);
                        result = (MsgRpcCallAction)Activator.CreateInstance(constructedClass);
                    }
                    break;
                }
            case 8: result = new MsgDeSpawn(); break;
            case 9: result = new MsgDisconnect(); break;
            default: throw new Exception("Invalid msgType!");
        }

        result.buffer = buffer;
        result.Decode();
        return result;
    }
}

/// <summary>
/// Class MsgLogin - responsible for clients to login.
/// </summary>
public class MsgLogin : Message
{
    /// <summary>
    /// Gets the type of the messsage.
    /// </summary>
    /// <value>The type of the messsage.</value>
    public override byte msgType { get { return 0; } }

    /// <summary>
    /// The login char array.
    /// </summary>
    public char[] login;

    /// <summary>
    /// The hashword char array.
    /// </summary>
    public char[] hashword;


    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(login.Length);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        for (int i = 0; i < login.Length; ++i)
        {
            tmpBytes = StructTools.RawSerialize(login[i]);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        tmpBytes = StructTools.RawSerialize(hashword.Length);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        for (int i = 0; i < hashword.Length; ++i)
        {
            tmpBytes = StructTools.RawSerialize(hashword[i]);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());


        login = new char[StructTools.RawDeserialize<int>(buffer, pos)];
        pos += Marshal.SizeOf(login.Length.GetType());

        for (int i = 0; i < login.Length; ++i)
        {
            login[i] = StructTools.RawDeserialize<char>(buffer, pos);
            pos += Marshal.SizeOf(login[i].GetType());
        }


        hashword = new char[StructTools.RawDeserialize<int>(buffer, pos)];
        pos += Marshal.SizeOf(hashword.Length.GetType());

        for (int i = 0; i < hashword.Length; ++i)
        {
            hashword[i] = StructTools.RawDeserialize<char>(buffer, pos);
            pos += Marshal.SizeOf(hashword[i].GetType());
        }

    }
}

/// <summary>
/// Class MsgTransform - responsible for handling data about gameObject position, rotation and velocity if required.
/// </summary>
public class MsgTransform : Message
{
    /// <summary>
    /// Gets the type of the messsage.
    /// </summary>
    /// <value>The type of the messsage.</value>
    public override byte msgType { get { return 1; } }

    /// <summary>
    /// The net identifier of synchronized NetObj.
    /// </summary>
    public UInt16 netID;
    /// <summary>
    /// The position dirty flag.
    /// </summary>
    public bool posDirty;
    /// <summary>
    /// The rotation dirty flag.
    /// </summary>
    public bool rotDirty;
    /// <summary>
    /// The velocity dirty flag.
    /// </summary>
    public bool velDirty;

    /// <summary>
    /// The saved position
    /// </summary>
    public Vector3 position;
    /// <summary>
    /// The saved rotation
    /// </summary>
    public Quaternion rotation;
    /// <summary>
    /// The saved velocity
    /// </summary>
    public Vector3 velocity;


    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        stream.WriteByte((byte)(posDirty ? 1 : 0));
        stream.WriteByte((byte)(rotDirty ? 1 : 0));
        stream.WriteByte((byte)(velDirty ? 1 : 0));

        if (posDirty)
        {
            tmpBytes = StructTools.RawSerialize(position);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }
        if (rotDirty)
        {
            tmpBytes = StructTools.RawSerialize(rotation);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }
        if (velDirty)
        {
            tmpBytes = StructTools.RawSerialize(velocity);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netID.GetType());
        posDirty = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        rotDirty = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        velDirty = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        if (posDirty)
        {
            position = StructTools.RawDeserialize<Vector3>(buffer, pos);
            pos += Marshal.SizeOf(position.GetType());
        }
        if (rotDirty)
        {
            rotation = StructTools.RawDeserialize<Quaternion>(buffer, pos);
            pos += Marshal.SizeOf(rotation.GetType());
        }
        if (velDirty)
        {
            velocity = StructTools.RawDeserialize<Vector3>(buffer, pos);
            pos += Marshal.SizeOf(velocity.GetType());
        }
    }
}

/// <summary>
/// Class MsgSpawn - responsible for spawning synchronized items on clients.
/// </summary>
public class MsgSpawn : Message
{
    /// <summary>
    /// Gets the type of the messsage.
    /// </summary>
    /// <value>The type of the messsage.</value>
    public override byte msgType { get { return 2; } }

    /// <summary>
    /// The net identifier of spawned object.
    /// </summary>
    public UInt16 netID;

    /// <summary>
    /// The spawnable identifier of spawned object.
    /// </summary>
    public UInt16 spawnableID;

    /// <summary>
    /// The owning player net identifier
    /// </summary>
    public UInt16 owningPlayerNetID;

    /// <summary>
    /// The position of spawn.
    /// </summary>
    public Vector3 position;

    /// <summary>
    /// The rotation of spawn.
    /// </summary>
    public Quaternion rotation;

    /// <summary>
    /// If velocity is being send flag
    /// </summary>
    public bool syncVelocity;

    /// <summary>
    /// The velocity of spawn
    /// </summary>
    public Vector3 velocity;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(spawnableID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(owningPlayerNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(position);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(rotation);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(syncVelocity);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        if (syncVelocity)
        {
            tmpBytes = StructTools.RawSerialize(velocity);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netID.GetType());
        spawnableID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(spawnableID.GetType());
        owningPlayerNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(owningPlayerNetID.GetType());

        position = StructTools.RawDeserialize<Vector3>(buffer, pos);
        pos += Marshal.SizeOf(position.GetType());
        rotation = StructTools.RawDeserialize<Quaternion>(buffer, pos);
        pos += Marshal.SizeOf(rotation.GetType());

        syncVelocity = StructTools.RawDeserialize<bool>(buffer, pos);
        pos += Marshal.SizeOf(syncVelocity.GetType());

        if (syncVelocity)
        {
            velocity = StructTools.RawDeserialize<Vector3>(buffer, pos);
            pos += Marshal.SizeOf(velocity.GetType());
        }
    }
}

/// <summary>
/// Class MsgConfirm - responsible for confirming message receivement.
/// </summary>
public class MsgConfirm : Message
{
    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 3; } }

    /// <summary>
    /// The confirmed message stamp
    /// </summary>
    public Int64 confirmStamp;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(confirmStamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        timestamp = StructTools.RawDeserialize<Int64>(buffer, 1);
        confirmStamp = StructTools.RawDeserialize<Int64>(buffer, 9);
    }
}

/// <summary>
/// Class MsgSpawnPlayer - responsible for player spawning
/// </summary>
public class MsgSpawnPlayer : Message
{
    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 4; } }

    /// <summary>
    /// The net identifier of spawned player.
    /// </summary>
    public UInt16 netID;
    /// <summary>
    /// The spawnable identifier of spawned player.
    /// </summary>
    public UInt16 spawnableID;
    /// <summary>
    /// Flag whether set spawned player to local player.
    /// </summary>
    public bool setLocalPlayer;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(spawnableID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(setLocalPlayer);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netID.GetType());
        spawnableID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(spawnableID.GetType());
        setLocalPlayer = StructTools.RawDeserialize<bool>(buffer, pos);
        pos += Marshal.SizeOf(setLocalPlayer.GetType());
    }
}

/// <summary>
/// Class MsgKick - reponsible for disconnecting client or signalizing host that client has been disconected.
/// </summary>
public class MsgKick : Message
{
    /// <summary>
    /// The kicked player net identifier
    /// </summary>
    public UInt16 kickedPlayerNetID;
    /// <summary>
    /// The harmless flag
    /// </summary>
    public bool harmless;
    /// <summary>
    /// The reason char array
    /// </summary>
    public char[] reason;

    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 5; } }

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(kickedPlayerNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(harmless);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(reason.Length);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        for (int i = 0; i < reason.Length; ++i)
        {
            tmpBytes = StructTools.RawSerialize(reason[i]);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        kickedPlayerNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(kickedPlayerNetID.GetType());
        harmless = StructTools.RawDeserialize<bool>(buffer, pos);
        pos += Marshal.SizeOf(harmless.GetType());

        reason = new char[StructTools.RawDeserialize<int>(buffer, pos)];
        pos += Marshal.SizeOf(reason.Length.GetType());

        for (int i = 0; i < reason.Length; ++i)
        {
            reason[i] = StructTools.RawDeserialize<char>(buffer, pos);
            pos += Marshal.SizeOf(reason[i].GetType());
        }
    }
}

/// <summary>
/// Base MsgSyncVar class - responsible for synchronizing SyncVar values.
/// </summary>
public abstract class MsgSyncVar : Message
{
    /// <summary>
    /// The synchronize variable type as int.
    /// </summary>
    public int syncVarTypeAsInt;

    /// <summary>
    /// The net object net identifier.
    /// </summary>
    public UInt16 netObjNetID;

    /// <summary>
    /// The synchronize variable index.
    /// </summary>
    public int syncVarIndex;

    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 6; } }
}

/// <summary>
/// Generic MsgSyncVar class.
/// </summary>
/// <typeparam name="T">Type of SyncVar</typeparam>
public class MsgSyncVar<T> : MsgSyncVar
{
    /// <summary>
    /// Synchronized value.
    /// </summary>
    public T value;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(syncVarTypeAsInt);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netObjNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(syncVarIndex);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(value);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        syncVarTypeAsInt = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarTypeAsInt.GetType());
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netObjNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netObjNetID.GetType());
        syncVarIndex = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarIndex.GetType());

        value = StructTools.RawDeserialize<T>(buffer, pos);
        pos += Marshal.SizeOf(value.GetType());
    }
}

/// <summary>
/// Class MsgSyncVarString - responsible for synchronizing SyncVarString class instance.
/// </summary>
public class MsgSyncVarString : MsgSyncVar
{
    /// <summary>
    /// The string value as char array.
    /// </summary>
    public char[] value;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(syncVarTypeAsInt);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netObjNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(syncVarIndex);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(value.Length);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        for (int i = 0; i < value.Length; ++i)
        {
            tmpBytes = StructTools.RawSerialize(value[i]);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        syncVarTypeAsInt = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarTypeAsInt.GetType());
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netObjNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netObjNetID.GetType());
        syncVarIndex = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarIndex.GetType());

        value = new char[StructTools.RawDeserialize<int>(buffer, pos)];
        pos += Marshal.SizeOf(value.Length.GetType());

        for (int i = 0; i < value.Length; ++i)
        {
            value[i] = StructTools.RawDeserialize<char>(buffer, pos);
            pos += Marshal.SizeOf(value[i].GetType());
        }
    }
}

/// <summary>
/// Class MsgRpcCallAction - responsible for handling RPC calls.
/// </summary>
public class MsgRpcCallAction : Message
{
    /// <summary>
    /// The synchronized variable type as int - applies to generic messages only.
    /// </summary>
    public int syncVarTypeAsInt;

    /// <summary>
    /// The net object net identifier.
    /// </summary>
    public UInt16 netObjNetID;

    /// <summary>
    /// The RPC index.
    /// </summary>
    public int rpcIndex;

    /// <summary>
    /// The invoked correctly flag
    /// </summary>
    public bool invokedCorrectly;

    /// <summary>
    /// Gets the type of the mmessage.
    /// </summary>
    /// <value>The type of the mmessage.</value>
    public override byte msgType { get { return 7; } }

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(syncVarTypeAsInt);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netObjNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(rpcIndex);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(invokedCorrectly);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        syncVarTypeAsInt = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarTypeAsInt.GetType());
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netObjNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netObjNetID.GetType());
        rpcIndex = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(rpcIndex.GetType());
        invokedCorrectly = StructTools.RawDeserialize<bool>(buffer, pos);
        pos += Marshal.SizeOf(invokedCorrectly.GetType());
    }
}

/// <summary>
/// Class MsgRpcCallActionWithParameter - respongisble for RPC calls with passed parameter.
/// </summary>
/// <typeparam name="T">Parameter type.</typeparam>
public class MsgRpcCallActionWithParameter<T> : MsgRpcCallAction
{
    /// <summary>
    /// The parameter value.
    /// </summary>
    public T value;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(syncVarTypeAsInt);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netObjNetID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(rpcIndex);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(invokedCorrectly);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        tmpBytes = StructTools.RawSerialize(value);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        syncVarTypeAsInt = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(syncVarTypeAsInt.GetType());
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netObjNetID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netObjNetID.GetType());
        rpcIndex = StructTools.RawDeserialize<int>(buffer, pos);
        pos += Marshal.SizeOf(rpcIndex.GetType());
        invokedCorrectly = StructTools.RawDeserialize<bool>(buffer, pos);
        pos += Marshal.SizeOf(invokedCorrectly.GetType());

        value = StructTools.RawDeserialize<T>(buffer, pos);
        pos += Marshal.SizeOf(value.GetType());
    }
}

/// <summary>
/// Class MsgDeSpawn - reponsible for despawning items on clients.
/// </summary>
public class MsgDeSpawn : Message
{
    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 8; } }

    /// <summary>
    /// The net identifier.
    /// </summary>
    public UInt16 netID;

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netID.GetType());
    }
}

/// <summary>
/// Class MsgDisconnect - responsible for requests of disconnection.
/// </summary>
public class MsgDisconnect : Message
{
    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <value>The type of the message.</value>
    public override byte msgType { get { return 9; } }

    /// <summary>
    /// Encodes this instance.
    /// </summary>
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    /// <summary>
    /// Decodes this instance.
    /// </summary>
    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
    }
}

/// <summary>
/// Class StructTools - tolls for serializing data.
/// </summary>
public static class StructTools
{
    /// <summary>
    /// Deserialized object from raw data from provided position.
    /// </summary>
    /// <typeparam name="T">Data type.</typeparam>
    /// <param name="rawData">The raw data buffer.</param>
    /// <param name="position">The position from start deserialization.</param>
    /// <returns>Deserialized object.</returns>
    /// <exception cref="System.ArgumentException">Not enough data to fill struct.</exception>
    public static T RawDeserialize<T>(byte[] rawData, int position)
    {
        int rawsize = Marshal.SizeOf(typeof(T));
        if (rawsize > rawData.Length - position)
            throw new ArgumentException("Not enough data to fill struct. Array length from position: " + (rawData.Length - position) + ", Struct length: " + rawsize);
        IntPtr buffer = Marshal.AllocHGlobal(rawsize);
        Marshal.Copy(rawData, position, buffer, rawsize);
        T retobj = (T)Marshal.PtrToStructure(buffer, typeof(T));
        Marshal.FreeHGlobal(buffer);
        return retobj;
    }

    /// <summary>
    /// Serializes object to raw data.
    /// </summary>
    /// <param name="anything">Object to serialize.</param>
    /// <returns>Serialized object as byte array.</returns>
    public static byte[] RawSerialize(object anything)
    {
        int rawSize = Marshal.SizeOf(anything);
        IntPtr buffer = Marshal.AllocHGlobal(rawSize);
        Marshal.StructureToPtr(anything, buffer, false);
        byte[] rawDatas = new byte[rawSize];
        Marshal.Copy(buffer, rawDatas, 0, rawSize);
        Marshal.FreeHGlobal(buffer);
        return rawDatas;
    }
}