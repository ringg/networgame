﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with usefull conection-management classes.</summary>
// ***********************************************************************
using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

/// <summary>
/// Class Connection - handles client connection info.
/// </summary>
public class Connection
{
    /// <summary>
    /// Client login.
    /// </summary>
    public string login;
    /// <summary>
    /// Client hashword (SHA256 hash calculated from ASCI password bytes).
    /// </summary>
    public string hashword;
    /// <summary>
    /// Client IPEndPoint.
    /// </summary>
    public IPEndPoint ep;
}

/// <summary>
/// Class MsgSendInfo - handles message and message source IPEndPoint.
/// </summary>
public class MsgSendInfo
{
    /// <summary>
    /// Current game time.
    /// </summary>
    public static float gameTime;

    /// <summary>
    /// Initializes a new instance of the <see cref="MsgSendInfo"/> class.
    /// </summary>
    /// <param name="msg">Message.</param>
    /// <param name="ep">IPEndPoint.</param>
    public MsgSendInfo(Message msg, IPEndPoint ep)
    {
        this.msg = msg;
        this.ep = ep;
        this.firstSendTime = gameTime;
        this.lastSendTime = gameTime;
    }

    /// <summary>
    /// Message.
    /// </summary>
    public Message msg;

    /// <summary>
    /// IPEndPoint.
    /// </summary>
    public IPEndPoint ep;

    /// <summary>
    /// The first send time.
    /// </summary>
    public float firstSendTime;

    /// <summary>
    /// The last send time.
    /// </summary>
    public float lastSendTime;
}

/// <summary>
/// Class IpHelper - usefull functions for IP Adresses handling.
/// </summary>
public static class IpHelper
{
    /// <summary>
    /// Gets the local ip address.
    /// </summary>
    /// <returns>Local IP Adress.</returns>
    /// <exception cref="System.Exception">If local IP Address was not found.</exception>
    public static IPAddress GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                return ip;
        throw new Exception("Local IP Address Not Found!");
    }
}