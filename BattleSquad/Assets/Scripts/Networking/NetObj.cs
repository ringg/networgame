﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with NetObj class defined.</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class NetObj. It represents object synchronized over network.
/// </summary>
public class NetObj : MonoBehaviour
{
    /// <summary>
    /// The net identifier
    /// </summary>
    [InspectorReadOnly]
    public UInt16 netID;

    /// <summary>
    /// The spawnable identifier
    /// </summary>
    [InspectorReadOnly]
    public UInt16 spawnableID;

    /// <summary>
    /// The initialization flag.
    /// </summary>
    [InspectorReadOnly]
    public bool wasInit;

    /// <summary>
    /// The is server flag.
    /// </summary>
    [InspectorReadOnly]
    public bool isServer;

    /// <summary>
    /// The owning player net identifier
    /// </summary>
    [InspectorReadOnly]
    public UInt16 owningPlayerNetID;

    /// <summary>
    /// The position dirty flag. Important only on server.
    /// </summary>
    [InspectorReadOnly]
    public bool posDirty;

    /// <summary>
    /// The rotation dirty flag. Important only on server.
    /// </summary>
    [InspectorReadOnly]
    public bool rotDirty;

    /// <summary>
    /// The velocity dirty flag. Important only on server.
    /// </summary>
    [InspectorReadOnly]
    public bool velDirty;

    /// <summary>
    /// List of this netObj syncVars.
    /// </summary>
    [NonSerialized]
    public List<SyncVar> syncVars;

    /// <summary>
    /// List of this netObj syncRpcs.
    /// </summary>
    [NonSerialized]
    public List<SyncRPC> syncRPCs;


    /// <summary>
    /// The default rubberband position.
    /// </summary>
    [SerializeField]
    private float defaultRubberbandPosition = 0.2F;
    /// <summary>
    /// Gets the default rubberband position.
    /// </summary>
    /// <value>The default rubberband position.</value>
    public float DefaultRubberbandPosition { get { return defaultRubberbandPosition; } }

    /// <summary>
    /// The default rubberband rotation.
    /// </summary>
    [SerializeField]
    private float defaultRubberbandRotation = 0.2F;
    /// <summary>
    /// Gets the default rubberband rotation.
    /// </summary>
    /// <value>The default rubberband rotation.</value>
    public float DefaultRubberbandRotation { get { return defaultRubberbandRotation; } }

    /// <summary>
    /// The default rubberband velocity.
    /// </summary>
    [SerializeField]
    private float defaultRubberbandVelocity = 1.0F;
    /// <summary>
    /// Gets the default rubberband velocity.
    /// </summary>
    /// <value>The default rubberband velocity.</value>
    public float DefaultRubberbandVelocity { get { return defaultRubberbandVelocity; } }

    /// <summary>
    /// Gets or sets the rubberband position.
    /// </summary>
    /// <value>The rubberband position.</value>
    public float RubberbandPosition { get; set; }
    /// <summary>
    /// Gets or sets the rubberband rotation.
    /// </summary>
    /// <value>The rubberband rotation.</value>
    public float RubberbandRotation { get; set; }
    /// <summary>
    /// Gets or sets the rubberband velocity.
    /// </summary>
    /// <value>The rubberband velocity.</value>
    public float RubberbandVelocity { get; set; }

    /// <summary>
    /// The synchronized position.
    /// </summary>
    private Vector3 synchronizedPosition;
    /// <summary>
    /// Gets the synchronized position.
    /// </summary>
    /// <value>The synchronized position.</value>
    public Vector3 SynchronizedPosition { get { return synchronizedPosition; } }

    /// <summary>
    /// The synchronized rotation.
    /// </summary>
    private Quaternion synchronizedRotation;
    /// <summary>
    /// Gets the synchronized rotation.
    /// </summary>
    /// <value>The synchronized rotation.</value>
    public Quaternion SynchronizedRotation { get { return synchronizedRotation; } }

    /// <summary>
    /// The synchronized velocity.
    /// </summary>
    private Vector3 synchronizedVelocity;
    /// <summary>
    /// Gets the synchronized velocity.
    /// </summary>
    /// <value>The synchronized velocity.</value>
    public Vector3 SynchronizedVelocity { get { return synchronizedVelocity; } }

    /// <summary>
    /// The rigidbody component.
    /// </summary>
    private Rigidbody rigidbodyComponent;
    /// <summary>
    /// Gets the rigidbody component.
    /// </summary>
    /// <value>The rigidbody component.</value>
    public Rigidbody RigidbodyComponent { get { return rigidbodyComponent; } }

    /// <summary>
    /// The synchronize rigidbody flag.
    /// </summary>
    private bool syncRB = true;

    /// <summary>
    /// Gets a value indicating whether to synchronize rigidbody (whether rigidbody component is present on gameObject).
    /// </summary>
    /// <value><c>true</c> if rigidbody component is present; otherwise, <c>false</c>.</value>
    public bool SyncRB { get { return syncRB; } }

    /// <summary>
    /// Gets a value indicating whether this instance is owned by local player.
    /// </summary>
    /// <value><c>true</c> if this instance is owned by local player; otherwise, <c>false</c>.</value>
    public bool isOwnedByLocalPlayer
    {
        get
        {
            return SyncPlayer.localPlayer == null ? (NetObjMgr.instance.IsServer && owningPlayerNetID == 0) : (owningPlayerNetID == SyncPlayer.localPlayer.NetObj.netID);
        }
    }

    /// <summary>
    /// The last synchronized timestamp.
    /// </summary>
    Int64 lastTimestamp;

    /// <summary>
    /// Unity build-in Awake function. Sets rubberband values and gets rigidbody component with syncRB flag set.
    /// </summary>
    private void Awake()
    {
        RubberbandPosition = defaultRubberbandPosition;
        RubberbandRotation = defaultRubberbandRotation;
        RubberbandVelocity = defaultRubberbandVelocity;
        rigidbodyComponent = GetComponent<Rigidbody>();
        syncRB = (rigidbodyComponent != null);
    }

    /// <summary>
    /// Unity build-in LateUpdate function. If netObj is on server, synchronizes dirty values. Otherwise, interpolates current values to synchronized ones.
    /// </summary>
    private void LateUpdate()
    {
        if (wasInit == false)
        {
            return;
        }
        if (isServer)
        {
            posDirty |= transform.position != synchronizedPosition;
            synchronizedPosition = transform.position;
            rotDirty |= transform.rotation != synchronizedRotation;
            synchronizedRotation = transform.rotation;
            if (syncRB)
            {
                velDirty |= rigidbodyComponent.velocity != synchronizedVelocity;
                synchronizedVelocity = rigidbodyComponent.velocity;
            }
        }
        else
        {
            transform.position =              Vector3.Lerp    (transform.position,          synchronizedPosition, RubberbandPosition);
            transform.rotation =              Quaternion.Slerp(transform.rotation,          synchronizedRotation, RubberbandRotation);
            if (syncRB)
            {
                rigidbodyComponent.velocity = Vector3.Lerp    (rigidbodyComponent.velocity, synchronizedVelocity, RubberbandVelocity);
            }
        }      
    }

    /// <summary>
    /// Initializes netObj.
    /// </summary>
    /// <param name="netID">The net identifier.</param>
    /// <param name="spawnableID">The spawnable identifier.</param>
    /// <param name="isServer">Is server flag.</param>
    /// <param name="owningPlayerNetID">The owning player net identifier.</param>
    public void Init(UInt16 netID, UInt16 spawnableID, bool isServer, UInt16 owningPlayerNetID)
    {
        syncVars = new List<SyncVar>();
        syncRPCs = new List<SyncRPC>();
        this.netID = netID;
        this.spawnableID = spawnableID;
        this.isServer = isServer;
        this.owningPlayerNetID = owningPlayerNetID;
        wasInit = true;
        posDirty = false;
        rotDirty = false;
        velDirty = false;
        synchronizedPosition = transform.position;
        synchronizedRotation = transform.rotation;
        if (syncRB)
        {
            synchronizedVelocity = rigidbodyComponent.velocity;
        }
        SyncBehaviour[] synchronizedBehaviours = GetComponents<SyncBehaviour>();
        foreach (SyncBehaviour synchronizedBehaviour in synchronizedBehaviours)
        {
            synchronizedBehaviour.Init(this);
        }
    }

    /// <summary>
    /// Adjusts the netObj synchronized values.
    /// </summary>
    /// <param name="msg">MsgTransform message.</param>
    public virtual void Adjust(MsgTransform msg)
    {
        if (msg.timestamp < lastTimestamp) return;
        lastTimestamp = msg.timestamp;

        if (msg.posDirty) synchronizedPosition = msg.position;
        if (msg.rotDirty) synchronizedRotation = msg.rotation;
        if (syncRB && msg.velDirty) synchronizedVelocity = msg.velocity;
    }
}
