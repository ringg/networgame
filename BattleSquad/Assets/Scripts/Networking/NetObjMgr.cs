﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with NetObjMgr class defined.</summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Enum QuitMode - how to quit from game.
/// </summary>
public enum QuitMode
{
    /// <summary>
    /// The quit to main menu
    /// </summary>
    QuitToMainMenu,
    /// <summary>
    /// The exit game to windows.
    /// </summary>
    ExitGame,
    /// <summary>
    /// Exit requested by host.
    /// </summary>
    RequestedByHost
}

/// <summary>
/// Class NetObjMgr - responsible for synchronizing objects between clients and server.
/// </summary>
public class NetObjMgr : MonoBehaviour
{
    #region variables

    /// <summary>
    /// Gets the instance.
    /// </summary>
    /// <value>The instance.</value>
    public static NetObjMgr instance { get; private set; }

    /// <summary>
    /// The spawnables
    /// </summary>
    [SerializeField]
    private List<NetObj> spawnables = null;
    /// <summary>
    /// The spawnable player prefab
    /// </summary>
    [SerializeField]
    private SyncPlayer spawnablePlayerPrefab = null;
    /// <summary>
    /// The offline scene name
    /// </summary>
    [SerializeField]
    private string offlineSceneName = null;
    /// <summary>
    /// The offline scene error name
    /// </summary>
    [SerializeField]
    private string offlineSceneErrorName = null;

    #region bothHostAndClientContainers

    /// <summary>
    /// Players list.
    /// </summary>
    [NonSerialized]
    public Dictionary<UInt16, SyncPlayer> players = new Dictionary<UInt16, SyncPlayer>();

    /// <summary>
    /// NetObjs list.
    /// </summary>
    private Dictionary<UInt16, NetObj> netObjs = new Dictionary<UInt16, NetObj>(); //<netid, obj>

    /// <summary>
    /// The delayed messages list.
    /// </summary>
    private List<MsgSendInfo> delayedMessages = new List<MsgSendInfo>();

    /// <summary>
    /// The deSpawned netIDs list.
    /// </summary>
    private List<UInt16> deSpawnedNetIDs = new List<UInt16>();

    #endregion

    #region hostContainers

    /// <summary>
    /// The messages to send to clients. Host only.
    /// </summary>
    [NonSerialized]
    public List<KeyValuePair<Message, IPEndPoint>> messagesToSendToClients;

    /// <summary>
    /// The players net identifier with ip end points. Host only.
    /// </summary>
    [NonSerialized]
    public Dictionary<UInt16, IPEndPoint> playersNetIDWithIPEndPoints;

    #endregion

    #region clientContainers

    /// <summary>
    /// The messages to send to host. Client only.
    /// </summary>
    [NonSerialized]
    public List<Message> messagesToSendToHost;

    #endregion


    /// <summary>
    /// Net settings.
    /// </summary>
    private NetSettings settings;

    /// <summary>
    /// Client component if user is client.
    /// </summary>
    private Client client;

    /// <summary>
    /// Host component if user is host.
    /// </summary>
    private Host host;

    /// <summary>
    /// The last uniq net identifier.
    /// </summary>
    private UInt16 lastUniqNetId;

    /// <summary>
    /// The message dispatch timer.
    /// </summary>
    private float msgDispatchTimer;

    /// <summary>
    /// Current quit mode.
    /// </summary>
    private QuitMode currentQuitMode;

    /// <summary>
    /// The is server being closed flag.
    /// </summary>
    private bool isServerBeingClosed = false;

    /// <summary>
    /// Gets a value indicating whether this instance is server being closed.
    /// </summary>
    /// <value><c>true</c> if this instance is server being closed; otherwise, <c>false</c>.</value>
    public bool IsServerBeingClosed { get { return isServerBeingClosed; } }

    /// <summary>
    /// The is server flag.
    /// </summary>
    private bool isServer;

    /// <summary>
    /// Gets a value indicating whether this instance is server.
    /// </summary>
    /// <value><c>true</c> if this instance is server; otherwise, <c>false</c>.</value>
    public bool IsServer { get { return isServer; } }

    #endregion

    /// <summary>
    /// Unity build-in Awake function. Gets component references and sets singleton instance.
    /// </summary>
    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(this);
            return;
        }

        instance = this;
        client = GetComponent<Client>();
        host = GetComponent<Host>();
        settings = GetComponent<NetSettings>();

        currentQuitMode = QuitMode.QuitToMainMenu;
    }

    /// <summary>
    /// Unity build-in Start function. Checks if user is host or client.
    /// If host:
    /// -checks for host credentials
    /// -spawns player if host is also client
    /// -initializes all netObjs in scene
    /// -sets messages handlers
    /// If client:
    /// -destroys all scene netObjs (will be received from host)
    /// -sets messages handlers
    /// </summary>
    /// <exception cref="System.InvalidOperationException">If no syncPlayer netObj prefab found in spawnables.</exception>
    private void Start()
    {
        List<NetObj> objs = FindObjectsOfType<NetObj>().ToList();
        if (host != null && host.IsHosting)
        {
            isServer = true;
            playersNetIDWithIPEndPoints = new Dictionary<UInt16, IPEndPoint>();
            UInt16 spawnableID;

            if (host.IsAlsoClient == false)
            {
                lastUniqNetId = 1;
            }
            else
            {
                string hostCheckMessage;
                if (host.CheckHostCredentials(Overlook.Instance.PlayerLoginData, out hostCheckMessage) == false)
                {
                    Overlook.Instance.ErrorMessageDisplay = hostCheckMessage;
                    SceneManager.LoadScene(offlineSceneErrorName);
                    return;
                }
                lastUniqNetId = 0;
                // Spawn player for us.
                SyncPlayer ourPlayer = Instantiate(spawnablePlayerPrefab);
                spawnableID = FindSpawnableID(ourPlayer.NetObj);
                if (spawnableID == UInt16.MaxValue)
                {
                    throw new InvalidOperationException("No syncPlayer netObj prefab found in spawnables!");
                }
                UInt16 ourPlayerNetId = lastUniqNetId++;
                ourPlayer.SetLocalPlayer();
                ourPlayer.NetObj.Init(ourPlayerNetId, spawnableID, true, ourPlayerNetId);
                netObjs.Add(ourPlayerNetId, ourPlayer.NetObj);
                playersNetIDWithIPEndPoints.Add(ourPlayerNetId, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11000));
            }

            // other preparations
            messagesToSendToClients = new List<KeyValuePair<Message, IPEndPoint>>();
            // catalogue all netObjs in scene
            for (UInt16 ID = 0; ID < objs.Count; ++ID)
            {
                spawnableID = FindSpawnableID(objs[ID]);

                if (spawnableID == UInt16.MaxValue)
                {
                    Destroy(objs[ID].gameObject);
                    objs.RemoveAt(ID);
                    --ID;
                    continue;
                }
                UInt16 ourObjectNetID = lastUniqNetId++;
                objs[ID].Init(ourObjectNetID, spawnableID, true, 0);
                netObjs.Add(ourObjectNetID, objs[ID]);
            }

            // start listening
            host.msgHandlers[5] += HandlePlayerBeingKicked;
            host.msgHandlers[6] += HandleSyncVarSet;
            host.msgHandlers[7] += HandleIncommingRPC;
            host.msgHandlers[9] += HandleDisconnect;

            EventManager.TriggerEvent(EventType.Connected);
        }
        else // we're just a client, clean up and wait for object info from host
        {
            host = null;
            isServer = false;
            messagesToSendToHost = new List<Message>();
            foreach (NetObj netObj in objs)
            {
                Destroy(netObj.gameObject);
            }

            client.msgHandlers[1] += HandleTransform;
            client.msgHandlers[2] += HandleSpawn;
            client.msgHandlers[4] += HandleSpawnPlayer;
            client.msgHandlers[5] += HandlePlayerKick;
            client.msgHandlers[6] += HandleSyncVar;
            client.msgHandlers[7] += HandleIncommingRPC;
            client.msgHandlers[8] += HandleDeSpawn;
            client.msgHandlers[9] += HandleDisconnectInfoFromServer;
        }
    }

    /// <summary>
    /// Unity build-in Update function. Calculates msgDispatch timer.
    /// If server - sends all unsynchronized data to clients - netObjs states, syncVars, syncRPCs, sends messages to clients.
    /// If client - sends messages to host.
    /// Whether server or client - processes delayed messages.
    /// </summary>
    private void Update()
    {
        msgDispatchTimer += Time.deltaTime;
        if (msgDispatchTimer < settings.SendInterval)
            return;
        msgDispatchTimer %= settings.SendInterval;

        List<MsgSendInfo> delayedMessagesTmp = new List<MsgSendInfo>(delayedMessages);
        delayedMessages.Clear();

        // server - update objects transforms and SyncVars
        if (isServer)
        {
            foreach (NetObj netObj in netObjs.Values)
            {
                if (netObj.posDirty || netObj.rotDirty || netObj.velDirty)
                {
                    MsgTransform msg = new MsgTransform();
                    msg.netID = netObj.netID;
                    msg.posDirty = netObj.posDirty;
                    if (msg.posDirty)
                    {
                        msg.position = netObj.transform.position;
                    }
                    msg.rotDirty = netObj.rotDirty;
                    if (msg.rotDirty)
                    {
                        msg.rotation = netObj.transform.rotation;
                    }
                    msg.velDirty = netObj.velDirty;
                    if (netObj.SyncRB && msg.velDirty)
                    {
                        msg.velocity = netObj.RigidbodyComponent.velocity;
                    }
                    host.SendToAllClients(msg);
                    netObj.posDirty = false;
                    netObj.rotDirty = false;
                    netObj.velDirty = false;
                }
                for (int i = 0; i < netObj.syncVars.Count; ++i)
                {
                    if (netObj.syncVars[i].isDirty)
                    {
                        MsgSyncVar msg = netObj.syncVars[i].CreateMessage();
                        host.SendToAllClients(msg);
                        netObj.syncVars[i].isDirty = false;
                    }
                }
            }
            // send any waiting messages
            foreach (KeyValuePair<Message, IPEndPoint> messageWithDestinationPoint in messagesToSendToClients)
            {
                if (messageWithDestinationPoint.Value != null)
                {
                    host.SendToClient(messageWithDestinationPoint.Key, messageWithDestinationPoint.Value);
                }
                else
                {
                    host.SendToAllClients(messageWithDestinationPoint.Key);

                    if (/*host.IsAlsoClient*/true)
                    {
                        // We are also client - examine message.
                        messageWithDestinationPoint.Key.Encode();
                        host.ProcessLocalMsg(new MsgSendInfo(messageWithDestinationPoint.Key, messageWithDestinationPoint.Value));
                    }
                }
            }
            messagesToSendToClients.Clear();
            foreach (MsgSendInfo delayedMessage in delayedMessagesTmp)
            {
                Debug.Log("Processing delayed message.");
                host.ProcessLocalMsg(delayedMessage);
            }
        }
        else // client - check for data to send to host
        {
            foreach (Message message in messagesToSendToHost)
            {
                client.SendToHost(message);
            }
            messagesToSendToHost.Clear();
            foreach (MsgSendInfo delayedMessage in delayedMessagesTmp)
            {
                Debug.Log("Processing delayed message.");
                client.ProcessLocalMsg(delayedMessage);
            }
        }
    }

    /// <summary>
    /// Spawns the specified net object.
    /// </summary>
    /// <param name="netObj">The net object prefab to spawn.</param>
    /// <param name="position">The spawn position.</param>
    /// <param name="rotation">The spawn rotation.</param>
    /// <param name="owningPlayerNetID">The owning player net identifier. (default - 0 - server will own object)</param>
    /// <returns>NetObj.</returns>
    public NetObj Spawn(NetObj netObj, Vector3 position, Quaternion rotation, UInt16 owningPlayerNetID = 0)
    {
        return Spawn(spawnables.FindIndex(x => x == netObj), position, rotation, owningPlayerNetID);
    }

    /// <summary>
    /// Spawns the specified net object by ins spawnableID. Can be called only on server.
    /// </summary>
    /// <param name="spawnableID">The spawnable object ID.</param>
    /// <param name="position">The spawn position.</param>
    /// <param name="rotation">The spawn rotation.</param>
    /// <param name="owningPlayerNetID">The owning player net identifier. (default - 0 - server will own object)</param>
    /// <returns>NetObj.</returns>
    /// <exception cref="System.InvalidOperationException">If spawn was called on client</exception>
    public NetObj Spawn(int spawnableID, Vector3 position, Quaternion rotation, UInt16 owningPlayerNetID = 0)
    {
        if (isServer == false)
        {
            throw new InvalidOperationException("Spawn can be called only on server!");
        }
        NetObj result;
        if (spawnableID < 0 || spawnableID >= spawnables.Count) return null;
        
        result = Instantiate(spawnables[spawnableID], position, rotation);
        UInt16 ourObjectNetID = lastUniqNetId++;
        result.Init(ourObjectNetID, (UInt16)spawnableID, true, owningPlayerNetID);
        netObjs.Add(ourObjectNetID, result);

        MsgSpawn msgSpawn = new MsgSpawn();
        msgSpawn.netID = ourObjectNetID;
        msgSpawn.spawnableID = (UInt16)spawnableID;
        msgSpawn.position = position;
        msgSpawn.rotation = rotation;
        msgSpawn.owningPlayerNetID = owningPlayerNetID;
        msgSpawn.syncVelocity = result.SyncRB;
        if (msgSpawn.syncVelocity)
        {
            msgSpawn.velocity = result.RigidbodyComponent.velocity;
        }
        host.SendToAllClients(msgSpawn);

        return result;
    }

    /// <summary>
    /// Despawn object. Can be called only on server.
    /// </summary>
    /// <param name="netID">The net identifier of despawned object.</param>
    /// <exception cref="System.InvalidOperationException">If DeSpawn was called on client.</exception>
    /// <exception cref="System.ArgumentException">If no netObj with provided netID found.</exception>
    public void DeSpawn(UInt16 netID)
    {
        if (isServer == false)
        {
            throw new InvalidOperationException("DeSpawn can be called only on server!");
        }
        if (netObjs.ContainsKey(netID) == false)
        {
            throw new ArgumentException("No netObj with provided netID found!");
        }
        Destroy(netObjs[netID].gameObject);
        netObjs.Remove(netID);
        deSpawnedNetIDs.Add(netID);

        MsgDeSpawn msgDeSpawn = new MsgDeSpawn();
        msgDeSpawn.netID = netID;
        host.SendToAllClients(msgDeSpawn);
    }

    /// <summary>
    /// Quits with specified quit mode. This starts disconnecting sequence - disconnections occurs only after host permission, or, if host, after all cleints disconnection.
    /// </summary>
    /// <param name="quitMode">The quit mode.</param>
    public void Quit(QuitMode quitMode)
    {
        currentQuitMode = quitMode;
        if (quitMode != QuitMode.RequestedByHost)
        {
            DisconnectingUI.Instance.Show(quitMode);
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        if (isServer)
        {
            MsgDisconnect msgDisconnect = new MsgDisconnect();
            host.SendToAllClients(msgDisconnect);
            StartCoroutine(HostQuitRoutine());
        }
        else
        {
            MsgDisconnect msgDisconnect = new MsgDisconnect();
            client.SendToHost(msgDisconnect);
        }
    }

    /// <summary>
    /// Host quit routine - performs quit after all clients have disconnected.
    /// </summary>
    /// <returns>IEnumerator - Coroutine IEnumarator.</returns>
    private IEnumerator HostQuitRoutine()
    {
        yield return host.StartCoroutine(host.HostClientsDisconnectingRoutine());
        // Destroy everything connected with networking and perform appropriate action
        host.Disconnect();
        messagesToSendToClients.Clear();
        //playersNetIDWithIPEndPoints - is being cleaned by SyncPlayers
        //players - is being cleaned by SyncPlayers
        foreach (NetObj netObj in netObjs.Values)
        {
            Destroy(netObj.gameObject);
        }
        netObjs.Clear();
        delayedMessages.Clear();
        deSpawnedNetIDs.Clear();
        Destroy(gameObject);
        ForceQuit(currentQuitMode);
    }

    /// <summary>
    /// Forces the quit - for impatient users - should be used only as a last resort - does not clean up connections!.
    /// </summary>
    /// <param name="quitMode">The quit mode.</param>
    public void ForceQuit(QuitMode quitMode)
    {
        switch (quitMode)
        {
            case QuitMode.QuitToMainMenu:
            case QuitMode.RequestedByHost:
                SceneManager.LoadScene(offlineSceneName);
                break;
            case QuitMode.ExitGame:
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
                break;
        }
    }

    #region message handling

    #region server

    //works on server
    /// <summary>
    /// Handles the client login. Server only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    /// <exception cref="System.InvalidOperationException">If syncPlayer netObj prefab found in spawnables.</exception>
    public void HandleLogin(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgLogin msgLogin = (MsgLogin)msg;

            if (msgLogin == null) return;
            if (isServer == false) return;

            foreach (NetObj netObj in netObjs.Values)
            {
                MsgSpawn msgSpawn = new MsgSpawn();
                msgSpawn.netID = netObj.netID;
                msgSpawn.spawnableID = netObj.spawnableID;
                msgSpawn.position = netObj.transform.position;
                msgSpawn.rotation = netObj.transform.rotation;
                msgSpawn.owningPlayerNetID = netObj.owningPlayerNetID;
                msgSpawn.syncVelocity = netObj.SyncRB;
                if (msgSpawn.syncVelocity)
                {
                    msgSpawn.velocity = netObj.RigidbodyComponent.velocity;
                }
                host.SendToClient(msgSpawn, ep);
                foreach (SyncVar syncVar in netObj.syncVars)
                {
                    MsgSyncVar msgSyncVar = syncVar.CreateMessage();
                    host.SendToClient(msgSyncVar, ep);
                }
            }

            // Spawn player for new client
            SyncPlayer clientPlayer = Instantiate(spawnablePlayerPrefab);
            UInt16 spawnableID = FindSpawnableID(clientPlayer.NetObj);
            if (spawnableID == UInt16.MaxValue)
            {
                throw new InvalidOperationException("No syncPlayer netObj prefab found in spawnables!");
            }
            UInt16 ourPlayerNetId = lastUniqNetId++;
            clientPlayer.NetObj.Init(ourPlayerNetId, spawnableID, true, ourPlayerNetId);
            netObjs.Add(ourPlayerNetId, clientPlayer.NetObj);
            playersNetIDWithIPEndPoints.Add(ourPlayerNetId, ep);

            MsgSpawnPlayer msgSpawnPlayerForOwner = new MsgSpawnPlayer();
            msgSpawnPlayerForOwner.netID = clientPlayer.NetObj.netID;
            msgSpawnPlayerForOwner.spawnableID = clientPlayer.NetObj.spawnableID;
            msgSpawnPlayerForOwner.setLocalPlayer = true;
            host.SendToClient(msgSpawnPlayerForOwner, ep);

            MsgSpawnPlayer msgSpawnPlayerForOthers = new MsgSpawnPlayer();
            msgSpawnPlayerForOthers.netID = clientPlayer.NetObj.netID;
            msgSpawnPlayerForOthers.spawnableID = clientPlayer.NetObj.spawnableID;
            msgSpawnPlayerForOthers.setLocalPlayer = false;
            host.SendToAllClientsExcept(msgSpawnPlayerForOthers, ep);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on server
    /// <summary>
    /// Handles the player being kicked. Server only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandlePlayerBeingKicked(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgKick msgKick = (MsgKick)msg;

            if (msgKick == null) return;
            if (isServer == false) return;

            foreach (KeyValuePair<UInt16, NetObj> netObjWithNetID in netObjs.ToList())
            {
                if (netObjWithNetID.Value.owningPlayerNetID == msgKick.kickedPlayerNetID)
                {
                    DeSpawn(netObjWithNetID.Key);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on server
    /// <summary>
    /// Handles the synchronize variable set. Server only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleSyncVarSet(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgSyncVar msgSyncVar = (MsgSyncVar)msg;

            if (msgSyncVar == null) return;
            if (isServer == false) return;

            if (!netObjs.ContainsKey(msgSyncVar.netObjNetID) && !deSpawnedNetIDs.Contains(msgSyncVar.netObjNetID))
            {
                delayedMessages.Add(new MsgSendInfo(msgSyncVar, ep));
                return;
            }
            if (msgSyncVar.syncVarIndex >= netObjs[msgSyncVar.netObjNetID].syncVars.Count)
            {
                Debug.LogError("Wrong sync var index for HandleSyncVarSet!");
                return;
            }

            netObjs[msgSyncVar.netObjNetID].syncVars[msgSyncVar.syncVarIndex].Adjust(msgSyncVar);
            netObjs[msgSyncVar.netObjNetID].syncVars[msgSyncVar.syncVarIndex].isDirty = true;
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on server
    /// <summary>
    /// Handles the disconnect. Server only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleDisconnect(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgDisconnect msgDisconnect = (MsgDisconnect)msg;

            if (msgDisconnect == null) return;
            if (isServer == false) return;

            host.Kick(ep, true, "Disconnection requested by client.");
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    #endregion

    #region client

    //works on client
    /// <summary>
    /// Handles the transform. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleTransform(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgTransform msgTransform = (MsgTransform)msg;

            if (msgTransform == null) return;
            if (isServer) return;
            if (!netObjs.ContainsKey(msgTransform.netID) && !deSpawnedNetIDs.Contains(msgTransform.netID))
            {
                delayedMessages.Add(new MsgSendInfo(msgTransform, ep));
                return;
            }
            
            netObjs[msgTransform.netID].Adjust(msgTransform);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the spawn. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleSpawn(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgSpawn msgSpawn = (MsgSpawn)msg;

            if (msgSpawn == null) return;
            if (isServer) return;
            if (netObjs.ContainsKey(msgSpawn.netID)) return;

            NetObj no = Instantiate(spawnables[msgSpawn.spawnableID], msgSpawn.position, msgSpawn.rotation);
            if (msgSpawn.syncVelocity && no.SyncRB)
            {
                no.RigidbodyComponent.velocity = msgSpawn.velocity;
            }
            no.Init(msgSpawn.netID, msgSpawn.spawnableID, false, msgSpawn.owningPlayerNetID);
            netObjs.Add(msgSpawn.netID, no);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the spawn player. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleSpawnPlayer(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgSpawnPlayer msgSpawnPlayer = (MsgSpawnPlayer)msg;

            if (msgSpawnPlayer == null) return;
            if (isServer) return;
            if (netObjs.ContainsKey(msgSpawnPlayer.netID)) return;

            SyncPlayer spawnedPlayer = Instantiate(spawnablePlayerPrefab);
            if (msgSpawnPlayer.setLocalPlayer)
            {
                spawnedPlayer.SetLocalPlayer();
            }
            spawnedPlayer.NetObj.Init(msgSpawnPlayer.netID, msgSpawnPlayer.spawnableID, false, msgSpawnPlayer.netID);
            netObjs.Add(msgSpawnPlayer.netID, spawnedPlayer.NetObj);
            if (msgSpawnPlayer.setLocalPlayer)
            {
                EventManager.TriggerEvent(EventType.Connected);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the player kick. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandlePlayerKick(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgKick msgKick = (MsgKick)msg;

            if (msgKick == null) return;
            if (isServer) return;

            // Destroy everything connected with networking and perform appropriate action
            client.Disconnect();
            messagesToSendToHost.Clear();
            //players - is being cleaned by SyncPlayers
            foreach (NetObj netObj in netObjs.Values)
            {
                Destroy(netObj.gameObject);
            }
            netObjs.Clear();
            delayedMessages.Clear();
            deSpawnedNetIDs.Clear();
            Destroy(gameObject);
            if (msgKick.harmless)
            {
                ForceQuit(currentQuitMode);
            }
            else
            {
                Overlook.Instance.ErrorMessageDisplay = new string(msgKick.reason);
                SceneManager.LoadScene(offlineSceneErrorName);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the synchronize variable. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleSyncVar(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgSyncVar msgSyncVar = (MsgSyncVar)msg;

            if (msgSyncVar == null) return;
            if (isServer) return;
            if (!netObjs.ContainsKey(msgSyncVar.netObjNetID) && !deSpawnedNetIDs.Contains(msgSyncVar.netObjNetID))
            {
                delayedMessages.Add(new MsgSendInfo(msgSyncVar, ep));
                return;
            }
            if (msgSyncVar.syncVarIndex >= netObjs[msgSyncVar.netObjNetID].syncVars.Count)
            {
                Debug.LogError("Wrong sync var index for HandleSyncVar!");
                return;
            }

            netObjs[msgSyncVar.netObjNetID].syncVars[msgSyncVar.syncVarIndex].Adjust(msgSyncVar);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the de spawn. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleDeSpawn(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgDeSpawn msgDeSpawn = (MsgDeSpawn)msg;

            if (msgDeSpawn == null) return;
            if (isServer) return;
            if (!netObjs.ContainsKey(msgDeSpawn.netID)) return;

            Destroy(netObjs[msgDeSpawn.netID].gameObject);
            netObjs.Remove(msgDeSpawn.netID);
            deSpawnedNetIDs.Add(msgDeSpawn.netID);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //works on client
    /// <summary>
    /// Handles the disconnect information from server. Client only.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleDisconnectInfoFromServer(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgDisconnect msgDisconnect = (MsgDisconnect)msg;

            if (msgDisconnect == null) return;
            if (isServer) return;

            isServerBeingClosed = true;

            Quit(QuitMode.RequestedByHost);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    #endregion

    //works on both server and client
    /// <summary>
    /// Handles the incomming RPC. Both server and client.
    /// </summary>
    /// <param name="msg">The MSG.</param>
    /// <param name="ep">The ep.</param>
    private void HandleIncommingRPC(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgRpcCallAction msgIncommingRpc = (MsgRpcCallAction)msg;

            if (msgIncommingRpc == null) return;

            if (!netObjs.ContainsKey(msgIncommingRpc.netObjNetID) && !deSpawnedNetIDs.Contains(msgIncommingRpc.netObjNetID))
            {
                delayedMessages.Add(new MsgSendInfo(msgIncommingRpc, ep));
                return;
            }
            if (msgIncommingRpc.rpcIndex >= netObjs[msgIncommingRpc.netObjNetID].syncRPCs.Count)
            {
                Debug.LogError("Wrong rpc index for HandleIncommingRPC!");
                return;
            }

            netObjs[msgIncommingRpc.netObjNetID].syncRPCs[msgIncommingRpc.rpcIndex].InvokeFromIncommingRPC(msgIncommingRpc);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    #endregion

    #region helper functions

    /// <summary>
    /// Finds the spawnable ID.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    /// <returns>UInt16 - netObj index in spawnables, if not found - UInt16.MaxValue.</returns>
    private UInt16 FindSpawnableID(NetObj netObj)
    {
        for (UInt16 i = 0; i < spawnables.Count; ++i)
        {
            if (netObj.name.Contains(spawnables[i].name))
            {
                return i;
            }
        }
        return UInt16.MaxValue;
    }

    #endregion
}
