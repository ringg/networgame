﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with NetSettings class defined.</summary>
// ***********************************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class NetSettings. It contains settings related to networking performance.
/// </summary>
public class NetSettings : MonoBehaviour
{
    /// <summary>
    /// The unconfirmed message resend time.
    /// </summary>
    [SerializeField]
    private float resendTime = 1.0F;
    /// <summary>
    /// Gets unconfirmed message resend time.
    /// </summary>
    /// <value>Unconfirmed message resend time.</value>
    public float ResendTime { get { return resendTime; } }

    /// <summary>
    /// The messages net send rate.
    /// </summary>
    [SerializeField]
    private float netSendRate = 15.0F;
    /// <summary>
    /// Gets the send interval. This is inverse of netSendRate.
    /// </summary>
    /// <value>The send interval.</value>
    public float SendInterval { get { return 1.0F / netSendRate; } }

    /// <summary>
    /// The seconds of unconfirmed message sent time to kick.
    /// </summary>
    [SerializeField]
    private float secondsOfUnconfirmedToKick = 6.0F;
    /// <summary>
    /// Gets the seconds of unconfirmed message sent time to kick.
    /// </summary>
    /// <value>The seconds of unconfirmed message sent time to kick.</value>
    public float SecondsOfUnconfirmedToKick { get { return secondsOfUnconfirmedToKick; } }

    /// <summary>
    /// The number of unconfirmed messages to kick.
    /// </summary>
    [SerializeField]
    private int numberOfUnconfirmedToKick = 1000;
    /// <summary>
    /// Gets the number of unconfirmed messages to kick.
    /// </summary>
    /// <value>The number of unconfirmed messages to kick.</value>
    public int NumberOfUnconfirmedToKick { get { return numberOfUnconfirmedToKick; } }

    /// <summary>
    /// Unity build-in Update function. Updates MsgSendInfo gameTime field with current game time.
    /// </summary>
    private void Update()
    {
        MsgSendInfo.gameTime = Time.time;
    }
}
