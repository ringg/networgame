﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-08-2017
//
// ***********************************************************************
// <summary>File with SocketWrapper class defined.</summary>
// ***********************************************************************
using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

/// <summary>
/// Class SocketWrapper - class containing socket.
/// </summary>
public class SocketWrapper
{
    /// <summary>
    /// The OnReceive handler - being calles each time new message is being received by socket.
    /// </summary>
    public Action<Message, IPEndPoint> OnReceive;

    /// <summary>
    /// The socket.
    /// </summary>
    private UdpClient socket;
    /// <summary>
    /// The is host flg.
    /// </summary>
    private bool isHost;
    /// <summary>
    /// The closed flag.
    /// </summary>
    private bool closed;

    /// <summary>
    /// Initializes a new instance of the <see cref="SocketWrapper"/> class.
    /// </summary>
    /// <param name="port">The port to open socket at.</param>
    /// <param name="isHost">Flag whether we are opening socket as host or client.</param>
    public SocketWrapper(int port, bool isHost)
    {
        socket = new UdpClient(port);
        socket.BeginReceive(new AsyncCallback(Receive), socket);
        this.isHost = isHost;
        closed = false;
    }

    /// <summary>
    /// Sends message as byte array through socket to IPEndPoint target.
    /// </summary>
    /// <param name="message">The message.</param>
    /// <param name="target">The target.</param>
    public void Send(byte[] message, IPEndPoint target)
    {
        socket.Send(message, message.Length, target);
    }

    /// <summary>
    /// Cleams up by closing socket.
    /// </summary>
    public void CleamUp()
    {
        if (closed)
        {
            return;
        }
        closed = true;
        socket.Close();
    }

    /// <summary>
    /// Function that handles data receiving on socket. Disconnects client on exception (signal that there is something wrong iwth connection).
    /// </summary>
    /// <param name="result">Socket as async result.</param>
    void Receive(IAsyncResult result)
    {
        UdpClient socketAsync = result.AsyncState as UdpClient;
        try
        {
            IPEndPoint source = new IPEndPoint(0, 0);
            byte[] message = socketAsync.EndReceive(result, ref source);
            Message msg = Message.MakeFromBuffer(message);
            if (closed == false)
            {
                socketAsync.BeginReceive(new AsyncCallback(Receive), socketAsync);
            }
            if (OnReceive != null)
            {
                OnReceive(msg, source);
            }
        }
        catch(Exception ex)
        {
            if (closed)
            {
                return;
            }
            if (isHost == false)
            {
                Debug.Log(ex);
                MsgKick message = new MsgKick();
                if (NetObjMgr.instance.IsServerBeingClosed)
                {
                    message.harmless = true;
                    message.kickedPlayerNetID = SyncPlayer.localPlayer.NetObj.netID;
                    message.reason = "Disconnection requested by host.".ToCharArray();
                }
                else
                {
                    message.harmless = false;
                    if (SyncPlayer.localPlayer == null)
                    {
                        message.reason = "Could not connect to host.".ToCharArray();
                    }
                    else
                    {
                        message.kickedPlayerNetID = SyncPlayer.localPlayer.NetObj.netID;
                        message.reason = "Connection to host lost.".ToCharArray();
                    }
                }
                message.Encode();
                IPEndPoint usAsSource = new IPEndPoint(IPAddress.None, 11000);
                if (OnReceive != null)
                {
                    OnReceive(message, usAsSource);
                }
            }
            else
            {
                socketAsync.BeginReceive(new AsyncCallback(Receive), socketAsync);
            }
        }
    }
}