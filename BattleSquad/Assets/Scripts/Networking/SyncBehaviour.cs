﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-10-2017
//
// ***********************************************************************
// <summary>File with SyncBehaviour class defined.</summary>
// ***********************************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class SyncBehaviour - component should ihnerit from this class in order to use SyncVars or SyncRPCs.
/// </summary>
public abstract class SyncBehaviour : MonoBehaviour
{
    /// <summary>
    /// Initializes SyncBehaviour - this is the place to create SyncVars and RPCs.
    /// </summary>
    /// <param name="netObj">NetObj component of this gameObject.</param>
    public abstract void Init(NetObj netObj);
}