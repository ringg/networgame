﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-11-2017
//
// ***********************************************************************
// <summary>File with SyncPlayer class defined.</summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class SyncPlayer - represents player.
/// </summary>
public class SyncPlayer : SyncBehaviour
{
    /// <summary>
    /// Local player.
    /// </summary>
    public static SyncPlayer localPlayer;

    /// <summary>
    /// The spawnable soldier prefab.
    /// </summary>
    [SerializeField]
    private NetObj spawnableSoldierPrefab = null;

    /// <summary>
    /// The is ready SyncVar.
    /// </summary>
    public SyncVar<bool> isReady;
    /// <summary>
    /// The username SyncVar.
    /// </summary>
    public SyncVarString username;
    /// <summary>
    /// The score SyncVar.
    /// </summary>
    public SyncVar<int> score;

    /// <summary>
    /// The spawn soldier SyncRPCRunOnServerAction.
    /// </summary>
    private SyncRPCRunOnServerAction spawnSoldier;

    /// <summary>
    /// The is local player flag.
    /// </summary>
    private bool isLocalPlayer = false;
    /// <summary>
    /// Gets a value indicating whether this syncPlayer is local player.
    /// </summary>
    /// <value><c>true</c> if this instance is local player; otherwise, <c>false</c>.</value>
    public bool IsLocalPlayer { get { return isLocalPlayer; } }

    /// <summary>
    /// The netObj of this syncPlayer.
    /// </summary>
    private NetObj netObj;
    /// <summary>
    /// Gets the net object of this player.
    /// </summary>
    /// <value>The netObj object.</value>
    public NetObj NetObj { get { return netObj; } }

    /// <summary>
    /// Lobby UI item.
    /// </summary>
    private LobbyItemUI myLobbyItem;
    /// <summary>
    /// Leaderboard UI item.
    /// </summary>
    private LeaderboardItemUI myLeaderboardItem;

    /// <summary>
    /// Unity build-in Awake function. Gets NetObj component, and raises GameStarted event.
    /// </summary>
    private void Awake()
    {
        netObj = GetComponent<NetObj>();
        EventManager.StartListening(EventType.GameStarted, OnGameStarted);
    }

    /// <summary>
    /// Unity build-in OnDestroy function. Clean ups after player upon its destroyment.
    /// </summary>
    private void OnDestroy()
    {
        if (NetObjMgr.instance != null)
        {
            if (NetObjMgr.instance.players.ContainsKey(netObj.netID))
            {
                NetObjMgr.instance.players.Remove(netObj.netID);
            }
            if (NetObjMgr.instance.playersNetIDWithIPEndPoints != null)
            {
                if (NetObjMgr.instance.playersNetIDWithIPEndPoints.ContainsKey(netObj.netID))
                {
                    NetObjMgr.instance.playersNetIDWithIPEndPoints.Remove(netObj.netID);
                }
            }
        }
        if (myLobbyItem != null)
        {
            Destroy(myLobbyItem.gameObject);
        }
        if (myLeaderboardItem != null)
        {
            if (LeaderboardUI.Instance != null)
            {
                LeaderboardUI.Instance.RemoveLeaderboardItemUIFromList(myLeaderboardItem);
            }
            Destroy(myLeaderboardItem.gameObject);
        }
        EventManager.StopListening(EventType.GameStarted, OnGameStarted);
    }

    /// <summary>
    /// Initializes syncPlayer. Triggers event PlayerCreated and LocalPlayerCreated, if player become slocal player.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    public override void Init(NetObj netObj)
    {
        isReady = new SyncVar<bool>(false, OnIsReadyChanged, netObj);
        username = new SyncVarString("Player" + netObj.netID, OnUsernameChanged, netObj);
        score = new SyncVar<int>(0, OnScoreChanged, netObj);
        spawnSoldier = new SyncRPCRunOnServerAction(SpawnSoldierOnServer, netObj);
        myLobbyItem = LobbyUI.Instance.CreateAndReturnLobbyItemUI();
        myLeaderboardItem = LeaderboardUI.Instance.CreateAndReturnLeaderboardItemUI();
        NetObjMgr.instance.players.Add(netObj.netID, this);
        EventManager.TriggerEvent(EventType.PlayerCreated, this);
        if (isLocalPlayer)
        {
            username.Value = Overlook.Instance.Username;
            EventManager.TriggerEvent(EventType.LocalPlayerCreated, this);

            // Comment this, if joining player should remain in spectator mode.
            if (Game.Instance != null && Game.Instance.isStarted != null && Game.Instance.isStarted.Value)
            {
                spawnSoldier.Invoke();
            }
        }
    }

    /// <summary>
    /// Sets this syncPlayer as local player.
    /// </summary>
    /// <exception cref="System.InvalidOperationException">If local Player was defined more than once.</exception>
    public void SetLocalPlayer()
    {
        isLocalPlayer = true;
        if (localPlayer != null)
        {
            throw new InvalidOperationException("Local Player defined more than once!");
        }
        localPlayer = this;
    }

    /// <summary>
    /// Handle called when isReady value changes.
    /// </summary>
    /// <param name="newValue">isReady new value.</param>
    private void OnIsReadyChanged(bool newValue)
    {
        if (myLobbyItem != null)
        {
            myLobbyItem.IsReadyToggle.isOn = newValue;
        }
    }

    /// <summary>
    /// Handle called when username value changes.
    /// </summary>
    /// <param name="newValue">username new value.</param>
    private void OnUsernameChanged(string newValue)
    {
        if (myLobbyItem != null)
        {
            myLobbyItem.PlayerNameText.text = newValue;
        }
        if (myLeaderboardItem != null)
        {
            myLeaderboardItem.PlayerNameText.text = newValue;
        }
    }

    /// <summary>
    /// Handle called when score value changes.
    /// </summary>
    /// <param name="newValue">score new value.</param>
    private void OnScoreChanged(int newValue)
    {
        if (myLeaderboardItem != null)
        {
            myLeaderboardItem.PlayerScoreText.text = newValue.ToString();
            LeaderboardUI.Instance.ReorderLeaderboardItems();
        }
    }

    /// <summary>
    /// Called when gameplay starts.
    /// </summary>
    private void OnGameStarted()
    {
        if (isLocalPlayer)
        {
            spawnSoldier.Invoke();
        }
    }

    /// <summary>
    /// Spawns the soldier. RPC called on server.
    /// </summary>
    private void SpawnSoldierOnServer()
    {
        Transform spawnPoint = SpawnPointsManager.Instance.GetRandomSpawnPoint();
        NetObjMgr.instance.Spawn(spawnableSoldierPrefab, spawnPoint.position, spawnPoint.rotation, netObj.netID);
    }
}