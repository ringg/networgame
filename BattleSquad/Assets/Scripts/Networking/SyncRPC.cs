﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-10-2017
//
// ***********************************************************************
// <summary>File with all available SyncRPC classes defined.</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

/// <summary>
/// Class SyncRPC - responsible for performing remote procedure calls.
/// </summary>
public abstract class SyncRPC
{
    /// <summary>
    /// Enum RPCDeterminedResult
    /// </summary>
    protected enum RPCDeterminedResult
    {
        /// <summary>
        /// The invoke
        /// </summary>
        Invoke,
        /// <summary>
        /// The create and send message to host
        /// </summary>
        CreateAndSendMessageToHost,
        /// <summary>
        /// The create and send message to all clients
        /// </summary>
        CreateAndSendMessageToAllClients,
        /// <summary>
        /// The create and send message to specific client
        /// </summary>
        CreateAndSendMessageToSpecificClient
    }

    /// <summary>
    /// The net object
    /// </summary>
    protected NetObj netObj;
    /// <summary>
    /// The synchronize RPC index
    /// </summary>
    protected int syncRpcIndex;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPC"/> class.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    public SyncRPC(NetObj netObj)
    {
        this.netObj = netObj;
        netObj.syncRPCs.Add(this);
        syncRpcIndex = netObj.syncRPCs.Count - 1;
    }

    /// <summary>
    /// Invokes from incomming RPC.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    public abstract void InvokeFromIncommingRPC(MsgRpcCallAction incommingMessage);

    /// <summary>
    /// Checks if delegate should be invoked.
    /// </summary>
    /// <returns>RPCDeterminedResult.</returns>
    protected abstract RPCDeterminedResult CheckIfDelegateShouldBeInvoked();

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected abstract void DoInvokeFromMessage(MsgRpcCallAction incommingMessage);
}

/// <summary>
/// Class SyncRPCRunOnServer.
/// </summary>
public abstract class SyncRPCRunOnServer : SyncRPC
{
    /// <summary>
    /// The allow to run without transition
    /// </summary>
    protected bool allowToRunWithoutTransition;


    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnServer"/> class.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowToRunWithoutTransition">if set to <c>true</c> allows to run rpc on server when called already on it.</param>
    public SyncRPCRunOnServer(NetObj netObj, bool allowToRunWithoutTransition = true) : base(netObj)
    {
        this.allowToRunWithoutTransition = allowToRunWithoutTransition;
    }

    /// <summary>
    /// Invokes from incomming RPC.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    /// <exception cref="System.InvalidOperationException">
    /// "RPC Run On Server" invoked somehow incorrectly!
    /// or
    /// Incomming "RPC Run On Server" invoked on client!
    /// </exception>
    public override void InvokeFromIncommingRPC(MsgRpcCallAction incommingMessage)
    {
        if (incommingMessage.invokedCorrectly == false)
        {
            //ExtendedDebug.LogError("\"RPC Run On Server\" invoked somehow incorrectly!");
            throw new InvalidOperationException("\"RPC Run On Server\" invoked somehow incorrectly!");
        }
        if (netObj.isServer)
        {
            //ExtendedDebug.Log("Incomming \"RPC Run On Server\" invoked on server.");
            DoInvokeFromMessage(incommingMessage);
        }
        else
        {
            //ExtendedDebug.LogError("Incomming \"RPC Run On Server\" invoked on client!");
            throw new InvalidOperationException("Incomming \"RPC Run On Server\" invoked on client!");
        }
    }

    /// <summary>
    /// Checks if delegate should be invoked.
    /// </summary>
    /// <returns>RPCDeterminedResult.</returns>
    /// <exception cref="System.InvalidOperationException">"RPC Run On Server" invoked already on server!</exception>
    protected override RPCDeterminedResult CheckIfDelegateShouldBeInvoked()
    {
        if (netObj.isServer)
        {
            if (allowToRunWithoutTransition)
            {
                //ExtendedDebug.Log("\"RPC Run On Server\" invoked already on server.");
                return RPCDeterminedResult.Invoke;
            }
            else
            {
                //ExtendedDebug.LogError("\"RPC Run On Server\" invoked already on server!");
                throw new InvalidOperationException("\"RPC Run On Server\" invoked already on server!");
            }
        }
        else
        {
            return RPCDeterminedResult.CreateAndSendMessageToHost;
        }
    }
}

/// <summary>
/// Class SyncRPCRunOnServerAction.
/// </summary>
public class SyncRPCRunOnServerAction : SyncRPCRunOnServer
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnServerAction"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowToRunWithoutTransition">if set to <c>true</c> allows to run rpc on server when called already on it.</param>
    public SyncRPCRunOnServerAction(Action actionDelegate, NetObj netObj, bool allowToRunWithoutTransition = true) : base(netObj, allowToRunWithoutTransition)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes this instance.
    /// </summary>
    public void Invoke()
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate();
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost();
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        actionDelegate();
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    private void CreateAndSendMessageToHost()
    {
        //ExtendedDebug.Log("\"RPC Run On Server Action\" invoked on client - passing to server.");
        MsgRpcCallAction msgRpcCall = new MsgRpcCallAction();
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }
}

/// <summary>
/// Class SyncRPCRunOnServerActionWithParameter.
/// </summary>
/// <typeparam name="T"></typeparam>
public class SyncRPCRunOnServerActionWithParameter<T> : SyncRPCRunOnServer
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action<T> actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnServerActionWithParameter{T}"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowToRunWithoutTransition">if set to <c>true</c> allows to run rpc on server when called already on it.</param>
    public SyncRPCRunOnServerActionWithParameter(Action<T> actionDelegate, NetObj netObj, bool allowToRunWithoutTransition = true) : base(netObj, allowToRunWithoutTransition)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Invoke(T value)
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate(value);
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost(value);
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        MsgRpcCallActionWithParameter<T> msgRpcCall = (MsgRpcCallActionWithParameter<T>)incommingMessage;
        actionDelegate(msgRpcCall.value);
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    /// <param name="value">The value.</param>
    private void CreateAndSendMessageToHost(T value)
    {
        //ExtendedDebug.Log("\"RPC Run On Server Action With Parameter\" invoked on client - passing to server.");
        MsgRpcCallActionWithParameter<T> msgRpcCall = new MsgRpcCallActionWithParameter<T>();
        msgRpcCall.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        msgRpcCall.value = value;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }
}

/// <summary>
/// Class SyncRPCMulticast.
/// </summary>
public abstract class SyncRPCMulticast : SyncRPC
{
    // If true - allow multicast called on client to be first transitioned to the server rather than being called just like non-replicated method
    /// <summary>
    /// The allow more abstract behaviour
    /// </summary>
    protected bool allowMoreAbstractBehaviour;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCMulticast"/> class.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCMulticast(NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj)
    {
        this.allowMoreAbstractBehaviour = allowMoreAbstractBehaviour;
    }

    /// <summary>
    /// Invokes from incomming RPC.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    /// <exception cref="System.InvalidOperationException">Incomming "RPC Multicast" invoked on client with intention to scarcely perform multicast!</exception>
    public override void InvokeFromIncommingRPC(MsgRpcCallAction incommingMessage)
    {
        if (incommingMessage.invokedCorrectly)
        {
            // We are receiving RPC as client - invoke despite server or client.
            //ExtendedDebug.Log("Incomming \"RPC Multicast\" invoked on client.");
            DoInvokeFromMessage(incommingMessage);
        }
        else
        {
            if (netObj.isServer)
            {
                //ExtendedDebug.Log("Incomming \"RPC Multicast\" invoked on server with intention to scarcely perform Multicast.");
                ReInvokeOnServer(incommingMessage);
            }
            else
            {
                //ExtendedDebug.LogError("Incomming \"RPC Multicast\" invoked on client with intention to scarcely perform multicast!");
                throw new InvalidOperationException("Incomming \"RPC Multicast\" invoked on client with intention to scarcely perform multicast!");
            }
        }
    }

    /// <summary>
    /// Checks if delegate should be invoked.
    /// </summary>
    /// <returns>RPCDeterminedResult.</returns>
    protected override RPCDeterminedResult CheckIfDelegateShouldBeInvoked()
    {
        if (netObj.isServer)
        {
            return RPCDeterminedResult.CreateAndSendMessageToAllClients;
        }
        else
        {
            if (allowMoreAbstractBehaviour)
            {
                return RPCDeterminedResult.CreateAndSendMessageToHost;
            }
            else
            {
                //ExtendedDebug.LogWarning("\"RPC Multicast\" invoked on client without abstract behaviour - calling as non-replicated method!");
                return RPCDeterminedResult.Invoke;
            }
        }
    }

    /// <summary>
    /// Res the invoke on server.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected abstract void ReInvokeOnServer(MsgRpcCallAction incommingMessage);
}

/// <summary>
/// Class SyncRPCMulticastAction.
/// </summary>
public class SyncRPCMulticastAction : SyncRPCMulticast
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCMulticastAction"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCMulticastAction(Action actionDelegate, NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj, allowMoreAbstractBehaviour)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes this instance.
    /// </summary>
    public void Invoke()
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate();
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost();
                break;
            case RPCDeterminedResult.CreateAndSendMessageToAllClients:
                CreateAndSendMessageToAllClients();
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        actionDelegate();
    }

    /// <summary>
    /// Res the invoke on server.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void ReInvokeOnServer(MsgRpcCallAction incommingMessage)
    {
        Invoke();
    }

    /// <summary>
    /// Creates the and send message to all clients.
    /// </summary>
    private void CreateAndSendMessageToAllClients()
    {
        //ExtendedDebug.Log("\"RPC Multicast Action\" invoked on server - passing to each client.");
        MsgRpcCallAction msgRpcCall = new MsgRpcCallAction();
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        NetObjMgr.instance.messagesToSendToClients.Add(new KeyValuePair<Message, IPEndPoint>(msgRpcCall, null));
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    private void CreateAndSendMessageToHost()
    {
        //ExtendedDebug.Log("\"RPC Multicast Action\" invoked on client with abstraction behaviour enabled.");
        MsgRpcCallAction msgRpcCall = new MsgRpcCallAction();
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = false;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }
}

/// <summary>
/// Class SyncRPCMulticastActionWithParameter.
/// </summary>
/// <typeparam name="T"></typeparam>
public class SyncRPCMulticastActionWithParameter<T> : SyncRPCMulticast
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action<T> actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCMulticastActionWithParameter{T}"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCMulticastActionWithParameter(Action<T> actionDelegate, NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj, allowMoreAbstractBehaviour)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Invoke(T value)
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate(value);
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost(value);
                break;
            case RPCDeterminedResult.CreateAndSendMessageToAllClients:
                CreateAndSendMessageToAllClients(value);
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        MsgRpcCallActionWithParameter<T> msgRpcCall = (MsgRpcCallActionWithParameter<T>)incommingMessage;
        actionDelegate(msgRpcCall.value);
    }

    /// <summary>
    /// Res the invoke on server.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void ReInvokeOnServer(MsgRpcCallAction incommingMessage)
    {
        MsgRpcCallActionWithParameter<T> msgRpcCall = (MsgRpcCallActionWithParameter<T>)incommingMessage;
        Invoke(msgRpcCall.value);
    }

    /// <summary>
    /// Creates the and send message to all clients.
    /// </summary>
    /// <param name="value">The value.</param>
    private void CreateAndSendMessageToAllClients(T value)
    {
        //ExtendedDebug.Log("\"RPC Multicast Action With Parameter\" invoked on server - passing to each client.");
        MsgRpcCallActionWithParameter<T> msgRpcCall = new MsgRpcCallActionWithParameter<T>();
        msgRpcCall.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgRpcCall.value = value;
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        NetObjMgr.instance.messagesToSendToClients.Add(new KeyValuePair<Message, IPEndPoint>(msgRpcCall, null));
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    /// <param name="value">The value.</param>
    private void CreateAndSendMessageToHost(T value)
    {
        //ExtendedDebug.Log("\"RPC Multicast Action With Parameter\" invoked on client with abstraction behaviour enabled.");
        MsgRpcCallActionWithParameter<T> msgRpcCall = new MsgRpcCallActionWithParameter<T>();
        msgRpcCall.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgRpcCall.value = value;
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = false;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }
}

/// <summary>
/// Class SyncRPCRunOnOwningClient.
/// </summary>
public abstract class SyncRPCRunOnOwningClient : SyncRPC
{
    // If true - allow RunOnOwningClient called on client to be first transitioned to the server rather than being called just like non-replicated method
    /// <summary>
    /// The allow more abstract behaviour
    /// </summary>
    protected bool allowMoreAbstractBehaviour;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnOwningClient"/> class.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCRunOnOwningClient(NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj)
    {
        this.allowMoreAbstractBehaviour = allowMoreAbstractBehaviour;
    }

    /// <summary>
    /// Invokes from incomming RPC.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    /// <exception cref="System.InvalidOperationException">
    /// Incomming "RPC Run On Owning Client" invoked somehow on server with assumption that call should be invoked on owning client but not server!
    /// or
    /// Incomming "RPC Run On Owning Client" invoked on client with assumption that it owns this object, but it doesn't!
    /// or
    /// Incomming "RPC Run On Owning Client" invoked somehow on client with assumption that call should be invoked on server!
    /// </exception>
    public override void InvokeFromIncommingRPC(MsgRpcCallAction incommingMessage)
    {
        if (incommingMessage.invokedCorrectly)
        {
            if (netObj.isServer)
            {
                //ExtendedDebug.LogError("Incomming \"RPC Run On Owning Client\" invoked somehow on server with assumption that call should be invoked on owning client but not server!");
                throw new InvalidOperationException("Incomming \"RPC Run On Owning Client\" invoked somehow on server with assumption that call should be invoked on owning client but not server!");
            }
            else
            {
                // We are receiving RPC as owner of this object
                if (netObj.isOwnedByLocalPlayer)
                {
                    //ExtendedDebug.Log("Incomming \"RPC Run On Owning Client\" invoked on client, which owns this object.");
                    DoInvokeFromMessage(incommingMessage);
                }
                else
                {
                    //ExtendedDebug.LogError("Incomming \"RPC Run On Owning Client\" invoked on client with assumption that it owns this object, but it doesn't!");
                    throw new InvalidOperationException("Incomming \"RPC Run On Owning Client\" invoked on client with assumption that it owns this object, but it doesn't!");
                }
            }
        }
        else
        {
            if (netObj.isServer)
            {
                // We are receiving RPC as server (either as owner of this object, or to pass it to appropriate client)
                if (netObj.isOwnedByLocalPlayer)
                {
                    //ExtendedDebug.Log("Incomming \"RPC Run On Owning Client\" invoked on server, which owns this object.");
                    DoInvokeFromMessage(incommingMessage);
                }
                else
                {
                    //ExtendedDebug.Log("Incomming \"RPC Run On Owning Client\" invoked on non-owning server, with intention to pass it to owning client - doing so.");
                    PassIncommingRPCToOwningClient(incommingMessage);
                }
            }
            else
            {
                //ExtendedDebug.LogError("Incomming \"RPC Run On Owning Client\" invoked somehow on client with assumption that call should be invoked on server!");
                throw new InvalidOperationException("Incomming \"RPC Run On Owning Client\" invoked somehow on client with assumption that call should be invoked on server!");
            }
        }
    }

    /// <summary>
    /// Checks if delegate should be invoked.
    /// </summary>
    /// <returns>RPCDeterminedResult.</returns>
    protected override RPCDeterminedResult CheckIfDelegateShouldBeInvoked()
    {
        if (netObj.isServer)
        {
            if (netObj.isOwnedByLocalPlayer)
            {
                //ExtendedDebug.Log("\"RPC Run On Owning Client\" invoked on server, which owns this object.");
                return RPCDeterminedResult.Invoke;
            }
            else
            {
                return RPCDeterminedResult.CreateAndSendMessageToSpecificClient;
            }
        }
        else
        {
            if (netObj.isOwnedByLocalPlayer)
            {
                //ExtendedDebug.Log("\"RPC Run On Owning Client\" invoked on client, which owns this object.");
                return RPCDeterminedResult.Invoke;
            }
            else
            {
                if (allowMoreAbstractBehaviour)
                {
                    return RPCDeterminedResult.CreateAndSendMessageToHost;
                }
                else
                {
                    //ExtendedDebug.LogWarning("\"RPC Run On Owning Client\" invoked on non-owning client without abstract behaviour - calling as non-replicated method!");
                    return RPCDeterminedResult.Invoke;
                }
            }
        }
    }

    /// <summary>
    /// Passes the incomming RPC to owning client.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected abstract void PassIncommingRPCToOwningClient(MsgRpcCallAction incommingMessage);
}

/// <summary>
/// Class SyncRPCRunOnOwningClientAction.
/// </summary>
public class SyncRPCRunOnOwningClientAction : SyncRPCRunOnOwningClient
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnOwningClientAction"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCRunOnOwningClientAction(Action actionDelegate, NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj, allowMoreAbstractBehaviour)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes this instance.
    /// </summary>
    public void Invoke()
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate();
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost();
                break;
            case RPCDeterminedResult.CreateAndSendMessageToSpecificClient:
                CreateAndSendMessageToSpecificClient();
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        actionDelegate();
    }

    /// <summary>
    /// Creates the and send message to specific client.
    /// </summary>
    private void CreateAndSendMessageToSpecificClient()
    {
        //ExtendedDebug.Log("\"RPC Run On Owning Client Action\" invoked on non-owning server, passing to appropriate client.");
        MsgRpcCallAction msgRpcCall = new MsgRpcCallAction();
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        NetObjMgr.instance.messagesToSendToClients.Add(new KeyValuePair<Message, IPEndPoint>(msgRpcCall, NetObjMgr.instance.playersNetIDWithIPEndPoints[netObj.owningPlayerNetID]));
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    private void CreateAndSendMessageToHost()
    {
        //ExtendedDebug.Log("\"RPC Run On Owning Client Action\" invoked on non-owning client with abstraction behaviour enabled.");
        MsgRpcCallAction msgRpcCall = new MsgRpcCallAction();
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = false;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }

    /// <summary>
    /// Passes the incomming RPC to owning client.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void PassIncommingRPCToOwningClient(MsgRpcCallAction incommingMessage)
    {
        CreateAndSendMessageToSpecificClient();
    }
}

/// <summary>
/// Class SyncRPCRunOnOwningClientActionWithParameter.
/// </summary>
/// <typeparam name="T"></typeparam>
public class SyncRPCRunOnOwningClientActionWithParameter<T> : SyncRPCRunOnOwningClient
{
    /// <summary>
    /// The action delegate
    /// </summary>
    private Action<T> actionDelegate;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncRPCRunOnOwningClientActionWithParameter{T}"/> class.
    /// </summary>
    /// <param name="actionDelegate">The action delegate.</param>
    /// <param name="netObj">The net object.</param>
    /// <param name="allowMoreAbstractBehaviour">if set to <c>true</c> allows to firstly pass rpc to server to call it instead of calling it as non-replicated method.</param>
    public SyncRPCRunOnOwningClientActionWithParameter(Action<T> actionDelegate, NetObj netObj, bool allowMoreAbstractBehaviour = true) : base(netObj, allowMoreAbstractBehaviour)
    {
        this.actionDelegate = actionDelegate;
    }

    /// <summary>
    /// Invokes the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Invoke(T value)
    {
        switch (CheckIfDelegateShouldBeInvoked())
        {
            case RPCDeterminedResult.Invoke:
                actionDelegate(value);
                break;
            case RPCDeterminedResult.CreateAndSendMessageToHost:
                CreateAndSendMessageToHost(value);
                break;
            case RPCDeterminedResult.CreateAndSendMessageToSpecificClient:
                CreateAndSendMessageToSpecificClient(value);
                break;
        }
    }

    /// <summary>
    /// Does the invoke from message.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void DoInvokeFromMessage(MsgRpcCallAction incommingMessage)
    {
        MsgRpcCallActionWithParameter<T> msgRpcCall = (MsgRpcCallActionWithParameter<T>)incommingMessage;
        actionDelegate(msgRpcCall.value);
    }

    /// <summary>
    /// Creates the and send message to specific client.
    /// </summary>
    /// <param name="value">The value.</param>
    private void CreateAndSendMessageToSpecificClient(T value)
    {
        //ExtendedDebug.Log("\"RPC Run On Owning Client Action With Parameter\" invoked on non-owning server, passing to appropriate client.");
        MsgRpcCallActionWithParameter<T> msgRpcCall = new MsgRpcCallActionWithParameter<T>();
        msgRpcCall.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgRpcCall.value = value;
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = true;
        NetObjMgr.instance.messagesToSendToClients.Add(new KeyValuePair<Message, IPEndPoint>(msgRpcCall, NetObjMgr.instance.playersNetIDWithIPEndPoints[netObj.owningPlayerNetID]));
    }

    /// <summary>
    /// Creates the and send message to host.
    /// </summary>
    /// <param name="value">The value.</param>
    private void CreateAndSendMessageToHost(T value)
    {
        //ExtendedDebug.Log("\"RPC Run On Owning Client Action With Parameter\" invoked on non-owning client with abstraction behaviour enabled.");
        MsgRpcCallActionWithParameter<T> msgRpcCall = new MsgRpcCallActionWithParameter<T>();
        msgRpcCall.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgRpcCall.value = value;
        msgRpcCall.netObjNetID = netObj.netID;
        msgRpcCall.rpcIndex = syncRpcIndex;
        msgRpcCall.invokedCorrectly = false;
        NetObjMgr.instance.messagesToSendToHost.Add(msgRpcCall);
    }

    /// <summary>
    /// Passes the incomming RPC to owning client.
    /// </summary>
    /// <param name="incommingMessage">The incomming message.</param>
    protected override void PassIncommingRPCToOwningClient(MsgRpcCallAction incommingMessage)
    {
        MsgRpcCallActionWithParameter<T> msgRpcCall = (MsgRpcCallActionWithParameter<T>)incommingMessage;
        CreateAndSendMessageToSpecificClient(msgRpcCall.value);
    }
}
