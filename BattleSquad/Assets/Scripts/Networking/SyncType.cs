﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-18-2017
//
// ***********************************************************************
// <summary>File with static SyncType class defined.</summary>
// ***********************************************************************
using System;
using UnityEngine;

/// <summary>
/// Class SyncType - reponsible for mapping type to int value.
/// </summary>
public static class SyncType
{
    /// <summary>
    /// Returns int value from provided type.
    /// </summary>
    /// <param name="type">Type of object</param>
    /// <returns>Int representing provided type.</returns>
    /// <exception cref="System.ArgumentException">If unsupported type provided.</exception>
    public static int SyncTypeTypeToInt(Type type)
    {
        if (type == typeof(int)) return 1;
        else if (type == typeof(float)) return 2;
        else if (type == typeof(char)) return 3;
        else if (type == typeof(bool)) return 4;
        else if (type == typeof(Vector3)) return 5;
        else if (type == typeof(Quaternion)) return 6;
        else if (type == typeof(string)) return 7;
        else if (type == typeof(UInt16)) return 8;
        else throw new ArgumentException("Unsupported SyncType!");
    }

    /// <summary>
    /// Returns Type from provided int value.
    /// </summary>
    /// <param name="intValue">Int value of type.</param>
    /// <returns>Type representing provided int.</returns>
    /// <exception cref="System.ArgumentException">If unknown int value of type provided.</exception>
    public static Type SyncTypeIntToType(int intValue)
    {
        switch (intValue)
        {
            case 1: return typeof(int);
            case 2: return typeof(float);
            case 3: return typeof(char);
            case 4: return typeof(bool);
            case 5: return typeof(Vector3);
            case 6: return typeof(Quaternion);
            case 7: return typeof(string);
            case 8: return typeof(UInt16);
            default: throw new ArgumentException("Unknown int value of SyncType!");
        }
    }
}