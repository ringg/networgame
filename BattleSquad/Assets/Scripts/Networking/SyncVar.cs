﻿// ***********************************************************************
// Assembly         : Assembly-CSharp.dll
// Authors          : Patryk, Kuba
// Created          : 12-10-2017
//
// ***********************************************************************
// <summary>File with all available SyncVar classes defined.</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Base SyncVar class. It reprezents variable which value is synchronized over network.
/// </summary>
public abstract class SyncVar
{
    /// <summary>
    /// The is dirty flag.
    /// </summary>
    public bool isDirty;
    /// <summary>
    /// The netObj.
    /// </summary>
    protected NetObj netObj;
    /// <summary>
    /// The syncVar index.
    /// </summary>
    protected int syncVarIndex;

    /// <summary>
    /// The last timestamp
    /// </summary>
    protected Int64 lastTimestamp;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncVar"/> class.
    /// </summary>
    /// <param name="netObj">The net object.</param>
    public SyncVar(NetObj netObj)
    {
        this.netObj = netObj;
        netObj.syncVars.Add(this);
        syncVarIndex = netObj.syncVars.Count - 1;
    }

    /// <summary>
    /// Creates the message specific for this syncVar.
    /// </summary>
    /// <returns>SyncVar specific message.</returns>
    public abstract MsgSyncVar CreateMessage();

    /// <summary>
    /// Adjusts syncVar.
    /// </summary>
    /// <param name="msg">Received SyncVar message.</param>
    public abstract void Adjust(MsgSyncVar msg);
}

/// <summary>
/// Generic SyncVar class.
/// </summary>
/// <typeparam name="T">SyncVar type.</typeparam>
public class SyncVar<T> : SyncVar
{
    /// <summary>
    /// SyncVar value
    /// </summary>
    protected T value;
    /// <summary>
    /// The hook
    /// </summary>
    protected Action<T> hook;

    /// <summary>
    /// Initializes a new instance of the <see cref="SyncVar{T}"/> class.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="hook">Delegate that will be invoked on value change.</param>
    /// <param name="netObj">The net object.</param>
    public SyncVar(T value, Action<T> hook, NetObj netObj) : base(netObj)
    {
        this.value = value;
        this.hook = hook;
    }

    /// <summary>
    /// Gets or sets the value. If client - the value is not changed, but request to change value is send to host. If value is already equal, no change is performed.
    /// </summary>
    /// <value>New value to set.</value>
    public T Value
    {
        get { return value; }
        set
        {
            if (this.value.Equals(value))
            {
                return;
            }
            if (netObj.isServer)
            {
                if (hook != null)
                {
                    hook(value);
                }
                this.value = value;
                isDirty = true;
            }
            else
            {
                //Let's make one abstraction step ahead
                T oldValue = this.value;
                this.value = value;
                MsgSyncVar msgSyncVarSet = CreateMessage();
                this.value = oldValue;
                NetObjMgr.instance.messagesToSendToHost.Add(msgSyncVarSet);
            }
        }
    }

    /// <summary>
    /// Creates the message specific to this SyncVar.
    /// </summary>
    /// <returns>Prepared message.</returns>
    public override MsgSyncVar CreateMessage()
    {
        MsgSyncVar<T> msgSyncVar = new MsgSyncVar<T>();
        msgSyncVar.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(T));
        msgSyncVar.netObjNetID = netObj.netID;
        msgSyncVar.syncVarIndex = syncVarIndex;
        msgSyncVar.value = value;
        return msgSyncVar;
    }

    /// <summary>
    /// Adjusts syncVar value.
    /// </summary>
    /// <param name="msg">Received SyncVar value.</param>
    public override void Adjust(MsgSyncVar msg)
    {
        MsgSyncVar<T> msgSyncVar = (MsgSyncVar<T>)msg;

        if (msgSyncVar.timestamp < lastTimestamp) return;
        lastTimestamp = msgSyncVar.timestamp;

        if (hook != null)
        {
            hook(msgSyncVar.value);
        }
        value = msgSyncVar.value;
    }
}

/// <summary>
/// Class SyncVarString - string synchronized over network.
/// </summary>
public class SyncVarString : SyncVar<string>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SyncVarString"/> class.
    /// </summary>
    /// <param name="value"><see cref="SyncVar{T}"/>.</param>
    /// <param name="hook"><see cref="SyncVar{T}"/>.</param>
    /// <param name="netObj"><see cref="SyncVar{T}"/>.</param>
    public SyncVarString(string value, Action<string> hook, NetObj netObj) : base(value, hook, netObj)
    {

    }

    /// <summary>
    /// Creates the message specific to this SyncVar.
    /// </summary>
    /// <returns>Prepared message.</returns>
    public override MsgSyncVar CreateMessage()
    {
        MsgSyncVarString msgSyncVarString = new MsgSyncVarString();
        msgSyncVarString.syncVarTypeAsInt = SyncType.SyncTypeTypeToInt(typeof(string));
        msgSyncVarString.netObjNetID = netObj.netID;
        msgSyncVarString.syncVarIndex = syncVarIndex;
        msgSyncVarString.value = value.ToCharArray();
        return msgSyncVarString;
    }

    /// <summary>
    /// Adjusts syncVar value.
    /// </summary>
    /// <param name="msg">Received SyncVar value.</param>
    public override void Adjust(MsgSyncVar msg)
    {
        MsgSyncVarString msgSyncVarString = (MsgSyncVarString)msg;

        if (msgSyncVarString.timestamp < lastTimestamp) return;
        lastTimestamp = msgSyncVarString.timestamp;

        string valueAsString = new string(msgSyncVarString.value);

        if (hook != null)
        {
            hook(valueAsString);
        }
        value = valueAsString;
    }
}