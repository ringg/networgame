﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform ammoUIRectTransform = null;
    [SerializeField]
    private Text ammoText = null;

    private void Awake()
    {
        EventManager.StartListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StartListening(EventType.LocalSoldierDied, OnLocalSoldierDied);
        EventManager.StartListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StopListening(EventType.LocalSoldierDied, OnLocalSoldierDied);
        EventManager.StopListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
    }

    public void ShowAmmo(int currentAmmo, int ammoPerMagazine)
    {
        ammoText.text = currentAmmo + "/" + ammoPerMagazine;
    }

    private void OnLocalSoldierDied()
    {
        ammoUIRectTransform.gameObject.SetActive(false);
    }

    private void OnLocalSoldierSpawnedOrResurrected()
    {
        ammoUIRectTransform.gameObject.SetActive(true);
    }
}