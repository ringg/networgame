﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMenuUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform battleUIRectTransform = null;

    public static BattleMenuUI Instance;

    private bool isBattleMenuActive;
    public bool IsBattleMenuActive { get { return isBattleMenuActive; } }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("BattleMenuUI instance created more than once!");
            Destroy(gameObject);
            return;
        }
        Instance = this;
        isBattleMenuActive = false;
    }

    private void Update()
    {
        if ((Game.Instance == null) || (Game.Instance.isStarted == null) || (Game.Instance.isStarted.Value == false))
        {
            return;
        }
        if (Input.GetButtonDown("Cancel"))
        {
            if (isBattleMenuActive)
            {
                HideBattleMenuUI();
                HideAndLockCursor();
            }
            else
            {
                ShowBattleMenuUI();
            }
        }
    }

    private void ShowBattleMenuUI()
    {
        battleUIRectTransform.gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        isBattleMenuActive = true;
    }

    private void HideBattleMenuUI()
    {
        battleUIRectTransform.gameObject.SetActive(false);
        isBattleMenuActive = false;
    }

    private void HideAndLockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void OnReturnToGameButtonClick()
    {
        HideBattleMenuUI();
        HideAndLockCursor();
    }

    public void OnQuitToMainMenuButtonClick()
    {
        HideBattleMenuUI();
        NetObjMgr.instance.Quit(QuitMode.QuitToMainMenu);
    }

    public void OnExitGameButtonClick()
    {
        HideBattleMenuUI();
        NetObjMgr.instance.Quit(QuitMode.ExitGame);
    }
}