﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConnectingUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform connectingUIRectTransform = null;

    private void Awake()
    {
        EventManager.StartListening(EventType.Connected, OnConnected);
        connectingUIRectTransform.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.Connected, OnConnected);
    }

    private void OnConnected()
    {
        if (Game.Instance == null || Game.Instance.isStarted == null || Game.Instance.isStarted.Value == false)
        {
            LobbyUI.Instance.ToggleShowLobby(true);
        }
        connectingUIRectTransform.gameObject.SetActive(false);
    }

    public void OnCancelButtonClick()
    {
        NetObjMgr.instance.ForceQuit(QuitMode.QuitToMainMenu);
    }
}