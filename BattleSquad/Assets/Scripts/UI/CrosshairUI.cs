﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrosshairUI : MonoBehaviour
{
    [SerializeField]
    private float minDistance = default(float);
    [SerializeField]
    private float maxDistance = default(float);
    [SerializeField]
    private float hitmarkerDuration = default(float);

    [SerializeField]
    private RectTransform crosshairUIRectTransform = null;
    [SerializeField]
    private Image crosshairUpperPart = null;
    [SerializeField]
    private Image crosshairLowerPart = null;
    [SerializeField]
    private Image crosshairRightPart = null;
    [SerializeField]
    private Image crosshairLeftPart = null;
    [SerializeField]
    private Image hitmarker = null;

    private Coroutine hitmarkerCoroutine;

    private void Awake()
    {
        hitmarkerCoroutine = StartCoroutine(Helper.EmptyRoutine());
        EventManager.StartListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StartListening(EventType.LocalSoldierDied, OnLocalSoldierDied);
        EventManager.StartListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.LocalSoldierSpawned, OnLocalSoldierSpawnedOrResurrected);
        EventManager.StopListening(EventType.LocalSoldierDied, OnLocalSoldierDied);
        EventManager.StopListening(EventType.LocalSoldierResurrected, OnLocalSoldierSpawnedOrResurrected);
    }

    public void SetRecoil(float recoilNormalized)
    {
        float distance = Mathf.Lerp(minDistance, maxDistance, recoilNormalized);
        crosshairUpperPart.rectTransform.anchoredPosition = new Vector2(crosshairUpperPart.rectTransform.anchoredPosition.x, distance);
        crosshairLowerPart.rectTransform.anchoredPosition = new Vector2(crosshairLowerPart.rectTransform.anchoredPosition.x, -distance);
        crosshairRightPart.rectTransform.anchoredPosition = new Vector2(distance, crosshairRightPart.rectTransform.anchoredPosition.y);
        crosshairLeftPart.rectTransform.anchoredPosition = new Vector2(-distance, crosshairLeftPart.rectTransform.anchoredPosition.y);
    }

    public void ShowHitmarker()
    {
        hitmarker.gameObject.SetActive(true);
        StopCoroutine(hitmarkerCoroutine);
        hitmarkerCoroutine = StartCoroutine(HideHitmarker());
    }

    private IEnumerator HideHitmarker()
    {
        yield return new WaitForSecondsRealtime(hitmarkerDuration);
        hitmarker.gameObject.SetActive(false);
    }

    private void OnLocalSoldierDied()
    {
        crosshairUIRectTransform.gameObject.SetActive(false);
    }

    private void OnLocalSoldierSpawnedOrResurrected()
    {
        crosshairUIRectTransform.gameObject.SetActive(true);
    }
}