﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageUI : MonoBehaviour
{
    [SerializeField]
    private Image postprocessImage = null;

    public void UpdatePostprocessStrenght(float helathNormalized)
    {
        Color newPostprocessColor = postprocessImage.color;
        newPostprocessColor.a = 1.0F - helathNormalized;
        postprocessImage.color = newPostprocessColor;
    }
}