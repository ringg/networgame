﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisconnectingUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform disconnectingUIRectTransform = null;

    public static DisconnectingUI Instance;

    private QuitMode quitMode;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("isconnectingUI instance created more than once!");
            Destroy(gameObject);
            return;
        }
        Instance = this;
        disconnectingUIRectTransform.gameObject.SetActive(false);
    }

    public void Show(QuitMode performedQuitMode)
    {
        quitMode = performedQuitMode;
        disconnectingUIRectTransform.gameObject.SetActive(true);
    }

    public void OnForceQuitButtonClick()
    {
        NetObjMgr.instance.ForceQuit(quitMode);
    }
}