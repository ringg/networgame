﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillInfoUI : MonoBehaviour
{
    [SerializeField]
    private Text killInfoText = null;
    [SerializeField]
    private float killInfoTextDuration = default(float);

    private Coroutine hideKillInfoTextCoroutine;

    private void Awake()
    {
        hideKillInfoTextCoroutine = StartCoroutine(Helper.EmptyRoutine());
    }

    public void ShowKillInfoForVictim(string killerName)
    {
        killInfoText.text = "You were killed by: " + killerName;
        ShowKillInfo();
    }

    public void ShowKillInfoForKiller(string victimName)
    {
        killInfoText.text = "You killed: " + victimName;
        ShowKillInfo();
    }

    public void ShowKillInfoForEveryoneElse(string killerName, string victimName)
    {
        killInfoText.text = killerName + " killed: " + victimName;
        ShowKillInfo();
    }

    public void ShowCollectInfoForKiller(string itemName)
    {
        killInfoText.text = "You have found: " + itemName;
        ShowKillInfo();
    }

    public void ShowCollectInfoForEveryoneElse(string killerName, string itemName)
    {
        killInfoText.text = killerName + " has found: " + itemName;
        ShowKillInfo();
    }

    private void ShowKillInfo()
    {
        StopCoroutine(hideKillInfoTextCoroutine);
        killInfoText.gameObject.SetActive(true);
        hideKillInfoTextCoroutine = StartCoroutine(HideKillInfoTextRoutine());
    }

    private IEnumerator HideKillInfoTextRoutine()
    {
        yield return new WaitForSecondsRealtime(killInfoTextDuration);
        killInfoText.gameObject.SetActive(false);
    }
}