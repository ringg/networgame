﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardItemUI : MonoBehaviour
{
    [SerializeField]
    private Text playerNameText = null;
    public Text PlayerNameText { get { return playerNameText; } }

    [SerializeField]
    private Text playerScoreText = null;
    public Text PlayerScoreText { get { return playerScoreText; } }
}