﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LeaderboardUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform leaderboardRectTransform = null;
    [SerializeField]
    private LeaderboardItemUI leaderboardItemUIPrefab = null;
    [SerializeField]
    private Transform gridOfPlayersItemUI = null;

    public static LeaderboardUI Instance;

    private bool isLeaderboardActive;
    public bool IsLeaderboardActive { get { return isLeaderboardActive; } }

    private List<LeaderboardItemUI> leaderboardItemsList = new List<LeaderboardItemUI>();

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("LeaderboardUI instance created more than once!");
            Destroy(gameObject);
            return;
        }
        Instance = this;
        isLeaderboardActive = false;
        EventManager.StartListening(EventType.GameEnded, OnGameHasEnded);
    }

    private void Update()
    {
        if (Game.Instance == null || Game.Instance.hasEnded == null || Game.Instance.hasEnded.Value)
        {
            return;
        }

        if (Input.GetButtonDown("Leaderboard"))
        {
            ShowLeaderboard();
        }
        else if (Input.GetButtonUp("Leaderboard"))
        {
            leaderboardRectTransform.gameObject.SetActive(false);
            if (BattleMenuUI.Instance.IsBattleMenuActive == false && Game.Instance.isStarted.Value)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            isLeaderboardActive = false;
        }
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.GameEnded, OnGameHasEnded);
    }

    public LeaderboardItemUI CreateAndReturnLeaderboardItemUI()
    {
        LeaderboardItemUI newLeaderboardItemUI = Instantiate(leaderboardItemUIPrefab, gridOfPlayersItemUI);
        leaderboardItemsList.Add(newLeaderboardItemUI);
        return newLeaderboardItemUI;
    }

    public void RemoveLeaderboardItemUIFromList(LeaderboardItemUI itemToRemove)
    {
        leaderboardItemsList.Remove(itemToRemove);
    }

    public void ReorderLeaderboardItems()
    {
        leaderboardItemsList = leaderboardItemsList.OrderByDescending(item => int.Parse(item.PlayerScoreText.text)).ToList();
        for (int i = 0; i < leaderboardItemsList.Count; ++i)
        {
            leaderboardItemsList[i].transform.SetAsLastSibling();
        }
    }

    private void OnGameHasEnded()
    {
        ShowLeaderboard();
    }

    private void ShowLeaderboard()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        leaderboardRectTransform.gameObject.SetActive(true);
        isLeaderboardActive = true;
    }
}