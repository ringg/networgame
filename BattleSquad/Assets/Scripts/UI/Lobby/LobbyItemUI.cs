﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyItemUI : MonoBehaviour
{
    [SerializeField]
    private Text playerNameText = null;
    public Text PlayerNameText { get { return playerNameText; } }

    [SerializeField]
    private Toggle isReadyToggle = null;
    public Toggle IsReadyToggle { get { return isReadyToggle; } }
}