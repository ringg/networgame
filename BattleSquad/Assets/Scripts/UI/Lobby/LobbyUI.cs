﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyUI : MonoBehaviour
{
    [SerializeField]
    private RectTransform lobbyUIRectTransform = null;
    [SerializeField]
    private Button toggleReadyButton = null;
    [SerializeField]
    private Button startGameButton = null;
    [SerializeField]
    private LobbyItemUI lobbyItemUIPrefab = null;
    [SerializeField]
    private Transform gridOfPlayersItemUI = null;
    [SerializeField]
    private Text ipAddressValue = null;

    public static LobbyUI Instance;

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("LobbyUI instance created more than once!");
            Destroy(gameObject);
            return;
        }
        Instance = this;
        ToggleShowLobby(false);
    }

    private void Start()
    {
        if (NetObjMgr.instance.IsServer == false)
        {
            startGameButton.gameObject.SetActive(false);
            ipAddressValue.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            ipAddressValue.text = IpHelper.GetLocalIPAddress().ToString();
        }
        if (SyncPlayer.localPlayer == null)
        {
            toggleReadyButton.gameObject.SetActive(false);
        }
        EventManager.StartListening(EventType.GameStarted, OnGameStarted);
    }

    private void OnDestroy()
    {
        EventManager.StopListening(EventType.GameStarted, OnGameStarted);
    }

    private void OnGameStarted()
    {
        ToggleShowLobby(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void OnStartGameButtonClick()
    {
        if (NetObjMgr.instance.players.All(player => player.Value.isReady.Value))
        {
            Game.Instance.OnStartGameClicked();
        }
    }

    public void OnToggleReadyButtonClick()
    {
        SyncPlayer.localPlayer.isReady.Value = !SyncPlayer.localPlayer.isReady.Value;
    }

    public void OnQuitButtonClick()
    {
        ToggleShowLobby(false);
        NetObjMgr.instance.Quit(QuitMode.QuitToMainMenu);
    }

    public LobbyItemUI CreateAndReturnLobbyItemUI()
    {
        LobbyItemUI newLobbyItemUI = Instantiate(lobbyItemUIPrefab, gridOfPlayersItemUI);
        return newLobbyItemUI;
    }

    public void ToggleShowLobby(bool show)
    {
        lobbyUIRectTransform.gameObject.SetActive(show);
    }
}