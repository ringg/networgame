﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour
{
    [SerializeField]
    private Text timerMinutesText = null;
    [SerializeField]
    private Text timerSecondsText = null;

    public void UpdateTimer(float timeInSeconds)
    {
        int minutes = Mathf.CeilToInt(timeInSeconds) / 60;
        int seconds = Mathf.CeilToInt(timeInSeconds) % 60;
        timerMinutesText.text = minutes.ToString("00");
        timerSecondsText.text = seconds.ToString("00");
    }
}