﻿using System;
using NAudio.Wave;
using System.Collections.Generic;

public class EffectStream : WaveStream
{
    public IWaveProvider SourceStream { get; set; }
    public IWaveProvider ModulatingStream { get; set; }
    public List<IEffect> Effects { get; private set; }

    protected int channel = 0;
    protected byte[] helperBuffer;

    public EffectStream(IWaveProvider stream)
    {
        Effects = new List<IEffect>();
        helperBuffer = new byte[0];
        SourceStream = stream;
    }

    public override long Length
    {
        get
        {
            if ((WaveStream)SourceStream != null)
                return ((WaveStream)SourceStream).Length;
            else return long.MaxValue;
        }
    }
    
    public override long Position
    {
        get
        {
            if ((WaveStream)SourceStream != null)
                return ((WaveStream)SourceStream).Position;
            else return 0;
        }
        set
        {
            if ((WaveStream)SourceStream != null)
                ((WaveStream)SourceStream).Position = value;
        }
    }

    public override WaveFormat WaveFormat
    {
        get { return SourceStream.WaveFormat; }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        if (ModulatingStream != null)
        {
            if (helperBuffer.Length < count)
                helperBuffer = new byte[count];
            ModulatingStream.Read(helperBuffer, offset, count);
        }

        int read = SourceStream.Read(buffer, offset, count);
        float sample, helperSample;
        for (int i = 0; i < read / 4; i++)
        {
            sample = BitConverter.ToSingle(buffer, i * 4 + offset);

            if (Effects.Count >= channel)
            {
                if (ModulatingStream != null)
                {
                    helperSample = BitConverter.ToSingle(helperBuffer, i * 4 + offset);
                    sample = Effects[channel].ApplyEffect(sample, helperSample);
                }
                else
                {
                    sample = Effects[channel].ApplyEffect(sample);
                }
                channel = (channel + 1) % WaveFormat.Channels;
            }

            byte[] bytes = BitConverter.GetBytes(sample);
            buffer[i * 4 + 0 + offset] = bytes[0];
            buffer[i * 4 + 1 + offset] = bytes[1];
            buffer[i * 4 + 2 + offset] = bytes[2];
            buffer[i * 4 + 3 + offset] = bytes[3];
        }

        return read;
    }
}