﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IEffect
{
    float ApplyEffect(float sample);
    float ApplyEffect(float sample, float modulatingSample);
}

public class Echo : IEffect
{
    public int EchoLength;
    public float EchoFactor;

    private Queue<float> samples;

    public Echo(int length = 20000, float factor = 0.5f)
    {
        this.EchoLength = length;
        this.EchoFactor = factor;
        this.samples = new Queue<float>();

        for (int i = 0; i < length; i++) samples.Enqueue(0f);
    }

    public float ApplyEffect(float sample)
    {
        samples.Enqueue(sample);
        return Math.Min(1, Math.Max(-1, sample + EchoFactor * samples.Dequeue()));
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        return ApplyEffect(sample);
    }
}

public class LowPass : IEffect
{
    public float baseF;
    public float f, Q;
    public float min;

    EffectStream estream;
    float fs, TwoPI = 2 * 3.1415f, s, c, alfa, r;
    float a0, a1, a2, b1, b2;
    float yn, yn1, yn2;
    float xn, xn1, xn2;

    public LowPass(EffectStream estream, float f, float Q, float min = 0.5f)
    {
        this.estream = estream;
        this.baseF = f;
        this.f = f;
        this.Q = Q;
        this.min = min;

        RecalcParams();
    }

    void RecalcParams()
    {
        fs = estream.WaveFormat.SampleRate;
        s = Mathf.Sin(TwoPI * f / fs);
        c = Mathf.Cos(TwoPI * f / fs);
        alfa = s / (2 * Q);
        r = 1 / (1 + alfa);

        a0 = 0.5f * (1 - c) * r;
        a1 = (1 - c) * r;
        a2 = a0;
        b1 = -2 * c * r;
        b2 = (1 - alfa) * r;
    }

    public float ApplyEffect(float sample)
    {
        xn2 = xn1;
        xn1 = xn;
        xn = sample;
        yn2 = yn1;
        yn1 = yn;
        yn = a0 * xn + a1 * xn1 + a2 * xn2 - b1 * yn1 - b2 * yn2;

        return yn;
    }
    
    public float ApplyEffect(float sample, float modulatingSample)
    {
        f = Mathf.Abs(Mathf.Lerp(min, 1, (modulatingSample + 1) * 0.5f) * baseF);
        RecalcParams();
        return ApplyEffect(sample);
    }
}

public class HighPass : IEffect
{
    public float baseF;
    public float f, Q;
    public float min;

    EffectStream estream;
    float fs, TwoPI = 2 * 3.1415f, s, c, alfa, r;
    float a0, a1, a2, b1, b2;
    float yn, yn1, yn2;
    float xn, xn1, xn2;

    public HighPass(EffectStream estream, float f, float Q, float min = 0.5f)
    {
        this.estream = estream;
        this.baseF = f;
        this.f = f;
        this.Q = Q;
        this.min = min;

        RecalcParams();
    }

    void RecalcParams()
    {
        fs = estream.WaveFormat.SampleRate;
        s = Mathf.Sin(TwoPI * f / fs);
        c = Mathf.Cos(TwoPI * f / fs);
        alfa = s / (2 * Q);
        r = 1 / (1 + alfa);

        a0 = 0.5f * (1+c) * r;
        a1 = -(1 + c) * r;
        a2 = a0;
        b1 = -2 * c * r;
        b2 = (1 - alfa) * r;
    }

    public float ApplyEffect(float sample)
    {
        xn2 = xn1;
        xn1 = xn;
        xn = sample;
        yn2 = yn1;
        yn1 = yn;
        yn = a0 * xn + a1 * xn1 + a2 * xn2 - b1 * yn1 - b2 * yn2;

        return yn;
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        f = Mathf.Abs(Mathf.Lerp(min, 1, (modulatingSample + 1) * 0.5f) * baseF);
        RecalcParams();
        return ApplyEffect(sample);
    }
}

public class BandPass : IEffect
{
    public float baseF;
    public float f, Q;
    public float min;

    EffectStream estream;
    float fs, TwoPI = 2 * 3.1415f, s, c, alfa, r;
    float a0, a1, a2, b1, b2;
    float yn, yn1, yn2;
    float xn, xn1, xn2;

    public BandPass(EffectStream estream, float f, float Q, float min = 0.5f)
    {
        this.estream = estream;
        this.baseF = f;
        this.f = f;
        this.Q = Q;
        this.min = min;

        RecalcParams();
    }

    void RecalcParams()
    {
        fs = estream.WaveFormat.SampleRate;
        s = Mathf.Sin(TwoPI * f / fs);
        c = Mathf.Cos(TwoPI * f / fs);
        alfa = s / (2 * Q);
        r = 1 / (1 + alfa);

        a0 = alfa * r;
        a1 = 0;
        a2 = -a0;
        b1 = -2 * c * r;
        b2 = (1 - alfa) * r;
    }

    public float ApplyEffect(float sample)
    {
        xn2 = xn1;
        xn1 = xn;
        xn = sample;
        yn2 = yn1;
        yn1 = yn;
        yn = a0 * xn + a1 * xn1 + a2 * xn2 - b1 * yn1 - b2 * yn2;

        return yn;
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        f = Mathf.Abs(Mathf.Lerp(min, 1, (modulatingSample + 1) * 0.5f) * baseF);
        RecalcParams();
        return ApplyEffect(sample);
    }
}

public class Impulser : IEffect
{
    float progress;
    public int duration;
    int nr;

    Func<float, float> shape;

    public Impulser(int duration, Func<float, float> shape)
    {
        this.duration = duration;
        this.shape = shape;
        nr = duration;
    }

    public void Impulse()
    {
        nr = 0;
    }

    public float ApplyEffect(float sample)
    {
        if (nr >= duration) return 0;
        progress = (float)nr / duration;
        ++nr;
        return shape(progress) * sample;
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        duration = (int)(modulatingSample * 50000);
        return ApplyEffect(sample);
    }
}

public class Amplifier : IEffect
{
    float multiplier = 1.0f;

    public Amplifier(float multiplier)
    {
        this.multiplier = multiplier;
    }

    public float ApplyEffect(float sample)
    {
        return sample * multiplier;
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        return sample * modulatingSample;
    }
}

public class Flanger : IEffect
{
    float sweepFreqBase = 0.3125f;

    int delay = 15;
    int range = 12;
    float sweepFreq = 0.3125f * 2;
    int curID = 0;
    float curPhase;

    float[] samples;

    public Flanger(float range, float delay, float sweepFreq)
    {
        this.delay = (int)delay;
        this.range = (int)range;
        this.sweepFreq = sweepFreq;
        this.sweepFreqBase = sweepFreq;

        this.samples = new float[this.delay + 2 * this.range];
    }

    float result;
    int offset, id;
    public float ApplyEffect(float sample)
    {
        curPhase += 2 * Mathf.PI * sweepFreq / 44100;
        offset = Mathf.RoundToInt(range * Mathf.Sin(curPhase));
        id = (curID - delay + offset) % (delay + 2 * range);
        if (id < 0) id += delay + 2 * range;

        result = (sample + samples[id]) * 0.5f;

        samples[curID % (delay + 2 * range)] = sample;
        curID++;

        return result;
    }


    public float ApplyEffect(float sample, float modulatingSample)
    {
        sweepFreq = sweepFreqBase * modulatingSample;
        return ApplyEffect(sample);
    }
}

public class ADSR : IEffect
{
    public int duration = 10000;
    int sampleID;
    float attackEnd = 0.25f;
    float decayEnd = 0.5f;
    float sustainEnd = 0.75f;
    // release

    float sustainVal = 0.25f;

    public ADSR(int length = 10000)
    {
        this.duration = length;
        this.sampleID = length;
    }
    
    public void Impulse()
    {
        sampleID = 0;
    }

    public float ApplyEffect(float sample)
    {
        if (sample >= duration) return 0;

        float progress = (float)sampleID / duration;
        sampleID++;

        if (progress < attackEnd)
        {
            progress = progress / attackEnd;
            return sample * progress;
        }
        if (progress < decayEnd)
        {
            progress = (progress - attackEnd) / (decayEnd - attackEnd);
            return sample * Mathf.Lerp(1, sustainVal, progress);
        }
        if (progress < sustainEnd)
        {
            return sample * sustainVal;
        }
        if (progress < 1)
        {
            progress = (progress - sustainEnd) / (1 - sustainEnd);
            return sample * Mathf.Lerp(sustainVal, 0, progress);
        }

        return 0;
    }

    public float ApplyEffect(float sample, float modulatingSample)
    {
        return ApplyEffect(sample);
    }
}