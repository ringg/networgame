﻿using NAudio.Wave;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FileAmplifier : MonoBehaviour
{
    public FileAmplifyingUI fileAmplifyingUI { get; set; }

    private float[] sampleBuffer;
    private float maxValue;
    private int originalFileSampleRate;
    private int originalFileChannels;

    public void LoadFile(string filePath)
    {
        try
        {
            using (AudioFileReader reader = new AudioFileReader(filePath))
            {
                originalFileSampleRate = reader.WaveFormat.SampleRate;
                originalFileChannels = reader.WaveFormat.Channels;

                // Reader Length represents length for byte data.
                // Because float consists f 4 bytes, we need
                // four times smaller buffer.
                sampleBuffer = new float[reader.Length / sizeof(float)];
                reader.Read(sampleBuffer, 0, sampleBuffer.Length);
            }

            maxValue = sampleBuffer.Max(value => Mathf.Abs(value));

            fileAmplifyingUI.ShowLoadedFilePeakAmplitude(maxValue);
            fileAmplifyingUI.ToggleAmplifyInteractibility(true);
            fileAmplifyingUI.ShowStatusText("File " + filePath + " loaded successfuly.", ActionStatusType.Successful);
        }
        catch (System.Exception e)
        {
            fileAmplifyingUI.ShowStatusText("Error loading a file " + filePath + ". Reason: " + e.Message, ActionStatusType.Failed);
        }
    }

    public void AmplifySamples(float newMax)
    {
        try
        {
            //newMax = multiplier * maxValue
            //multiplier = newMax / maxValue
            float multiplier = newMax / maxValue;
            for (int i = 0; i < sampleBuffer.Length; ++i)
            {
                sampleBuffer[i] = sampleBuffer[i] * multiplier;
            }
            fileAmplifyingUI.ShowLoadedFilePeakAmplitude(newMax);
            fileAmplifyingUI.ShowStatusText("Data successfuly amplified.", ActionStatusType.Successful);
        }
        catch (System.Exception e)
        {
            fileAmplifyingUI.ShowStatusText("Error amplifying data. Reason: " + e.Message, ActionStatusType.Failed);
        }
    }

    public void SaveToFile(string filePath)
    {
        try
        {
            int bitsPerSample = fileAmplifyingUI.GetSavedFileSelectedBits();

            WaveFormat destinationWaveFormat;

            if (bitsPerSample == 16)
            {
                destinationWaveFormat = new WaveFormat(originalFileSampleRate, bitsPerSample, originalFileChannels);
            }
            else
            {
                destinationWaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(originalFileSampleRate, originalFileChannels);
            }

            using (WaveFileWriter writer = new WaveFileWriter(filePath, destinationWaveFormat))
            {
                writer.WriteSamples(sampleBuffer, 0, sampleBuffer.Length);
            }

            fileAmplifyingUI.ShowStatusText("File successfuly saved.", ActionStatusType.Successful);
        }
        catch (System.Exception e)
        {
            fileAmplifyingUI.ShowStatusText("Error saving a file " + filePath + ". Reason: " + e.Message, ActionStatusType.Failed);
        }
    }
}