﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class MathHelper
{
    public static float LinearInter(float x) { return x; }
    public static float SquareInter(float x) { return x * x; }
    public static float OneMinusSquareInter(float x) { return 1 - x * x; }
    public static float SmoothInter(float x) { return x * x * 3 - x * x * x * 2; }
    public static float ParaboleInter(float x) { return x - x * x; }

    public static float Remap(float value, float low1, float high1, float low2, float high2)
    {
        return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
    }
}
