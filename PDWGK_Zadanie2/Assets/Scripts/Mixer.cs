﻿using NAudio.Wave;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Mixer : MonoBehaviour
{
    public List<WaveGenerator> waveGenerators;

    private WaveOut waveOut;
    private bool isPlaing;
    private bool wasPlayed;
    public bool IsPlaing
    {
        get { return isPlaing; }
    }

    private void Awake()
    {
        isPlaing = false;
        wasPlayed = false;
        waveOut = new WaveOut();
    }

    public void OnPlayClick()
    {
        if (isPlaing)
        {
            return;
        }
        if (wasPlayed)
        {
            waveOut.Dispose();
            waveOut = new WaveOut();
        }
        MixingWaveProvider32 mixer = new MixingWaveProvider32(waveGenerators.Where(generator => generator.ShouldBeSubjectedToEffects == false));
        waveOut.Init(mixer);

        wasPlayed = true;

        waveOut.Play();
        isPlaing = true;
    }

    public void OnStopClick()
    {
        if (isPlaing == false)
        {
            return;
        }
        if (waveOut != null)
        {
            waveOut.Stop();
        }
        isPlaing = false;
    }

    private void OnDestroy()
    {
        if (waveOut != null)
        {
            waveOut.Stop();
            waveOut.Dispose();
            waveOut = null;
        }
    }
}