﻿using NAudio.FileFormats.Mp3;
using NAudio.Utils;
using NAudio.Wave;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EchoTester : MonoBehaviour
{
    public bool playAtBegin, playMelodyAtBegin;
    IWavePlayer waveOutDevice;
    EffectStream es;

    public WaveGenerator[] baseSignals;
    public Mixer mixerWithSignals;
    public List<EffectInfo> effects;

    private Coroutine currentPlayCoroutine;
    private bool isPlaing;
    private WaveGenerator[] signalsWithEffectApplied;

    void Start()
    {
        if (baseSignals.Length > 0)
        {
            for (int i = 0; i < baseSignals.Length; ++i)
            {
                baseSignals[i].Amplitude = (float)Decibels.DecibelsToLinear(-10.0);
            }
            es = new EffectStream(new MixingWaveProvider32(baseSignals));
            for (int i = 0; i < effects.Count; ++i)
            {
                if (effects[i].signal != null)
                    effects[i].signal.Amplitude = (float)Decibels.DecibelsToLinear(-1.0);
                es = ApplyEffect(es, effects[i]);
                if (i != effects.Count - 1)
                    es = new EffectStream(es);
            }
        }

        if (playAtBegin)
        {
            waveOutDevice = new WaveOut();
            waveOutDevice.Init(es);
            waveOutDevice.Play();
            isPlaing = true;
        }
        if (playMelodyAtBegin && adsr != null)
        {
            currentPlayCoroutine = StartCoroutine(Impulse());
        }
    }

    #region instrument
    Impulser impulser;
    ADSR adsr;
    float[] frequencies = new float[] { 391.9f, 329.6f, 329.6f, 349.6f, 293.7f, 293.7f, 261.6f, 329.6f, 391.9f };
    float[] durations = new float[] { 400, 400, 400, 400, 400, 400, 200, 200, 400 };

    public void StartPlaing(bool melody)
    {
        if (isPlaing)
        {
            return;
        }
        if (baseSignals.Length > 0)
        {
            es = new EffectStream(new MixingWaveProvider32(baseSignals));
        }
        else
        {
            signalsWithEffectApplied = mixerWithSignals.waveGenerators.Where(generator => generator.ShouldBeSubjectedToEffects).ToArray();
            es = new EffectStream(new MixingWaveProvider32(signalsWithEffectApplied));
        }
        for (int i = 0; i < effects.Count; ++i)
        {
            es = ApplyEffect(es, effects[i]);
            if (i != effects.Count - 1)
                es = new EffectStream(es);
        }
        waveOutDevice = new WaveOut();
        waveOutDevice.Init(es);
        waveOutDevice.Play();
        if (melody && adsr != null)
        {
            currentPlayCoroutine = StartCoroutine(Impulse());
        }
        isPlaing = true;
    }

    public void StopPlaing()
    {
        if (isPlaing == false)
        {
            return;
        }
        if (currentPlayCoroutine != null)
        {
            StopCoroutine(currentPlayCoroutine);
            currentPlayCoroutine = null;
        }
        waveOutDevice.Stop();
        waveOutDevice.Dispose();
        waveOutDevice = null;
        isPlaing = false;
    }

    IEnumerator Impulse()
    {
        yield return new WaitForSeconds(1);

        int i = 0;
        while (true)
        {
            if (baseSignals.Length > 0)
            {
                for (int j = 0; j < baseSignals.Length; ++j)
                {
                    baseSignals[j].Frequency = frequencies[i];
                }
            }
            else
            {
                for (int j = 0; j < signalsWithEffectApplied.Length; ++j)
                {
                    signalsWithEffectApplied[j].Frequency = frequencies[i];
                }
            }
            adsr.duration = (int)(durations[i] * 44.1f);
            adsr.Impulse();
            yield return new WaitForSeconds(durations[i] / 1000);
            i++;
            if (i == durations.Length)
                yield return new WaitForSeconds(2);
            i %= durations.Length;
        }
    }
    #endregion

    void Update()
    {
        if (playMelodyAtBegin && Input.GetKeyDown(KeyCode.Q))
        {
            SendImpulseWithoutDuration();
        }
    }

    public void SendImpulse(float duration)
    {
        int trueDuration = (int)(duration * 44.1f);
        if (impulser != null)
        {
            impulser.duration = trueDuration;
            impulser.Impulse();
        }
        if (adsr != null)
        {
            adsr.duration = trueDuration;
            adsr.Impulse();
        }
    }

    private void SendImpulseWithoutDuration()
    {
        if (impulser != null)
        {
            impulser.Impulse();
        }
        if (adsr != null)
        {
            adsr.Impulse();
        }
    }

    EffectStream ApplyEffect(EffectStream stream, EffectInfo effect)
    {
        stream.ModulatingStream = effect.signal;
        switch (effect.type)
        {
            case EffectType.LowPass:
                stream.Effects.Add(new LowPass(stream, effect.a, effect.b, effect.c));
                break;
            case EffectType.HighPass:
                stream.Effects.Add(new HighPass(stream, effect.a, effect.b, effect.c));
                break;
            case EffectType.BandPass:
                stream.Effects.Add(new BandPass(stream, effect.a, effect.b, effect.c));
                break;
            case EffectType.Echo:
                stream.Effects.Add(new Echo((int)effect.a, effect.b));
                break;
            case EffectType.Amplifier:
                stream.Effects.Add(new Amplifier(effect.a));
                break;
            case EffectType.Impulser:
                impulser = new Impulser((int)effect.a, MathHelper.ParaboleInter);
                stream.Effects.Add(impulser);
                break;
            case EffectType.Flanger:
                stream.Effects.Add(new Flanger(effect.a, effect.b, effect.c));
                break;
            case EffectType.ASDR:
                adsr = new ADSR((int)effect.a);
                stream.Effects.Add(adsr);
                break;
        }

        return stream;
    }

    private void OnDisable()
    {
        if (currentPlayCoroutine != null)
        {
            StopCoroutine(currentPlayCoroutine);
            currentPlayCoroutine = null;
        }
    }

    void OnDestroy()
    {
        if (waveOutDevice != null)
        {
            waveOutDevice.Stop();
        }
        if (waveOutDevice != null)
        {
            waveOutDevice.Dispose();
            waveOutDevice = null;
        }
    }
}

public enum EffectType
{
    LowPass,
    HighPass,
    BandPass,
    Echo,
    Amplifier,
    Impulser,
    Flanger,
    ASDR,
}

[System.Serializable]
public class EffectInfo
{
    public EffectType type;
    public WaveGenerator signal;
    public float a, b, c;
}