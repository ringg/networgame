﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookmarksUI : MonoBehaviour
{
    [SerializeField]
    private Button[] bookmarkButtons = null;
    [SerializeField]
    private GameObject[] bookmarks = null;

    private int currentBookmarkIndex;
    private Button currentBookmarkButton;
    private GameObject currentBookmark;

    private void Awake()
    {
        foreach (GameObject go in bookmarks)
        {
            go.SetActive(false);
        }
        currentBookmarkIndex = -1;
    }

    public void OnBookmarkClicked(int newBookmarkIndex)
    {
        if (newBookmarkIndex == currentBookmarkIndex)
        {
            return;
        }
        currentBookmarkIndex = newBookmarkIndex;
        if (currentBookmarkButton != null)
        {
            ColorBlock buttonColors = currentBookmarkButton.colors;
            buttonColors.normalColor = new Color32(214, 214, 214, 255);
            currentBookmarkButton.colors = buttonColors;
        }
        if (currentBookmark != null)
        {
            currentBookmark.SetActive(false);
        }
        currentBookmark = bookmarks[newBookmarkIndex];
        currentBookmark.gameObject.SetActive(true);
        currentBookmarkButton = bookmarkButtons[newBookmarkIndex];
        ColorBlock newButtonColors = currentBookmarkButton.colors;
        newButtonColors.normalColor = Color.white;
        currentBookmarkButton.colors = newButtonColors;
    }
}