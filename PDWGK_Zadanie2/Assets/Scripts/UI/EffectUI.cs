﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EffectUI : MonoBehaviour
{
    [SerializeField]
    private Text typeText = null;
    [SerializeField]
    private Button waveGeneratorUIButton = null;

    public InstrumentUI CurrentInstrumentUI { get; set; }

    public EffectInfo MyEffectInfo { get; set; }

    private void Start()
    {
        typeText.text = MyEffectInfo.type.ToString() + " generator";
    }

    public void OnEffectUIClick()
    {
        ColorBlock buttonColors = waveGeneratorUIButton.colors;
        buttonColors.normalColor = new Color32(255, 255, 255, 255);
        waveGeneratorUIButton.colors = buttonColors;
        EventSystem.current.SetSelectedGameObject(null);
        CurrentInstrumentUI.OnEffectSelected(this);
    }

    public void OnEffectUIDeselected()
    {
        ColorBlock buttonColors = waveGeneratorUIButton.colors;
        buttonColors.normalColor = new Color32(214, 214, 214, 255);
        waveGeneratorUIButton.colors = buttonColors;
    }
}