﻿using NAudio.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FileAmplifyingUI : MonoBehaviour
{
    private FileAmplifier currentFileAmplifier;
    private float newMaxValueInDecibels;

    [SerializeField]
    private InputField inputFilePath = null;
    [SerializeField]
    private InputField outputFilePath = null;
    [SerializeField]
    private Text fileAmplitudeText = null;
    [SerializeField]
    private InputField newFileAmplitudeText = null;
    [SerializeField]
    private Slider newFileAmplitudeSlider = null;
    [SerializeField]
    private Button convertButton = null;
    [SerializeField]
    private Button saveFileButton = null;
    [SerializeField]
    private Text statusText = null;
    [SerializeField]
    private Dropdown bitsValueDropdown = null;

    public void OnLoadFileClick()
    {
        if (currentFileAmplifier != null)
        {
            Destroy(currentFileAmplifier.gameObject);
        }
        GameObject newGO = new GameObject("fileAmplifier");
        currentFileAmplifier = newGO.AddComponent<FileAmplifier>();
        currentFileAmplifier.fileAmplifyingUI = this;
        ShowStatusText("Loading file, please wait...", ActionStatusType.InProgress);
        StartCoroutine(PerformActionAfterOneFrame(() => currentFileAmplifier.LoadFile(inputFilePath.text) ));
        //currentFileAmplifier.LoadFile(inputFilePath.text);
    }

    public void ShowLoadedFilePeakAmplitude(float linearValue)
    {
        float decibelsValue = (float)Decibels.LinearToDecibels(linearValue);
        fileAmplitudeText.text = decibelsValue.ToString();
        newFileAmplitudeSlider.value = decibelsValue;
    }

    public void OnConvertFileClick()
    {
        ShowStatusText("Amplifying data, please wait...", ActionStatusType.InProgress);
        StartCoroutine(PerformActionAfterOneFrame( () => currentFileAmplifier.AmplifySamples((float)Decibels.DecibelsToLinear(newMaxValueInDecibels)) ));
        //currentFileAmplifier.AmplifySamples((float)Decibels.DecibelsToLinear(newMaxValueInDecibels));
    }

    public void OnSaveFileClick()
    {
        ShowStatusText("Saving file, please wait...", ActionStatusType.InProgress);
        StartCoroutine(PerformActionAfterOneFrame( () => currentFileAmplifier.SaveToFile(outputFilePath.text) ));
        //currentFileAmplifier.SaveToFile(outputFilePath.text);
    }

    public void OnAmplifySliderValueChanged(float value)
    {
        newMaxValueInDecibels = value;
        newFileAmplitudeText.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        newFileAmplitudeText.text = value.ToString();
        newFileAmplitudeText.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    public void OnAmplifyInputFieldValueChanged(string value)
    {
        float parsedValue;
        bool parsed = float.TryParse(value, out parsedValue);
        if (parsed == false)
        {
            newFileAmplitudeText.text = newMaxValueInDecibels.ToString();
            return;
        }
        newFileAmplitudeSlider.value = parsedValue;
    }

    public void ToggleAmplifyInteractibility(bool interactible)
    {
        newFileAmplitudeSlider.interactable = interactible;
        newFileAmplitudeText.interactable = interactible;
        convertButton.interactable = interactible;
        saveFileButton.interactable = interactible;
    }

    public void ShowStatusText(string text, ActionStatusType status)
    {
        statusText.text = text;
        switch (status)
        {
            case ActionStatusType.Successful:
                statusText.color = Color.green / 2.0F;
                break;
            case ActionStatusType.InProgress:
                statusText.color = Color.black;
                break;
            case ActionStatusType.Failed:
                statusText.color = Color.red / 2.0F;
                break;
        }
    }

    public int GetSavedFileSelectedBits()
    {
        if (bitsValueDropdown.value == 0)
        {
            return 16;
        }
        else
        {
            return 32;
        }
    }

    private IEnumerator PerformActionAfterOneFrame(Action actionToPerform)
    {
        yield return null;
        actionToPerform();
    }

    private void OnEnable()
    {
        ToggleAmplifyInteractibility(false);
        ShowStatusText("Ready", ActionStatusType.InProgress);
    }

    private void OnDisable()
    {
        if (currentFileAmplifier != null)
        {
            Destroy(currentFileAmplifier.gameObject);
        }
    }
}

public enum ActionStatusType
{
    Successful,
    InProgress,
    Failed
}