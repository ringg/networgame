﻿using NAudio.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorsUI : MonoBehaviour
{
    private static readonly float powFactor = 5.0F;

    [SerializeField]
    private Dropdown generatorTypeDropDown = null;
    [SerializeField]
    private Mixer ourMixer = null;
    [SerializeField]
    private RectTransform gridOfGeneratorUIs = null;
    [SerializeField]
    WaveGeneratorUI generaturUIPrefab = null;
    [SerializeField]
    private Slider gainSlider = null;
    [SerializeField]
    private InputField gainTextValue = null;
    [SerializeField]
    private Slider frequencySlider = null;
    [SerializeField]
    private InputField frequenctTextValue = null;
    [SerializeField]
    private Button playButton = null;
    [SerializeField]
    private Button addGeneratorButton = null;
    [SerializeField]
    private Button removeSelectedGeneratorButton = null;

    private WaveGeneratorUI currentlySelectedGeneratorUI;
    public WaveGeneratorUI CurrentlySelectedGeneratorUI
    {
        get { return currentlySelectedGeneratorUI; }
    }

    public void OnAddGeneratorClick()
    {
        GameObject newGeneratorGO = new GameObject(generatorTypeDropDown.captionText.text + ourMixer.waveGenerators.Count);
        WaveGenerator newGenerator = null;
        switch ((WaveGeneratorType)generatorTypeDropDown.value)
        {
            case WaveGeneratorType.Sin:
                newGenerator = newGeneratorGO.AddComponent<SineWaveGenerator>();
                break;
            case WaveGeneratorType.Square:
                newGenerator = newGeneratorGO.AddComponent<SquareWaveGenerator>();
                break;
            case WaveGeneratorType.Triangle:
                newGenerator = newGeneratorGO.AddComponent<TriangleWaveGenerator>();
                break;
            case WaveGeneratorType.SawTooth:
                newGenerator = newGeneratorGO.AddComponent<SawToothGenerator>();
                break;
            case WaveGeneratorType.WhiteNoise:
                newGenerator = newGeneratorGO.AddComponent<WhiteNoiseGenerator>();
                break;
            case WaveGeneratorType.PinkNoise:
                newGenerator = newGeneratorGO.AddComponent<PinkNoiseGenerator>();
                break;
            case WaveGeneratorType.RedNoise:
                newGenerator = newGeneratorGO.AddComponent<RedNoiseGenerator>();
                break;
        }
        ourMixer.waveGenerators.Add(newGenerator);
        WaveGeneratorUI newGeneratorUI = Instantiate(generaturUIPrefab, gridOfGeneratorUIs, false);
        newGeneratorUI.CurrentGeneratorsUI = this;
        newGeneratorUI.MyWaveGanerator = newGenerator;
    }

    private void TogglePlayStopInteractibility(bool interactible)
    {
        playButton.interactable = interactible;
        addGeneratorButton.interactable = interactible;
        removeSelectedGeneratorButton.interactable = interactible;
    }

    public void ToggleWaveParametersInteractibility(bool gainInteractible, bool frequencyInteractible)
    {
        gainSlider.interactable = gainInteractible;
        gainTextValue.interactable = gainInteractible;
        frequencySlider.interactable = frequencyInteractible;
        frequenctTextValue.interactable = frequencyInteractible;
    }

    public void OnPlayClick()
    {
        ourMixer.OnPlayClick();
        TogglePlayStopInteractibility(false);
    }

    public void OnStopClick()
    {
        ourMixer.OnStopClick();
        TogglePlayStopInteractibility(true);
    }

    public void OnRemoveSelectedGeneratorClick()
    {
        ourMixer.waveGenerators.Remove(currentlySelectedGeneratorUI.MyWaveGanerator);
        Destroy(currentlySelectedGeneratorUI.MyWaveGanerator.gameObject);
        Destroy(currentlySelectedGeneratorUI.gameObject);
        currentlySelectedGeneratorUI = null;
        ToggleWaveParametersInteractibility(false, false);
    }

    public void OnWaveGeneratorSelected(WaveGeneratorUI selectedWaveGeneratorUI)
    {
        if (currentlySelectedGeneratorUI != null && currentlySelectedGeneratorUI != selectedWaveGeneratorUI)
        {
            currentlySelectedGeneratorUI.OnVaweGeneratorUIDeselected();
        }
        currentlySelectedGeneratorUI = selectedWaveGeneratorUI;
        ToggleWaveParametersInteractibility(selectedWaveGeneratorUI.MyWaveGanerator.IsAllowedToPlay, selectedWaveGeneratorUI.MyWaveGanerator.GetWaveGeneratorType() < WaveGeneratorType.WhiteNoise);
        UpdateWaveGeneratorParameters();
    }

    public void UpdateWaveGeneratorParameters()
    {
        UpdateSliderValue(gainSlider, currentlySelectedGeneratorUI.MyWaveGanerator.IsAllowedToPlay ? (float)Decibels.LinearToDecibels(currentlySelectedGeneratorUI.MyWaveGanerator.Amplitude) : -100.0F);
        UpdateInputFieldValue(gainTextValue, currentlySelectedGeneratorUI.MyWaveGanerator.IsAllowedToPlay ? (float)Decibels.LinearToDecibels(currentlySelectedGeneratorUI.MyWaveGanerator.Amplitude) : -100.0F);
        UpdateSliderValue(frequencySlider, Mathf.Pow(MathHelper.Remap(currentlySelectedGeneratorUI.MyWaveGanerator.Frequency, 20.0F, 20000.0F, 0.0F, 1.0F), 1.0F / powFactor));
        UpdateInputFieldValue(frequenctTextValue, currentlySelectedGeneratorUI.MyWaveGanerator.Frequency);
    }

    private void UpdateSliderValue(Slider slider, float value)
    {
        slider.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        slider.value = value;
        slider.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    private void UpdateInputFieldValue(InputField inputField, float value)
    {
        inputField.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        inputField.text = value.ToString();
        inputField.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    public void OnGainSliderValueChanged(float newValue)
    {
        float clampedValue = Mathf.Clamp01((float)Decibels.DecibelsToLinear(newValue));
        currentlySelectedGeneratorUI.MyWaveGanerator.Amplitude = clampedValue;
        UpdateInputFieldValue(gainTextValue, newValue);
    }

    public void OnGainInputFieldValueChanged(string newValue)
    {
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false || (currentlySelectedGeneratorUI.MyWaveGanerator.IsAllowedToPlay == false))
        {
            gainTextValue.text = (currentlySelectedGeneratorUI.MyWaveGanerator.IsAllowedToPlay ? (float)Decibels.LinearToDecibels(currentlySelectedGeneratorUI.MyWaveGanerator.Amplitude) : -100.0F).ToString();
            return;
        }
        gainSlider.value = Mathf.Clamp(value, -100.0F, 0.0F);
    }

    public void OnFrequencySliderValueChanged(float newValue)
    {
        float linearToLogaritmicValue = Mathf.Pow(newValue, powFactor);
        float remapedValue = MathHelper.Remap(linearToLogaritmicValue, 0.0F, 1.0F, 20.0F, 20000.0F);
        float clampedValue = Mathf.Round(remapedValue);
        currentlySelectedGeneratorUI.MyWaveGanerator.Frequency = clampedValue;
        UpdateInputFieldValue(frequenctTextValue, clampedValue);
    }

    public void OnFrequencyInputFieldValueChanged(string newValue)
    {
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            frequenctTextValue.text = currentlySelectedGeneratorUI.MyWaveGanerator.Frequency.ToString();
            return;
        }
        frequencySlider.value = Mathf.Pow(MathHelper.Remap(Mathf.Clamp(Mathf.Round(value), 20.0F, 20000.0F), 20.0F, 20000.0F, 0.0F, 1.0F), 1.0F / powFactor);
    }

    private void OnDisable()
    {
        //if (ourMixer.IsPlaing)
        //{
        //    OnStopClick();
        //}
    }
}