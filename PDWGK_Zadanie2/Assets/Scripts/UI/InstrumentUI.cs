﻿using NAudio.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstrumentUI : MonoBehaviour
{
    private static readonly float powFactor = 5.0F;

    private EchoTester modifableEchoTester;
    private EffectUI currentylSelectedEffectUI;
    private List<EchoTester> currentlyPlaingEchoTesters;

    [SerializeField]
    private EchoTester[] echoTesters = null;
    [SerializeField]
    private EffectUI effectUIPrefab = null;
    [SerializeField]
    private RectTransform gridOfEffectUIs = null;
    [SerializeField]
    private Slider gainSlider = null;
    [SerializeField]
    private InputField gainTextValue = null;
    [SerializeField]
    private Slider frequencySlider = null;
    [SerializeField]
    private InputField frequenctTextValue = null;

    [SerializeField]
    private Dropdown effectSignalDropdown = null;
    [SerializeField]
    private InputField parameterAInputField = null;
    [SerializeField]
    private InputField parameterBInputField = null;
    [SerializeField]
    private InputField parameterCInputField = null;
    [SerializeField]
    private Dropdown effectTypeDropdown = null;
    [SerializeField]
    private InputField durationInputField = null;
    [SerializeField]
    private Toggle playMelodyToggle = null;

    [SerializeField]
    private Button[] buttonsToDisableDuringPlay = null;

    private void Awake()
    {
        modifableEchoTester = echoTesters[0];
    }

    private void Start()
    {
        CoroutinesHelder.Instance.StartCoroutine(QKey());
    }

    private IEnumerator QKey()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                OnSendImpulseClick();
            }
            yield return null;
        }
    }

    public void OnAddEffectClick()
    {
        EffectInfo newEffectInfo = new EffectInfo() { type = (EffectType)effectTypeDropdown.value };
        modifableEchoTester.effects.Add(newEffectInfo);
        EffectUI newEffectUI = Instantiate(effectUIPrefab, gridOfEffectUIs, false);
        newEffectUI.CurrentInstrumentUI = this;
        newEffectUI.MyEffectInfo = newEffectInfo;
    }

    public void OnEffectSelected(EffectUI effectUI)
    {
        if (currentylSelectedEffectUI != null && currentylSelectedEffectUI != effectUI)
        {
            currentylSelectedEffectUI.OnEffectUIDeselected();
        }
        currentylSelectedEffectUI = effectUI;
        UpdateWaveGeneratorParameters();
    }

    public void UpdateWaveGeneratorParameters()
    {
        if (currentylSelectedEffectUI.MyEffectInfo.signal != null)
        {
            UpdateSliderValue(gainSlider, (float)Decibels.LinearToDecibels(currentylSelectedEffectUI.MyEffectInfo.signal.Amplitude));
            UpdateInputFieldValue(gainTextValue, (float)Decibels.LinearToDecibels(currentylSelectedEffectUI.MyEffectInfo.signal.Amplitude));
            UpdateSliderValue(frequencySlider, Mathf.Pow(MathHelper.Remap(currentylSelectedEffectUI.MyEffectInfo.signal.Frequency, 0.001F, 20000.0F, 0.0F, 1.0F), 1.0F / powFactor));
            UpdateInputFieldValue(frequenctTextValue, currentylSelectedEffectUI.MyEffectInfo.signal.Frequency);
            SetEffectSignalDropdownValue((int)currentylSelectedEffectUI.MyEffectInfo.signal.GetWaveGeneratorType());
        }
        else
        {
            UpdateSliderValue(gainSlider, -100.0F);
            UpdateInputFieldValue(gainTextValue, -100.0F);
            UpdateSliderValue(frequencySlider, 0.0F);
            UpdateInputFieldValue(frequenctTextValue, 0.0F);
            SetEffectSignalDropdownValue(effectSignalDropdown.options.Count - 1);
        }
        parameterAInputField.text = currentylSelectedEffectUI.MyEffectInfo.a.ToString();
        parameterBInputField.text = currentylSelectedEffectUI.MyEffectInfo.b.ToString();
        parameterCInputField.text = currentylSelectedEffectUI.MyEffectInfo.c.ToString();
    }

    private void SetEffectSignalDropdownValue(int value)
    {
        effectSignalDropdown.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        effectSignalDropdown.value = value;
        effectSignalDropdown.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    private void UpdateSliderValue(Slider slider, float value)
    {
        slider.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        slider.value = value;
        slider.onValueChanged.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    private void UpdateInputFieldValue(InputField inputField, float value)
    {
        inputField.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
        inputField.text = value.ToString();
        inputField.onEndEdit.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
    }

    public void OnGainSliderValueChanged(float newValue)
    {
        float clampedValue = Mathf.Clamp01((float)Decibels.DecibelsToLinear(newValue));
        if (currentylSelectedEffectUI != null && currentylSelectedEffectUI.MyEffectInfo.signal != null)
        {
            currentylSelectedEffectUI.MyEffectInfo.signal.Amplitude = clampedValue;
        }
        UpdateInputFieldValue(gainTextValue, newValue);
    }

    public void OnGainInputFieldValueChanged(string newValue)
    {
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            if (currentylSelectedEffectUI != null)
            {
                gainTextValue.text = ((float)Decibels.LinearToDecibels(currentylSelectedEffectUI.MyEffectInfo.signal.Amplitude)).ToString();
            }
            return;
        }
        gainSlider.value = Mathf.Clamp(value, -100.0F, 0.0F);
    }

    public void OnFrequencySliderValueChanged(float newValue)
    {
        float linearToLogaritmicValue = Mathf.Pow(newValue, powFactor);
        float remapedValue = MathHelper.Remap(linearToLogaritmicValue, 0.0F, 1.0F, 0.001F, 20000.0F);
        float clampedValue = /*Mathf.Round(*/remapedValue/*)*/;
        if (currentylSelectedEffectUI != null && currentylSelectedEffectUI.MyEffectInfo.signal != null)
        {
            currentylSelectedEffectUI.MyEffectInfo.signal.Frequency = clampedValue;
        }
        UpdateInputFieldValue(frequenctTextValue, clampedValue);
    }

    public void OnFrequencyInputFieldValueChanged(string newValue)
    {
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            if (currentylSelectedEffectUI != null)
            {
                frequenctTextValue.text = currentylSelectedEffectUI.MyEffectInfo.signal.Frequency.ToString();
            }
            return;
        }
        frequencySlider.value = Mathf.Pow(MathHelper.Remap(Mathf.Clamp(/*Mathf.Round(*/value/*)*/, 0.001F, 20000.0F), 0.001F, 20000.0F, 0.0F, 1.0F), 1.0F / powFactor);
    }

    public void OnEffectSignaleDropdownValueChanged(int value)
    {
        if (currentylSelectedEffectUI != null)
        {
            GameObject generatorGameObject;
            if (currentylSelectedEffectUI.MyEffectInfo.signal != null)
            {
                generatorGameObject = currentylSelectedEffectUI.MyEffectInfo.signal.gameObject;
                Destroy(currentylSelectedEffectUI.MyEffectInfo.signal);
            }
            else
            {
                generatorGameObject = new GameObject("GeneratorType" + value);
            }
            WaveGenerator newWaveGenerator = null;
            switch ((WaveGeneratorType) value)
            {
                case WaveGeneratorType.Sin:
                    newWaveGenerator = generatorGameObject.AddComponent<SineWaveGenerator>();
                    break;
                case WaveGeneratorType.Square:
                    newWaveGenerator = generatorGameObject.AddComponent<SquareWaveGenerator>();
                    break;
                case WaveGeneratorType.Triangle:
                    newWaveGenerator = generatorGameObject.AddComponent<TriangleWaveGenerator>();
                    break;
                case WaveGeneratorType.SawTooth:
                    newWaveGenerator = generatorGameObject.AddComponent<SawToothGenerator>();
                    break;
                case WaveGeneratorType.WhiteNoise:
                    newWaveGenerator = generatorGameObject.AddComponent<WhiteNoiseGenerator>();
                    break;
                case WaveGeneratorType.PinkNoise:
                    newWaveGenerator = generatorGameObject.AddComponent<PinkNoiseGenerator>();
                    break;
                case WaveGeneratorType.RedNoise:
                    newWaveGenerator = generatorGameObject.AddComponent<RedNoiseGenerator>();
                    break;
                default:
                    newWaveGenerator = null;
                    Destroy(generatorGameObject);
                    break;
            }
            currentylSelectedEffectUI.MyEffectInfo.signal = newWaveGenerator;
            UpdateWaveGeneratorParameters();
        }
    }

    public void OnAParameterValueChanged(string newValue)
    {
        if (currentylSelectedEffectUI == null)
        {
            return;
        }
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            frequenctTextValue.text = currentylSelectedEffectUI.MyEffectInfo.a.ToString();
            return;
        }
        currentylSelectedEffectUI.MyEffectInfo.a = value;
    }

    public void OnBParameterValueChanged(string newValue)
    {
        if (currentylSelectedEffectUI == null)
        {
            return;
        }
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            frequenctTextValue.text = currentylSelectedEffectUI.MyEffectInfo.b.ToString();
            return;
        }
        currentylSelectedEffectUI.MyEffectInfo.b = value;
    }

    public void OnCParameterValueChanged(string newValue)
    {
        if (currentylSelectedEffectUI == null)
        {
            return;
        }
        float value;
        bool parsed = float.TryParse(newValue, out value);
        if (parsed == false)
        {
            frequenctTextValue.text = currentylSelectedEffectUI.MyEffectInfo.b.ToString();
            return;
        }
        currentylSelectedEffectUI.MyEffectInfo.c = value;
    }

    public void OnPlayClick()
    {
        modifableEchoTester.StartPlaing(playMelodyToggle.isOn);
        currentlyPlaingEchoTesters = new List<EchoTester> { modifableEchoTester };
        ToggleUIInteractibility(false);
    }

    public void OnStopClick()
    {
        modifableEchoTester.StopPlaing();
        echoTesters[1].StopPlaing();
        echoTesters[2].StopPlaing();
        echoTesters[3].StopPlaing();
        currentlyPlaingEchoTesters = null;
        ToggleUIInteractibility(true);
    }

    public void OnPlayMelody1Click()
    {
        echoTesters[1].StartPlaing(true);
        currentlyPlaingEchoTesters = new List<EchoTester> { echoTesters[1] };
        ToggleUIInteractibility(false);
    }

    public void OnPlayMelody2Click()
    {
        echoTesters[2].StartPlaing(true);
        currentlyPlaingEchoTesters = new List<EchoTester> { echoTesters[2] };
        ToggleUIInteractibility(false);
    }

    public void OnPlayBothMelodiesClick()
    {
        echoTesters[1].StartPlaing(true);
        echoTesters[2].StartPlaing(true);
        currentlyPlaingEchoTesters = new List<EchoTester> { echoTesters[1], echoTesters[2] };
        ToggleUIInteractibility(false);
    }

    public void OnPlayWindClick()
    {
        echoTesters[3].StartPlaing(false);
        currentlyPlaingEchoTesters = new List<EchoTester> { echoTesters[3] };
        ToggleUIInteractibility(false);
    }

    public void OnSendImpulseClick()
    {
        if (currentlyPlaingEchoTesters == null)
        {
            return;
        }
        float durationValue;
        bool result = float.TryParse(durationInputField.text, out durationValue);
        if (result)
        {
            for (int i = 0; i < currentlyPlaingEchoTesters.Count; ++i)
            {
                currentlyPlaingEchoTesters[i].SendImpulse(durationValue);
            }
        }
    }

    public void OnRemoveSelectedItemClick()
    {
        modifableEchoTester.effects.Remove(currentylSelectedEffectUI.MyEffectInfo);
        Destroy(currentylSelectedEffectUI.gameObject);
        currentylSelectedEffectUI = null;
    }

    private void ToggleUIInteractibility(bool interactible)
    {
        foreach (Button button in buttonsToDisableDuringPlay)
        {
            button.interactable = interactible;
        }
        //gainSlider.interactable = interactible;
        //gainTextValue.interactable = interactible;
        //frequencySlider.interactable = interactible;
        //frequenctTextValue.interactable = interactible;
        effectSignalDropdown.interactable = interactible;
        parameterAInputField.interactable = interactible;
        parameterBInputField.interactable = interactible;
        parameterCInputField.interactable = interactible;
    }

    private void OnDisable()
    {
        //OnStopClick();
    }
}