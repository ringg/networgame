﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WaveGeneratorUI : MonoBehaviour
{
    [SerializeField]
    private Text generatorTypeText = null;
    [SerializeField]
    private Button waveGeneratorUIButton = null;

    public GeneratorsUI CurrentGeneratorsUI { get; set; }

    public WaveGenerator MyWaveGanerator { get; set; }

    private void Start()
    {
        generatorTypeText.text = MyWaveGanerator.GetWaveGeneratorType().ToString() + " generator";
    }

    public void OnIsPlaingClick(bool value)
    {
        MyWaveGanerator.IsAllowedToPlay = value;
        if (CurrentGeneratorsUI.CurrentlySelectedGeneratorUI == this)
        {
            CurrentGeneratorsUI.ToggleWaveParametersInteractibility(value, value && MyWaveGanerator.GetWaveGeneratorType() < WaveGeneratorType.WhiteNoise);
            CurrentGeneratorsUI.UpdateWaveGeneratorParameters();
        }
    }

    public void OnShouldBeSubjectedToEffectsClick(bool value)
    {
        MyWaveGanerator.ShouldBeSubjectedToEffects = value;
    }

    public void OnWaveGeneratorUIClick()
    {
        ColorBlock buttonColors = waveGeneratorUIButton.colors;
        buttonColors.normalColor = new Color32(255, 255, 255, 255);
        waveGeneratorUIButton.colors = buttonColors;
        EventSystem.current.SetSelectedGameObject(null);
        CurrentGeneratorsUI.OnWaveGeneratorSelected(this);
    }

    public void OnVaweGeneratorUIDeselected()
    {
        ColorBlock buttonColors = waveGeneratorUIButton.colors;
        buttonColors.normalColor = new Color32(214, 214, 214, 255);
        waveGeneratorUIButton.colors = buttonColors;
    }
}