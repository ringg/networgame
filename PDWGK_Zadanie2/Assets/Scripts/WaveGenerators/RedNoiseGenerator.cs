﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedNoiseGenerator : WaveGenerator
{
    private double lastOutput;

    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.RedNoise;
    }

    protected override float GetSample()
    {
        double white = UnityEngine.Random.Range(-1.0F, 1.0F);
        double red = (lastOutput + (0.02 * white)) / 1.02;
        lastOutput = red;
        return Mathf.Clamp((float)(amplitude * red * 3.5), -amplitude, amplitude);
    }
}