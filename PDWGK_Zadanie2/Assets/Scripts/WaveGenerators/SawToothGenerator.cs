﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawToothGenerator : WaveGenerator
{
    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.SawTooth;
    }

    protected override float GetSample()
    {
        return amplitude * (((phase * 2) % 2) - 1);
    }
}