﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveGenerator : WaveGenerator
{
    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.Sin;
    }

    protected override float GetSample()
    {
        return amplitude * Mathf.Sin(TwoPI * phase);
    }
}