﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareWaveGenerator : WaveGenerator
{
    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.Square;
    }

    protected override float GetSample()
    {
        float sampleSaw = ((sample * (2 * Frequency / sampleRate)) % 2) - 1;
        return sampleSaw > 0 ? amplitude : -amplitude;
    }
}