﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleWaveGenerator : WaveGenerator
{
    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.Triangle;
    }

    protected override float GetSample()
    {
        float sampleSaw = ((phase * 2) % 2);
        float sampleValue = 2 * sampleSaw;
        if (sampleValue > 1)
            sampleValue = 2 - sampleValue;
        if (sampleValue < -1)
            sampleValue = -2 - sampleValue;

        return sampleValue * amplitude;
    }
}