﻿using NAudio.Utils;
using NAudio.Wave;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WaveGenerator : MonoBehaviour, IWaveProvider
{
    protected const int channels = 1;
    protected const int sampleRate = 44100;
    protected const float TwoPI = 2 * Mathf.PI;

    public float Frequency = 440;

    public bool ShouldBeSubjectedToEffects;
    
    protected float amplitude;
    public float Amplitude
    {
        get { return amplitude; }
        set
        {
            if (isAllowedToPlay)
            {
                amplitude = value;
            }
        }
    }
    protected bool isAllowedToPlay;
    public bool IsAllowedToPlay
    {
        get { return isAllowedToPlay; }
        set
        {
            isAllowedToPlay = value;
            if (isAllowedToPlay == false)
            {
                amplitude = 0.0F;
            }
            else
            {
                amplitude = (float)Decibels.DecibelsToLinear(-100.0);
            }
        }
    }

    protected WaveFormat waveFormat;
    protected int sample;
    protected float phase;

    protected virtual void Awake()
    {
        SetWaveFormat(sampleRate, channels);
        amplitude = (float)Decibels.DecibelsToLinear(-100.0);
        isAllowedToPlay = true;
    }

    public void SetWaveFormat(int sampleRate, int channels)
    {
        waveFormat = WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channels);
    }

    public int Read(byte[] buffer, int offset, int count)
    {
        WaveBuffer waveBuffer = new WaveBuffer(buffer);
        int samplesRequired = count / 4;
        int samplesRead = Read(waveBuffer.FloatBuffer, offset / 4, samplesRequired);
        return samplesRead * 4;
    }

    public int Read(float[] buffer, int offset, int sampleCount)
    {
        for (int n = 0; n < sampleCount; n++)
        {
            phase += Frequency / sampleRate;
            buffer[n + offset] = GetSample();
            sample++;
            if (sample >= sampleRate) sample = 0;
        }
        return sampleCount;
    }

    protected abstract float GetSample();

    public abstract WaveGeneratorType GetWaveGeneratorType();

    public WaveFormat WaveFormat
    {
        get { return waveFormat; }
    }
}

public enum WaveGeneratorType
{
    Sin,
    Square,
    Triangle,
    SawTooth,
    WhiteNoise,
    PinkNoise,
    RedNoise
}