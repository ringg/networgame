﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteNoiseGenerator : WaveGenerator
{
    public override WaveGeneratorType GetWaveGeneratorType()
    {
        return WaveGeneratorType.WhiteNoise;
    }

    protected override float GetSample()
    {
        return amplitude * UnityEngine.Random.Range(-1.0F, 1.0F);
    }
}