﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bugbear : MonoBehaviour
{
    [SerializeField]
    private StudioEventEmitter FmodEventEmitter = null;

    private float bugbearDurationInSeconds;

    private void Awake()
    {
        int bugbearDurationInMiliseconds;
        FMOD.RESULT result = FmodEventEmitter.EventDescription.getLength(out bugbearDurationInMiliseconds);
        if (result != FMOD.RESULT.OK)
        {
            bugbearDurationInMiliseconds = 12000;
        }
        bugbearDurationInSeconds = bugbearDurationInMiliseconds * 0.001F;
    }

    private void OnEnable()
    {
        StartCoroutine(DisableBugbearAfterTimeCoroutine());
    }

    IEnumerator DisableBugbearAfterTimeCoroutine()
    {
        yield return new WaitForSeconds(bugbearDurationInSeconds);
        gameObject.SetActive(false);
    }
}