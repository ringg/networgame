﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class BugbearManager : MonoBehaviour
{
    [SerializeField]
    private bool bugbearEnabled = true;
    [SerializeField]
    private int statisticalSecondsToRunBugbear = 1;
    [SerializeField]
    private Vector3 maxBugbearsFromPlayerDistanceOnEachAxis = Vector3.zero;
    [SerializeField]
    private Bugbear bugbearPrefab = null;
    [SerializeField]
    private RigidbodyFirstPersonController characterController = null;

    private List<Bugbear> spawnedBugbears;

    private Coroutine bugbearCoroutine;

    private void Awake()
    {
        spawnedBugbears = new List<Bugbear>();
    }

    private void Start()
    {
        if (bugbearCoroutine == null)
        {
            bugbearCoroutine = StartCoroutine(BugbearCoroutine());
        }
    }

    private void OnValidate()
    {
        if (Application.isPlaying == false)
        {
            return;
        }
        if (bugbearCoroutine == null)
        {
            bugbearCoroutine = StartCoroutine(BugbearCoroutine());
        }
    }

    private IEnumerator BugbearCoroutine()
    {
        while (bugbearEnabled)
        {
            yield return new WaitForSeconds(1.0F);
            if (Random.Range(0, statisticalSecondsToRunBugbear) == 0)
            {
                EnableSingleBugbear();
            }
        }
        bugbearCoroutine = null;
    }

    //Object pooling
    private void EnableSingleBugbear()
    {
        for (int i = 0; i < spawnedBugbears.Count; ++i)
        {
            if (spawnedBugbears[i].gameObject.activeSelf == false)
            {
                PrepareBugbear(spawnedBugbears[i]);
                spawnedBugbears[i].gameObject.SetActive(true);
                return;
            }
        }
        Bugbear newBugbear = Instantiate(bugbearPrefab, transform, true);
        spawnedBugbears.Add(newBugbear);
        PrepareBugbear(newBugbear);
        newBugbear.gameObject.SetActive(true);
    }

    private void PrepareBugbear(Bugbear bugbearToPrepare)
    {
        bugbearToPrepare.transform.position = new Vector3(
            characterController.cam.transform.position.x + Random.Range(-maxBugbearsFromPlayerDistanceOnEachAxis.x, maxBugbearsFromPlayerDistanceOnEachAxis.x),
            characterController.cam.transform.position.y + Random.Range(-maxBugbearsFromPlayerDistanceOnEachAxis.y, maxBugbearsFromPlayerDistanceOnEachAxis.y),
            characterController.cam.transform.position.z + Random.Range(-maxBugbearsFromPlayerDistanceOnEachAxis.z, maxBugbearsFromPlayerDistanceOnEachAxis.z)
        );
    }
}