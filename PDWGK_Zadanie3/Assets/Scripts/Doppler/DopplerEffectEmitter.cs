﻿using FMODUnity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DopplerEffectEmitter : MonoBehaviour
{
    public const float SoundSpeed = 340.0F;

    [SerializeField]
    private float basePitch = 1.0F;
    public float BasePitch
    {
        get { return basePitch; }
        set { basePitch = value; }
    }

    private StudioEventEmitter fmodStudioEventEmitter;
    private Rigidbody rigidbodyComponent;

    private void Awake()
    {
        fmodStudioEventEmitter = GetComponent<StudioEventEmitter>();
        if (fmodStudioEventEmitter == null)
        {
            Debug.LogWarning("No StudioEventEmitter component found in DopplerEffectEmitter's gameobject!");
            enabled = false;
        }
        rigidbodyComponent = GetComponentInParent<Rigidbody>();
        if (rigidbodyComponent == null)
        {
            Debug.LogWarning("No rigidbody component found in DopplerEffectEmitter's gameobject hierarchy!");
            enabled = false;
        }
    }

    private void Start()
    {
        if (DopplerEffectListener.instance == null)
        {
            Debug.LogWarning("No DopplerEffectListener instance found!");
            enabled = false;
        }
        if (DopplerEffectListener.instance.rigidbodyComponent == null)
        {
            Debug.LogWarning("DopplerEffectListener instance has no rigidbody component in gameobject hierarchy!");
            enabled = false;
        }
    }

    private void FixedUpdate()
    {
        Vector3 directionOfListener = (transform.position - DopplerEffectListener.instance.transform.position).normalized;
        Vector3 sumOfVelocities = rigidbodyComponent.velocity - DopplerEffectListener.instance.rigidbodyComponent.velocity;
        float speedRelativeToListener = sumOfVelocities.magnitude * Vector3.Dot(sumOfVelocities.normalized, directionOfListener);
        fmodStudioEventEmitter.EventInstance.setPitch(basePitch + (((SoundSpeed - speedRelativeToListener) / SoundSpeed) - 1.0F));
    }
}