﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DopplerEffectListener : MonoBehaviour
{
    public static DopplerEffectListener instance;

    internal Rigidbody rigidbodyComponent;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            rigidbodyComponent = GetComponentInParent<Rigidbody>();
            if (rigidbodyComponent == null)
            {
                Debug.LogWarning("No rigidbody component found in DopplerEffectListener's gameobject hierarchy!");
            }
        }
        else
        {
            Debug.LogWarning("DopplerEffectListener instance created more than once!");
            Destroy(gameObject);
        }
    }
}