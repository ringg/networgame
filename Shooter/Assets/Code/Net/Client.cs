﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

public class Client : MonoBehaviour
{
    List<MsgSendInfo> unconfirmedMsgs = new List<MsgSendInfo>();
    List<MsgSendInfo> receivedMsgs = new List<MsgSendInfo>();
    List<Message> msgsToSend = new List<Message>();
    public List<Action<Message, IPEndPoint>> msgHandlers = new List<Action<Message, IPEndPoint>>();
    Sucket socket;
    NetSettings settings;

    MsgSendInfo[] receivedHistory = new MsgSendInfo[200];
    int receivedHistoryIter;

    // host connection
    IPEndPoint hostEP;
    public string login;
    UInt64 hashword;

    float msgDispatchTimer;

    void Awake()
    {
        settings = GetComponent<NetSettings>();
    }

    void Start()
    {
        socket = new Sucket(0);
        socket.OnReceive += MsgReceived;
        for (int i = 0; i < 128; ++i)
        {
            msgHandlers.Add(null);
            msgHandlers[i] += ConfirmMsg;
        }
        
        msgHandlers[3] += HandleConfirm;
    }

    void Update()
    {
        // handle received messages
        for (int i = 0; i < receivedMsgs.Count; ++i)
            if (receivedMsgs[i] != null)
                ProcessMsg(receivedMsgs[i]);
        receivedMsgs.Clear();

        // dispatch queued messages
        msgDispatchTimer += Time.deltaTime;
        if (msgDispatchTimer < settings.sendInterval)
            return;
        msgDispatchTimer %= settings.sendInterval;

        Message msg;
        for (int i = 0; i < msgsToSend.Count; ++i)
        {
            msg = msgsToSend[i];
            if (msg == null)
                continue;

            msg.Encode();
            if (msg.buffer[0] != 3)
                unconfirmedMsgs.Add(new MsgSendInfo(msg, hostEP));
            socket.Send(msg.buffer, hostEP);
        }
        msgsToSend.Clear();

        // resend unconfirmed msgs
        for (int i = 0; i < unconfirmedMsgs.Count; ++i)
        {
            if (Time.time - unconfirmedMsgs[i].time < settings.resendTime)
                continue;

            unconfirmedMsgs[i].time = Time.time;
            msg = unconfirmedMsgs[i].msg;
            if (msg == null) continue;
            socket.Send(msg.buffer, unconfirmedMsgs[i].ep);
        }
    }

    void MsgReceived(Message msg, IPEndPoint ep)
    {
        receivedMsgs.Add(new MsgSendInfo(msg, ep));
    }

    public void Connect()
    {
        enabled = true;
        ConnectToHost(login, 1337);
    }

    public void Disconnect()
    {
        this.login = "";
        this.hashword = 0;
        hostEP = null;

        unconfirmedMsgs.Clear();
        receivedMsgs.Clear();
        msgsToSend.Clear();

        enabled = false;
    }

    public void ConnectToHost(string login, UInt64 hashword, string ip = "127.0.0.1")
    {
        this.login = login;
        this.hashword = hashword;
        hostEP = new IPEndPoint(IPAddress.Parse(ip), 11000);
        
        MsgLogin msgLogin = new MsgLogin();
        msgLogin.login = login;
        msgLogin.hashword = hashword;

        SendToHost(msgLogin);
    }

    public void SendToHost(Message msg)
    {
        msgsToSend.Add(msg);
    }

    void ConfirmMsg(Message msg, IPEndPoint ep)
    {
        if (msg.buffer[0] == 3) return;
        MsgConfirm msgConfirm = new MsgConfirm();
        msgConfirm.confirmStamp = msg.timestamp;
        SendToHost(msgConfirm);
    }

    #region message handlers

    void ProcessMsg(MsgSendInfo info)
    {
        if (IsInHistory(info))
        {
            Debug.Log("received sth twice, ignoring...");
            return;
        }
        else
        {
            receivedHistory[receivedHistoryIter] = info;
            receivedHistoryIter = (receivedHistoryIter + 1) % receivedHistory.Length;
            msgHandlers[info.msg.buffer[0]](info.msg, info.ep);
        }
        ConfirmMsg(info.msg, info.ep);
    }

    void HandleConfirm(Message msg, IPEndPoint ep)
    {
        MsgConfirm msgConfirm = (MsgConfirm)msg;

        MsgSendInfo info = unconfirmedMsgs
            .FirstOrDefault(x => x.msg.timestamp == msgConfirm.confirmStamp);

        if (info == null) return;
        unconfirmedMsgs.Remove(info);
    }

    bool IsInHistory(MsgSendInfo msgInfo)
    {
        foreach (MsgSendInfo info in receivedHistory)
            if (info != null && msgInfo.msg.timestamp == info.msg.timestamp && info.ep.Equals(msgInfo.ep))
                return true;
        return false;
    }

    #endregion
}
