﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

public class Host : MonoBehaviour
{
    Dictionary<string, List<MsgSendInfo>> unconfirmedMsgs = new Dictionary<string, List<MsgSendInfo>>();
    List<MsgSendInfo> receivedMsgs = new List<MsgSendInfo>();
    List<MsgSendInfo> msgsToSend = new List<MsgSendInfo>();
    public List<Action<Message, IPEndPoint>> msgHandlers = new List<Action<Message, IPEndPoint>>();
    NetSettings settings;

    MsgSendInfo[] receivedHistory = new MsgSendInfo[200];
    int receivedHistoryIter;

    Sucket socket;
    List<Connection> clients = new List<Connection>();
    public bool isHosting;

    float msgDispatchTimer;

    void Awake()
    {
        if (!isHosting) { Destroy(this); return; }
        settings = GetComponent<NetSettings>();

        Debug.Log(IpHelper.GetLocalIPAddress());
    }

    void Start()
    {
        socket = new Sucket(11000);
        socket.OnReceive += MsgReceived;

        for (int i = 0; i < 128; ++i)
        {
            msgHandlers.Add(null);
            msgHandlers[i] += ConfirmMsg;
        }

        msgHandlers[0] += HandleLogin;
        msgHandlers[3] += HandleConfirm;
    }

    void Update()
    {
        // handle received messages
        for (int i = 0; i < receivedMsgs.Count; ++i)
            if (receivedMsgs[i] != null)
                ProcessMsg(receivedMsgs[i]);
        receivedMsgs.Clear();

        // dispatch queued messages
        msgDispatchTimer += Time.deltaTime;
        if (msgDispatchTimer < settings.sendInterval)
            return;
        msgDispatchTimer %= settings.sendInterval;

        Message msg;
        for (int i = 0; i < msgsToSend.Count; ++i)
        {
            msg = msgsToSend[i].msg;
            if (msg == null)
                continue;

            msg.Encode();
            if (msg.buffer[0] != 3 && msg.buffer[0] != 5)
                unconfirmedMsgs[msgsToSend[i].ep.ToString()].Add(new MsgSendInfo(msg, msgsToSend[i].ep));
            socket.Send(msg.buffer, msgsToSend[i].ep);
        }
        msgsToSend.Clear();

        //kick bad connections
        List<MsgSendInfo>[] msgLists = unconfirmedMsgs.Values.ToArray();
        foreach (List<MsgSendInfo> msgList in msgLists)
        {
            if (msgList.Count > settings.unconfirmedForKick)
            {
                Kick(clients[FindConnection(msgList[0].ep)]);
                Debug.Log("kicking player due to poor communication");
                continue;
            }
        }
          
        // resend unconfirmed msgs
        foreach (List<MsgSendInfo> msgList in unconfirmedMsgs.Values)
        {
            for (int i = 0; i < msgList.Count; ++i)
            {
                if (Time.time - msgList[i].time < settings.resendTime)
                    continue;

                msgList[i].time = Time.time;
                msg = msgList[i].msg;

                if (msg == null)
                    continue;

                socket.Send(msg.buffer, msgList[i].ep);
            }
        }
    }

    void MsgReceived(Message msg, IPEndPoint ep)
    {
        if ((msg.buffer[0] == 0 && FindConnection(ep) != -1) || (msg.buffer[0] != 0 && FindConnection(ep) == -1))
        {
            Debug.Log("Ignoring message: either sent by unknown player, or a player trying to connect twice");
            return;
        }

        receivedMsgs.Add(new MsgSendInfo(msg, ep));
    }

    public void SendToAllClients(Message msg)
    {
        foreach (Connection player in clients)  
            SendToClient(msg, player.ep);
    }

    public void SendToClient(Message msg, IPEndPoint clientEP)
    {
        msgsToSend.Add(new MsgSendInfo(msg, clientEP));
    }

    void ConfirmMsg(Message msg, IPEndPoint clientEP)
    {
        if (msg.buffer[0] == 3) return;
        MsgConfirm msgConfirm = new MsgConfirm();
        msgConfirm.confirmStamp = msg.timestamp;
        SendToClient(msgConfirm, clientEP);
    }

    #region message handlers

    void ProcessMsg(MsgSendInfo info)
    {
        if (IsInHistory(info))
        {
            Debug.Log("received sth twice, ignoring...");
            return;
        }
        else
        {
            receivedHistory[receivedHistoryIter] = info;
            receivedHistoryIter = (receivedHistoryIter + 1) % receivedHistory.Length;
            msgHandlers[info.msg.buffer[0]](info.msg, info.ep);
        }
        ConfirmMsg(info.msg, info.ep);
    }

    void HandleLogin(Message msg, IPEndPoint ep)
    {
        MsgLogin logMsg = (MsgLogin)msg;

        if (FindConnection(ep) != -1)
        {
            Debug.Log("Login error: a player with this login already exists");
            return;
        }

        clients.Add(MakeConnection(logMsg, ep));
        Debug.Log("Adding new player");
    }

    void HandleConfirm(Message msg, IPEndPoint ep)
    {
        int id = FindConnection(ep);
        if (id == -1) return;

        MsgConfirm msgConfirm = (MsgConfirm)msg;
        
        MsgSendInfo info = unconfirmedMsgs[ep.ToString()]
            .FirstOrDefault(x => x.msg.timestamp == msgConfirm.confirmStamp);
        
        if (info == null) return;
        unconfirmedMsgs[ep.ToString()].Remove(info);
    }

    bool IsInHistory(MsgSendInfo msgInfo)
    {
        foreach(MsgSendInfo info in receivedHistory)
            if (info != null && msgInfo.msg.timestamp == info.msg.timestamp && info.ep.Equals(msgInfo.ep))
                return true;
        return false;
    }

    #endregion

    #region managing connections

    int FindConnection(string login)
    {
        Connection client = clients.FirstOrDefault(x => x.login == login);

        if (client == null) return -1;
        else return clients.IndexOf(client);
    }

    int FindConnection(IPEndPoint ep)
    {
        return clients.FindIndex(x => x.ep.Equals(ep));
    }

    Connection MakeConnection(MsgLogin logMsg, IPEndPoint ep)
    {
        unconfirmedMsgs.Add(ep.ToString(), new List<MsgSendInfo>());

        Connection c = new Connection();
        c.login = logMsg.login;
        c.hashword = logMsg.hashword;
        c.ep = ep;
        return c;
    }

    void Kick(Connection c)
    {
        MsgKick kick = new MsgKick();
        msgsToSend.RemoveAll(x => x.ep.Equals(c.ep));
        SendToClient(kick, c.ep);

        unconfirmedMsgs.Remove(c.ep.ToString());
        clients.Remove(c);
        // and other stuff
    }
    #endregion
}