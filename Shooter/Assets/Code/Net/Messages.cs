﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

public abstract class Message
{
    private static Int64 idCounter;

    public abstract byte msgType { get; }
    public Int64 timestamp;
    public byte[] buffer;

    public abstract void Encode(); // uses subclass fields to fill byte buffer
    public abstract void Decode(); // uses byte buffer to fill subclass fields

    public Message()
    {
        timestamp = idCounter++;
    }

    public static Message MakeFromBuffer(byte[] buffer)
    {
        Message result;
        switch(buffer[0])
        {
            case 0: result = new MsgLogin(); break;
            case 1: result = new MsgTransform(); break;
            case 2: result = new MsgSpawn(); break;
            case 3: result = new MsgConfirm(); break;
            case 4: result = new MsgInput(); break;
            case 5: result = new MsgKick(); break;
            default: throw new Exception("invalid msgType");
        }

        result.buffer = buffer;
        result.Decode();
        return result;
    }
}

public class MsgLogin : Message
{
    public override byte msgType { get { return 0; } }

    public string login;
    public UInt64 hashword;


    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(hashword);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = Encoding.ASCII.GetBytes(login);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode()
    {
        timestamp = StructTools.RawDeserialize<Int64>(buffer, 1);
        hashword = StructTools.RawDeserialize<UInt64>(buffer, 9);
        login = Encoding.ASCII.GetString(buffer, 17, buffer.Length - 17);
    }
}

public class MsgTransform : Message
{
    public override byte msgType { get { return 1; } }

    public UInt16 netID;
    public bool posDirty;
    public bool rotDirty;
    public bool syncRB;

    public Vector3 position;
    public Quaternion rotation;
    public Vector3 velocity;


    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        stream.WriteByte((byte)(posDirty ? 1 : 0));
        stream.WriteByte((byte)(rotDirty ? 1 : 0));
        stream.WriteByte((byte)(syncRB   ? 1 : 0));

        if (posDirty)
        {
            tmpBytes = StructTools.RawSerialize(position);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }
        if (rotDirty)
        {
            tmpBytes = StructTools.RawSerialize(rotation);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }
        if (syncRB)
        {
            tmpBytes = StructTools.RawSerialize(velocity);
            stream.Write(tmpBytes, 0, tmpBytes.Length);
        }

        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode()
    {
        int pos = 1;
        timestamp = StructTools.RawDeserialize<Int64>(buffer, pos);
        pos += Marshal.SizeOf(timestamp.GetType());
        netID = StructTools.RawDeserialize<UInt16>(buffer, pos);
        pos += Marshal.SizeOf(netID.GetType());
        posDirty = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        rotDirty = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        syncRB   = StructTools.RawDeserialize<byte>(buffer, pos) == 1;
        pos += 1;
        if (posDirty)
        {
            position = StructTools.RawDeserialize<Vector3>(buffer, pos);
            pos += Marshal.SizeOf(position.GetType());
        }
        if (rotDirty)
        {
            rotation = StructTools.RawDeserialize<Quaternion>(buffer, pos);
            pos += Marshal.SizeOf(rotation.GetType());
        }
        if (syncRB)
        {
            velocity = StructTools.RawDeserialize<Vector3>(buffer, pos);
            pos += Marshal.SizeOf(velocity.GetType());
        }
    }
}

public class MsgSpawn : Message
{
    public override byte msgType { get { return 2; } }

    public UInt16 netID;
    public UInt16 spawnableID;

    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(netID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(spawnableID);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode()
    {
        timestamp = StructTools.RawDeserialize<Int64>(buffer, 1);
        netID = StructTools.RawDeserialize<UInt16>(buffer, 9);
        spawnableID = StructTools.RawDeserialize<UInt16>(buffer, 11);
    }
}

public class MsgConfirm : Message
{
    public override byte msgType { get { return 3; } }

    public Int64 confirmStamp;

    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);
        tmpBytes = StructTools.RawSerialize(confirmStamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode()
    {
        timestamp = StructTools.RawDeserialize<Int64>(buffer, 1);
        confirmStamp = StructTools.RawDeserialize<Int64>(buffer, 9);
    }
}

public class MsgInput : Message
{
    public override byte msgType { get { return 4; } }

    public float vertical;
    public float horizontal;
    public bool fire;
    public bool jump;

    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        byte[] tmpBytes;

        stream.WriteByte(msgType);
        tmpBytes = StructTools.RawSerialize(timestamp);
        stream.Write(tmpBytes, 0, tmpBytes.Length);

        //todo

        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode()
    {
        timestamp = StructTools.RawDeserialize<Int64>(buffer, 1);

        // todo
    }
}

public class MsgKick : Message
{
    public override byte msgType { get { return 5; } }
    
    public override void Encode()
    {
        MemoryStream stream = new MemoryStream();
        stream.WriteByte(msgType);
        buffer = stream.ToArray();
        stream.Close();
    }

    public override void Decode() { }
}

public static class StructTools
{
    public static T RawDeserialize<T>(byte[] rawData, int position)
    {
        int rawsize = Marshal.SizeOf(typeof(T));
        if (rawsize > rawData.Length - position)
            throw new ArgumentException("Not enough data to fill struct. Array length from position: " + (rawData.Length - position) + ", Struct length: " + rawsize);
        IntPtr buffer = Marshal.AllocHGlobal(rawsize);
        Marshal.Copy(rawData, position, buffer, rawsize);
        T retobj = (T)Marshal.PtrToStructure(buffer, typeof(T));
        Marshal.FreeHGlobal(buffer);
        return retobj;
    }
    
    public static byte[] RawSerialize(object anything)
    {
        int rawSize = Marshal.SizeOf(anything);
        IntPtr buffer = Marshal.AllocHGlobal(rawSize);
        Marshal.StructureToPtr(anything, buffer, false);
        byte[] rawDatas = new byte[rawSize];
        Marshal.Copy(buffer, rawDatas, 0, rawSize);
        Marshal.FreeHGlobal(buffer);
        return rawDatas;
    }
}