﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Connection
{
    public string login;
    public UInt64 hashword;
    public IPEndPoint ep;
}

public class MsgSendInfo
{
    public static float gameTime;

    public MsgSendInfo(Message msg, IPEndPoint ep)
    {
        this.msg = msg;
        this.ep = ep;
        this.time = gameTime;
    }

    public Message msg;
    public IPEndPoint ep;
    public float time;
}

public static class IpHelper
{
    public static IPAddress GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                return ip;
        throw new Exception("Local IP Address Not Found!");
    }
}