﻿using System;
using UnityEngine;

public class NetObj : MonoBehaviour
{
    // dont change theese from inspector:
    public UInt16 netID;
    public UInt16 spawnableID;
    public bool wasInit;
    public bool isServer;
    public bool posDirty; //important only on server
    public bool rotDirty; //important only on server
    public bool syncRB { get { return rb != null; } }

    // these are ok to change
    public float rubberbandPos = 0.1f;
    public float rubberbandrot = 0.0f;

    Vector3 pos;   
    Quaternion rot;
    public Rigidbody rb { get; private set; }

    Int64 lastTimestamp;

    public void Init(UInt16 netID, UInt16 spawnableID, bool isServer)
    {
        this.netID = netID;
        this.spawnableID = spawnableID;
        this.isServer = isServer;
        wasInit = true;
        posDirty = true;
        rotDirty = true;
        pos = transform.position;
        rot = transform.rotation;
        rb = GetComponent<Rigidbody>();
    }
    
    public void Update()
    {
        if (isServer)
        {
            posDirty |= transform.position != pos;
            rotDirty |= transform.rotation != rot;
            pos = transform.position;
            rot = transform.rotation;
        }
        else
        {
            transform.position = Vector3.Lerp    (transform.position, pos, rubberbandPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, rubberbandrot);
        }      
    }

    public void Adjust(MsgTransform msg)
    {
        if (msg.timestamp < lastTimestamp) return;
        lastTimestamp = msg.timestamp;

        if (msg.posDirty) pos = msg.position;
        if (msg.rotDirty) rot = msg.rotation;
        if (msg.syncRB) rb.velocity = msg.velocity;
    }
}
