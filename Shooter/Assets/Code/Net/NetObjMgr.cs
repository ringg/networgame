﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

// Syncs objects between clients and server (does not handle connections)
public class NetObjMgr : MonoBehaviour
{
    #region variables

    public static NetObjMgr instance { get; private set; }
    public List<NetObj> spawnables;

    
    Dictionary<UInt16, NetObj> netObjs = new Dictionary<UInt16, NetObj>(); //<netid, obj>
    NetSettings settings;
    Client client;
    Host host;
    UInt16 lastUniqNetId;

    float msgDispatchTimer;

    #endregion
    
    void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(this);
            return;
        }

        instance = this;
        client = GetComponent<Client>();
        host = GetComponent<Host>();
        settings = GetComponent<NetSettings>();
    }

    void Start()
    {
        List<NetObj> objs = FindObjectsOfType<NetObj>().ToList();
        if (host != null && host.isHosting) // we're a host! we'll send info to everyone
        {
            // catalogue all netObjs in scene
            UInt16 spawnableID;
            for (UInt16 ID = 0; ID < objs.Count; ++ID)
            {
                spawnableID = FindSpawnableID(objs[ID]);

                if (spawnableID == UInt16.MaxValue)
                {
                    Debug.Log("Non registered NetObj found: " + objs[ID].name);
                    Destroy(objs[ID].gameObject);
                    objs.RemoveAt(ID);
                    --ID;
                    continue;
                }

                objs[ID].Init(lastUniqNetId, spawnableID, true);
                netObjs.Add(lastUniqNetId, objs[ID]);
                lastUniqNetId++;
            }
            InvokeRepeating("SpawnSth", 5, 5);
            // start listening
            host.msgHandlers[0] += HandleLogin;
        }
        else // we're just a client, clean up and wait for object info from host
        {
            foreach (NetObj netObj in objs)
                Destroy(netObj.gameObject);
        }

        client.msgHandlers[1] += HandleTransform;
        client.msgHandlers[2] += HandleSpawn;
    }

    void Update()
    {
        // update objects transforms
        if (host != null && host.isHosting)
        {
            msgDispatchTimer += Time.deltaTime;
            if (msgDispatchTimer < settings.sendInterval)
                return;
            msgDispatchTimer %= settings.sendInterval;

            foreach (NetObj netObj in netObjs.Values)
            {
                if (!netObj.posDirty && !netObj.rotDirty)
                    continue;
                MsgTransform msg = new MsgTransform();
                msg.netID = netObj.netID;
                msg.posDirty = netObj.posDirty;
                msg.rotDirty = netObj.rotDirty;
                msg.syncRB = netObj.syncRB;
                if (netObj.syncRB)
                    msg.velocity = netObj.rb.velocity;
                msg.position = netObj.transform.position;
                msg.rotation = netObj.transform.rotation;
                host.SendToAllClients(msg);
                netObj.posDirty = false;
                netObj.rotDirty = false;
            }
        }
    }
       

    void SpawnSth()
    {
        Spawn(3);
    }
    /// <summary>
    /// Server only :3
    /// </summary>
    public NetObj Spawn(int spawnableID)
    {
        NetObj result;
        if (spawnableID < 0 || spawnableID >= spawnables.Count) return null;
        
        result = Instantiate(spawnables[spawnableID]);
        result.name = spawnables[spawnableID].name;
        result.Init(lastUniqNetId, (ushort)spawnableID, true);
        netObjs.Add(lastUniqNetId, result);

        MsgSpawn msgRe = new MsgSpawn();
        msgRe.netID = lastUniqNetId;
        msgRe.spawnableID = (ushort)spawnableID;
        host.SendToAllClients(msgRe);
        result.posDirty = true;
        result.rotDirty = true;
        lastUniqNetId++;

        return result;
    }

    public NetObj Spawn(NetObj netObj)
    {
        return Spawn(spawnables.Find(x => x == netObj));
    }
     
    #region message handling

    public void HandleLogin(Message msg, IPEndPoint ep)
    {
        foreach (NetObj netObj in netObjs.Values)
        {
            MsgSpawn msgRe = new MsgSpawn();
            msgRe.netID = netObj.netID;
            msgRe.spawnableID = netObj.spawnableID;
            host.SendToClient(msgRe, ep);
            netObj.posDirty = true;
            netObj.rotDirty = true;
        }
    }

    public void HandleTransform(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgTransform msgTransform = (MsgTransform)msg;

            if (host != null && host.isHosting) return;
            if (!netObjs.ContainsKey(msgTransform.netID)) return;
            
            netObjs[msgTransform.netID].Adjust(msgTransform);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }
    
    public void HandleSpawn(Message msg, IPEndPoint ep)
    {
        try
        {
            MsgSpawn msgSpawn = (MsgSpawn)msg;

            if (host != null && host.isHosting) return;
            if (netObjs.ContainsKey(msgSpawn.netID)) return;

            NetObj no = Instantiate(spawnables[msgSpawn.spawnableID]);
            no.name = spawnables[msgSpawn.spawnableID].name;
            no.Init(msgSpawn.netID, msgSpawn.spawnableID, host != null && host.isHosting);
            netObjs.Add(msgSpawn.netID, no);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    #endregion

    #region helper functions

    // good enough? not rly -> (clone) / (1) at the end of name is a problem
    UInt16 FindSpawnableID(NetObj netObj)
    {
        for (UInt16 i = 0; i < spawnables.Count; ++i)
            if (netObj.name == spawnables[i].name)
                return i;
        return UInt16.MaxValue;
    }

    #endregion
}
