﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetSettings : MonoBehaviour
{
    public float resendTime = 0.5f;
    public float netSendRate = 20.0f;
    public float sendInterval { get { return 1.0f / netSendRate; } }

    public int unconfirmedForKick = 50;

    void Update()
    {
        MsgSendInfo.gameTime = Time.time;
    }
}
