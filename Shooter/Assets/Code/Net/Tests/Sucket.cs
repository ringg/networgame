﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Sucket
{
    public Action<Message, IPEndPoint> OnReceive;
    UdpClient socket;
    public float packetLossRate = 0.1f;

    public Sucket(int port)
    {
        socket = new UdpClient(port);
        socket.BeginReceive(new AsyncCallback(Receive), socket);
    }

    public void Send(byte[] message, IPEndPoint target)
    {
        if (UnityEngine.Random.Range(0.0f, 1.0f) < packetLossRate)
            return;

        socket.Send(message, message.Length, target);
    }

    void Receive(IAsyncResult result)
    {
        try
        {
            UdpClient socket = result.AsyncState as UdpClient;
            IPEndPoint source = new IPEndPoint(0, 0);
            byte[] message = socket.EndReceive(result, ref source);
            Message msg = Message.MakeFromBuffer(message);
            socket.BeginReceive(new AsyncCallback(Receive), socket);

            if (OnReceive != null)
                OnReceive(msg, source);
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }
}